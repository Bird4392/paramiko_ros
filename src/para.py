import paramiko
import numpy as np


class Paramiko():
    
    def __init__(self, hostname, username, password, port):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.port = port
        paramiko.util.log_to_file("paramiko.log")

        
        
    def ExecuteCommand(self, command):
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(self.hostname, username = self.username, password = self.password)
            print("Connected to %s" % self.hostname)
        except paramiko.AuthenticationException:
            print("Failed to connect to %s due to wrong username/password" %self.hostname)
            exit(1)
        except Exception as e:
            print(e.message)    
            exit(2)

        try:
            stdin, stdout, stderr = ssh.exec_command(command)
        except Exception as e:
            print(e.message)

        err = ''.join(stderr.readlines())
        out = ''.join(stdout.readlines())
        final_output = str(out)+str(err)
        print(final_output)
        
        return final_output


    def ConfigureSFTP(self):
        #FTP
        self.transport = paramiko.Transport((self.hostname,self.port))
        self.transport.connect(None,self.username,self.password) 
        self.sftp = paramiko.SFTPClient.from_transport(self.transport)
            
    def UploadSFTP(self, remotepath, localpath):
        self.sftp.put(localpath,remotepath)
        self.sftp.close()


    def DownloadSFTP(self, remotepath, localpath):
        self.sftp.get(remotepath,localpath)
        self.sftp.close()

    def OpenCSVSFTP(self, remotepath, localpath):
        self.DownloadSFTP(remotepath, localpath)
        data = np.genfromtxt(localpath, delimiter=',')
        return data

    
            
def main():
    hostname = "192.168.1.7"
    username = "ben"
    password = "starscape"
    port = 22
    command = 'nmap 192.168.1.*'

    remotepath = "/home/ben/xray_data.csv"
    localpath = "/home/ben/Desktop/xray_data.csv"

    para = Paramiko(hostname, username, password, port)
    answer = para.ExecuteCommand(command)

    para.ConfigureSFTP()
    data = para.OpenCSVSFTP(remotepath,localpath)
    print(data)


if __name__ == "__main__":
    main()
