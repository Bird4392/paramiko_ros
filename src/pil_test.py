
import numpy as np
from PIL import Image

data = np.genfromtxt("xray_data.csv", delimiter=',')
print np.argwhere(np.isnan(data))
data = data/data.max()
data = 255 * data
data = data.astype(np.uint8)
print(data)
# Creates PIL image
img = Image.fromarray(data)
img = img.convert('RGB')
img.save("test.bmp")


import sys
print(sys.version)

'''

import imageio
import numpy as np

# Construct 16-bit gradient greyscale image
#im = np.arange(65536,dtype=np.uint16).reshape(256,256)
im = np.genfromtxt("xray_data.csv", delimiter=',')

# Save as PNG with imageio
imageio.imwrite('result.png',im) 
# Now read image back from disk into Numpy array
im2 = imageio.imread('result.png') 

# Change first pixel to mid-grey
im2[0][0] = 32768



from PIL import Image
import numpy as np

# Construct 16-bit gradient greyscale image
#im = np.arange(65536,dtype=np.uint16).reshape(256,256)
#np.savetxt("xray_data.csv", im, delimiter=",")
im = np.genfromtxt("xray_data.csv", delimiter=',')

# Save as TIFF with PIL/Pillow
Image.fromarray(im).save('result.tif')

# Read image back from disk into PIL Image
im2 = Image.open('result.tif')                                                                                             

# Convert PIL Image to Numpy array
im2 = np.array(im2)

# Make first pixel mid-grey
im2[0][0] = 32768
'''