#!/usr/bin/env python3
import rospy
import para
from std_srvs.srv import Trigger, TriggerResponse
from sensor_msgs.msg import Image as SensorImage
import numpy as np
from PIL import Image

class Services():
    def __init__(self,hostname, username, password, port, command, localpath, remotepath):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.port = port
        self.command = command
        self.localpath = localpath
        self.remotepath = remotepath
        self.paramiko_functions = para.Paramiko(self.hostname, self.username, self.password, self.port)
        
        self.xray_image_service = rospy.Service('/xray_image', Trigger, self.trigger_xray_image)
        self.pub = rospy.Publisher('/image', SensorImage, queue_size=10)
    
    def trigger_xray_image(self,request):
        self.paramiko_functions.ConfigureSFTP()
        #print("configuring system")
        self.paramiko_functions.ExecuteCommand(self.command)
        self.data = self.paramiko_functions.OpenCSVSFTP(self.remotepath, self.localpath)
        self.data = self.data/self.data.max()
        self.data = 255 * self.data
        self.data = self.data.astype(np.uint8)
        # Creates PIL image
        self.img = Image.fromarray(self.data)
        self.img = self.img.convert('RGB')
        self.img.save("test.bmp")
        print("saved image")
        #img.show()
        msg = SensorImage()
        msg.header.stamp = rospy.Time.now()
        msg.height = self.img.height
        msg.width = self.img.width
        msg.encoding = "rgb8"
        msg.is_bigendian = False
        msg.step = 3 * self.img.width
        msg.data = np.array(self.img).tobytes()
        self.pub.publish(msg)
        return TriggerResponse(success=True, message="acquired xray image")
        



# Intializes everything
def start():
    
    hostname = rospy.get_param("/hostname")
    username = rospy.get_param('/username')
    password = rospy.get_param('/password')
    port = rospy.get_param('/port')
    command = rospy.get_param('/command')
    localpath = rospy.get_param('/localpath')
    remotepath = rospy.get_param('/remotepath')
    
    services = Services(hostname, username, password, port, command, localpath, remotepath)

    rospy.init_node('xray_sensor')
    rospy.spin()

    #I = numpy.asarray(PIL.Image.open('test.jpg'))
    #im = PIL.Image.fromarray(numpy.uint8(I))

if __name__ == '__main__':
    start()

