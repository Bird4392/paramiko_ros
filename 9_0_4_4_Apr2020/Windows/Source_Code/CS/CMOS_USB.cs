﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CMOS_USB_DemoApplication
{
    class CMOS_USB
    {
        //Error handle
        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        [StructLayout(LayoutKind.Sequential)]
        public struct UnitIntegrationParameter
        {
            public ushort Xray_Incident_Threshold;
            public ushort Xray_Integration_End_Threshold;
            public double Integration_Time;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitXrayImage
        {
            public byte Mode;
            public UnitIntegrationParameter IntegParam;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitSensorInformation
        {
            public UnitIntegrationParameter IntegParam;
            public ushort Lot_Serial_No;
            public byte Sensor_Type;
            public byte Firmware_Version;
        }

        public enum ImageEvents
        {
            CMOSUSB_EVENT_IMAGE_STARTED = 0x0001,
            CMOSUSB_EVENT_IMAGE_STOPPED = 0x0002,
        }
        public enum AbortEvents
        {
            CMOSUSB_EVENT_INTERNAL_ABORT = 0x10000000,
            CMOSUSB_EVENT_USER_ABORT = 0x20000000,
        }

        // Functions
        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_SetVid(ushort VID, string DeviceGUID);
        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenDevice(ushort ProductID);
        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenTargetDevice(ushort ProductID, ushort SerialNo);
        [DllImport("CMOS_USB.dll")]
        public extern static void USB_CloseDevice(System.IntPtr DeviceHandle);
        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenPipe(System.IntPtr DeviceHandle);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetXrayImage(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetTrigPdData(IntPtr DeviceHandle, double AcquisitionTime, ushort[] TrigPdData, out ulong bufferLength, ref UnitIntegrationParameter IntegParam);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetSensorInformation(IntPtr DeviceHandle, ref UnitIntegrationParameter IntegParam, out UnitSensorInformation SensorInfo);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_StopTrigPdData(IntPtr DeviceHandle, byte[] Buffer, out ulong bufferLength, ref UnitIntegrationParameter IntegParam);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ForceTrigAndGetDummy(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_AbortBulkPipe(IntPtr DeviceHandle);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetXrayCorrectionImage(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);
        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_SuspendDevice(IntPtr DeviceHandle, uint SuspendDelay);
        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_ResumeDevice(IntPtr DeviceHandle);
        
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ReadCustomerCode(IntPtr DeviceHandle, byte[] Buffer, uint Index, byte[] Code);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ReadTypeNumber(IntPtr DeviceHandle, byte[] Buffer);
        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ReadMemory(IntPtr DeviceHandle, byte[] Buffer, uint Address, uint Length);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_WaitEvent(IntPtr DeviceHandle, ref ulong pEventStatus, ulong timeout, IntPtr hAbortEvent);
        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_GetSuspendTime(IntPtr DeviceHandle, ref uint Time);
        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_SetDefaultSystemSuspendTime(IntPtr DeviceHandle, uint Time);
    }
}
