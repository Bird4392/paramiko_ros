﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// ExposureXray.xaml の相互作用ロジック
    /// </summary>
    public partial class ExposureXray : Window
    {
        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                Message window when acquiring X-ray iamge                                                              *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************
        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                                region                                                                                 =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        #region "最大化、最小化、閉じるボタンの非表示設定"

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        const int GWL_STYLE = -16;
        const int WS_SYSMENU = 0x80000;

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            IntPtr handle = new WindowInteropHelper(this).Handle;
            int style = GetWindowLong(handle, GWL_STYLE);
            style = style & (~WS_SYSMENU);
            SetWindowLong(handle, GWL_STYLE, style);
        }



        #endregion




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public bool blnUpdateDark;







        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public ExposureXray()
        {
            InitializeComponent();
            blnUpdateDark = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (blnUpdateDark)
            {
                this.Title = "Now processing";
                label.Content = "Now processing.";
            }
        }
    }
}
