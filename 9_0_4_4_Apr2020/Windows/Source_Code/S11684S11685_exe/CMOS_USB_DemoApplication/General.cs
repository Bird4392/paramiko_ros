﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace CMOS_USB_DemoApplication
{
    public class General
    {
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get version of application and retrun it as string
        public static string GetFileVersion()
        {
            // Get application version information
            System.Diagnostics.FileVersionInfo ver = System.Diagnostics.FileVersionInfo.GetVersionInfo(
                System.Reflection.Assembly.GetExecutingAssembly().Location);

            // Return file version
            return ver.FileVersion;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Screen Shot
        public static BitmapSource CopyScreen()
        {
            System.Windows.Forms.SendKeys.SendWait("%{PRTSC}");

            System.Threading.Thread.Sleep(100);

            IDataObject dobj = Clipboard.GetDataObject();
            if(dobj.GetDataPresent(DataFormats.Bitmap) == true)
            {
                System.Windows.Interop.InteropBitmap ibmp = (System.Windows.Interop.InteropBitmap)dobj.GetData(DataFormats.Bitmap);
                return ibmp;
            }

            return null;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "Device" folder.
        public static string CreateDeviceDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\Device\";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "log" folder.
        public static string CreateLogDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\log";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "Device" folder.
        public static string CreateDarkDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\Dark\";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get the system information
        public static void PcSystemInfo(out string strInfo)
        {
            strInfo = "System Information\r\n" +
                      "===============================================================================\r\n";

            System.Management.ManagementClass mcOperation = null;
            System.Management.ManagementClass mcProcessor = null;
            System.Management.ManagementClass mcComputer = null;

            try
            {
                //--------------------------------------------------------------------------------------------------------------------------------
                System.OperatingSystem os = System.Environment.OSVersion;

                mcOperation = new System.Management.ManagementClass("Win32_OperatingSystem");
                System.Management.ManagementObjectCollection moc = mcOperation.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "OS\t\t\t\t = " + mo["Caption"] + "\r\n";                            // OS
                    strInfo += "Version\t\t\t = " + mo["Version"] + mo["CSDVersion"] + "\r\n";      // Version
                    strInfo += "Memory Size\t\t = " + mo["TotalVisibleMemorySize"] + "\r\n";        // RAM
                    strInfo += "System Name\t\t = " + mo["CSName"] + "\r\n";                        // System Name
                    strInfo += "OS Manufacturer\t\t = " + mo["Manufacturer"] + "\r\n";              // OS Manufacturer

                    if (os.Version.Major >= 6)
                    {
                        strInfo += "OS Architecture\t\t = " + mo["OSArchitecture"] + "\r\n";
                    }
                }
                moc.Dispose();
                mcOperation.Dispose();

                //--------------------------------------------------------------------------------------------------------------------------------
                mcProcessor = new System.Management.ManagementClass("Win32_Processor");
                moc = mcProcessor.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "Processor\t\t\t = " + mo["Name"] + "\r\n";                            // Processor
                }
                moc.Dispose();
                mcProcessor.Dispose();

                //--------------------------------------------------------------------------------------------------------------------------------
                mcComputer = new System.Management.ManagementClass("Win32_ComputerSystem");
                moc = mcComputer.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "System Manufacturer\t = " + mo["Manufacturer"] + "\r\n";            // System Manufacturer
                    strInfo += "System Model\t\t = " + mo["Model"] + "\r\n";                        // System Model
                }
                moc.Dispose();
                mcComputer.Dispose();
            }
            finally
            {
                if (mcComputer != null) { mcComputer.Dispose(); }
                if (mcProcessor != null) { mcProcessor.Dispose(); }
                if (mcOperation != null) { mcOperation.Dispose(); }

                strInfo += "===============================================================================";
            }
        }












    }
}
