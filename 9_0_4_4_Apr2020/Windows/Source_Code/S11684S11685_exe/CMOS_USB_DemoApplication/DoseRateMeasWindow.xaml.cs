﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.ComponentModel;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;

namespace CMOS_USB_DemoApplication
{
    public partial class DoseRateMeasWindow : Window
    {
        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private constant                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private const byte MAXIMUM_DEVICE_COUNT = 3;                                                                // Set maximum number of devices being operated at a time
        private const int ACQUISITION_COUNT = 669643;                                                               // Monitor PD data acquisition count




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public string[] SensorSerial = new string[MAXIMUM_DEVICE_COUNT];
        public IntPtr[] DeviceHandle_List = new IntPtr[MAXIMUM_DEVICE_COUNT];
        public IntPtr DeviceHandle = new IntPtr(-1);




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private LineGraph TrigPD_Graph;                                                                             // dose rate graph
        private LineGraph Frequency;                                                                                // histogram graph

        private BackgroundWorker Worker_TrigPd;                                                                     // Get Trig Pd Data

        private ushort[] TrigDataBuff = new ushort[ACQUISITION_COUNT];                                              // Data buffer for storing trigger pd data

        private LogWindow windowLog = null;                                                                         // LogWindow




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public DoseRateMeasWindow()
        {
            InitializeComponent();

            // Initialization of BackgroundWorker
            Worker_TrigPd = new BackgroundWorker();
            Worker_TrigPd.DoWork += new DoWorkEventHandler(Worker_TrigPd_DoWork);
            Worker_TrigPd.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_TrigPd_RunWorkerCompleted);
            Worker_TrigPd.WorkerSupportsCancellation = true;

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";

            // Determines whether the log file exists.
            if (!LogFileExists())
            {
                // Get the system information
                string strInfo = "";
                General.PcSystemInfo(out strInfo);
                WriteLog(strInfo);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is laid out, rendered, and ready for interantion.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Log textbox clear
            txtLog.Clear();

            // Add sensor serial to combobox
            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                if (SensorSerial[i]!=null)
                    cboxSensor.Items.Add(SensorSerial[i]);
            }

            // Select the first sensor.
            cboxSensor.SelectedIndex = 0;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is about to close.
        private void Window_Closed(object sender, EventArgs e)
        {
            // Write to log file.
            WriteLog("**********************************************************************************");
            WriteLog("\"Dose rate measurement\" window closed");

            // Close Log Window
            if ((windowLog != null) && windowLog.IsActive)
            {
                windowLog = Application.Current.Windows.OfType<LogWindow>().FirstOrDefault();
                windowLog.Close();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Start button
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];
            
            // Enable only the stop button.
            ChangeEnabled();
            btnStart.Content = "Now Acquiring";

            // Write to log file.
            WriteLog("**********************************************************************************");

            // When asynchronous operation is not running.
            if (Worker_TrigPd.IsBusy == false)
            {
                // Starts execution of a background operation.
                Worker_TrigPd.RunWorkerAsync();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Exit button
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Take screenshot
        private void mnuScreenCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Desktop = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string FileName = Desktop + @"\Screenshot " + now.ToString("yyyy-MM-dd HH.mm.ss") + @".jpg";

                using (var fs = new FileStream(FileName, FileMode.Create))
                {
                    BitmapEncoder enc = new JpegBitmapEncoder();
                    BitmapSource ibmp = General.CopyScreen();

                    if (ibmp != null)
                    {
                        enc.Frames.Add(BitmapFrame.Create(ibmp));
                        enc.Save(fs);
                    }

                    fs.Close();
                }
            }
            catch
            {
                return;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Clear log text
        private void mnuClearLog_Click(object sender, RoutedEventArgs e)
        {
            txtLog.Clear();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Display "Log" window.
        private void mnuOpenLog_Click(object sender, RoutedEventArgs e)
        {
            // Confirm whethre a Contrast Window is showed.
            windowLog = Application.Current.Windows.OfType<LogWindow>().FirstOrDefault();

            if (windowLog != null)
            {
                // Attempts to bring the window to the foreground and activates it.
                windowLog.Activate();
            }
            else
            {
                // Instance of class created.
                windowLog = new LogWindow();
                windowLog.blnDose = true;
                windowLog.Show();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Manually closes a window.
        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // TextBox to the numbers only input so as not to be
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !new Regex("[0-9]").IsMatch(e.Text) && !new Regex("[.]").IsMatch(e.Text);
        }













        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Private function                                                                         =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ChangeEnabled
        ///     Enable / disable control.
        /// </summary>
        /// <param name="blnPower"></param>
        /// <param name="blnAcquire"></param>
        /// <param name="blnStop"></param>
        /// <param name="blnOther"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ChangeEnabled(bool blnEnable = false)
        {
            // Button control
            btnStart.IsEnabled = blnEnable;                 // Start button
            btnExit.IsEnabled = blnEnable;                  // Exit image button

            // Other controls
            gpbSensor.IsEnabled = blnEnable;                // Sensor serial groupbox
            grbParameters.IsEnabled = blnEnable;            // Parameters groupbox
            gpbOptions.IsEnabled = blnEnable;               // Options groupbox
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetTrigData
        ///     Getting trigger pd data
        /// </summary>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetTrigData(double AcquisitionTime, ulong bufferLength, CMOS_USB.UnitIntegrationParameter IntegParam)
        {
            uint result = 0;
            result = CMOS_USB.HPK_GetTrigPdData(DeviceHandle, AcquisitionTime, TrigDataBuff, out bufferLength, ref IntegParam);

            // Write returned value to log file.
            WriteLog("HPK_GetTrigPdData returned 0x" + result.ToString("X4"));

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Write returned value to log text.
                txtLog.AppendText("***HPK_GetTrigPdData***  returned 0x" + result.ToString("X4") + Environment.NewLine);
                txtLog.ScrollToEnd();
            }));

            // HPK_GetTrigPdData failed.
            if (result != 0)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    txtLog.AppendText("Acquiring data failed." + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));

                if (result == 0x03)
                {
                    byte[] temp1 = new byte[16];
                    ulong bufferLength1 = 0;
                    result = CMOS_USB.HPK_StopTrigPdData(DeviceHandle, temp1, out bufferLength1, ref IntegParam);
                }

                return false;
            }

            // HPK_GetTrigPdData success.
            else
            {
                byte[] temp = new byte[16];
                ulong tempLength = 0;
                result = CMOS_USB.HPK_StopTrigPdData(DeviceHandle, temp, out tempLength, ref IntegParam);

                // Write returned value to log file.
                WriteLog("HPK_StopTrigPdData returned 0x" + result.ToString("X4"));

                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    // Write returned value to log text.
                    txtLog.AppendText("***HPK_StopTrigPdData***  returned 0x" + result.ToString("X4") + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));

                // HPK_StopTrigPdData success.
                if (result == 0) {  return true; }

                // HPK_StopTrigPdData failed.
                else
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        txtLog.AppendText("Error occurred at Finish Phase of Monitor photodiode Reading.");
                        txtLog.ScrollToEnd();
                    }));

                    return false;
                }
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// SearchCount
        ///     Get value of the Histogram
        /// </summary>
        /// <param name="N"></param>
        /// <param name="Counter"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="max_count"></param>
        /// <param name="max_point"></param>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void SearchCount(int N, int[] Counter, ref double max, ref double min, ref int max_count, ref int max_point)
        {
            for (int i = 0; i < N; i++)
            {
                if (TrigDataBuff[i] <= 0) { continue; }

                if (TrigDataBuff[i] > max) { max = TrigDataBuff[i]; }
                if (TrigDataBuff[i] < min) { min = TrigDataBuff[i]; }
            }

            for (int i = 0; i < 1024; i++)
            {
                if (Counter[i] > max_count)
                {
                    max_count = Counter[i];
                    max_point = i;
                }
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CalcDoseRate
        ///     Calculation dose rate
        /// </summary>
        /// <param name="iCoffOfTotalDose">coeffient of total dose</param>
        /// <param name="AcquisitionTime">Acquisition time [s]</param>
        /// <param name="Sensitivity">Sensitivity</param>
        /// <param name="Offset">ADC offset mode</param>
        /// <param name="TotalDose">Total dose</param>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CalcDoseRate(int iCoffOfTotalDose, double AcquisitionTime, double Sensitivity, double Offset, ref double TotalDose)
        {
            int iCount = 0;
            for (int i = 0; i < ACQUISITION_COUNT; i++)
            {
                if (TrigDataBuff[i] != 0) { iCount++; }
            }

            int j = 0;
            int k = 0;
            int N = ACQUISITION_COUNT;
            double[] x = new double[iCount];
            double[] y = new double[iCount];
            short TrigGain = 0;
            double max_dose = 0;

            while (k < N)
            {
                if (TrigDataBuff[k] != 0 && TrigDataBuff[k] < 1024)
                {
                    // 1 Trig data takes 47.48 usec @15MHz.
                    x[j] = iCoffOfTotalDose * 0.00024 * (j + 1);

                    // Background level of TrigPD is around 430 LSB.
                    TrigGain = (short)(TrigDataBuff[k] - Offset);

                    if (TrigGain < 0) { TrigGain = 0; }

                    /* This is [uGy/ms]. TrigPD data to Dose rate. */
                    y[j] = TrigGain / Sensitivity;

                    // y[k] * 0.00048 has dimension of [uGy].
                    TotalDose = TotalDose + y[j]/*[uGy/ms]*/ * iCoffOfTotalDose * 0.24/*[ms]*/;
                    j++;
                    k++;

                    if (max_dose < TrigGain / Sensitivity)
                    {
                        max_dose = TrigGain / Sensitivity;
                    }
                }
                else
                {
                    k++;
                }
            }

            /* shows dose rate */
            var xDataSource = x.AsXDataSource();
            var yDataSource = y.AsYDataSource();

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                CompositeDataSource compositeDataSource = xDataSource.Join(yDataSource);
                chpXrayDose.Visible = new Rect(0, 0, iCount, 1024);
                chpXrayDose.Children.Remove(TrigPD_Graph);
                chpXrayDose.FitToView();
                TrigPD_Graph = chpXrayDose.AddLineGraph(compositeDataSource, Colors.Blue, 0.5D, "Trig. PD Data");

                // Resize graph
                double dCoorX = AcquisitionTime * -0.1f;
                double dCoorY = max_dose * -0.1f;
                chpXrayDose.Visible = new Rect(dCoorX, dCoorY, AcquisitionTime - (dCoorX * 2), max_dose - (dCoorY * 3));
                chpXrayDose.LegendVisible = false;
            }));

        }










        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CreateDoseDirectoryName
        ///     It returns the full path to "dose" folder.
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private string CreateDoseDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\dose";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// LogFileExists
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool LogFileExists()
        {
            DateTime LogTime = DateTime.Now;
            string Directory = General.CreateLogDirectoryName();
            string logFileName = Directory + "\\" + "Log_Dose_(" + LogTime.ToString("ddMMyy") + ").txt";

            // Determines whether the specified file exists.
            if (File.Exists(logFileName))
            {
                return true;
            }

            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WriteLog
        /// </summary>
        /// <param name="WrittenWords"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void WriteLog(String WrittenWords)
        {
            if (App.blnWriteLog)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    StreamWriter LogText = null;
                    try
                    {
                        // Get current time.
                        DateTime LogTime = DateTime.Now;

                        // Path
                        string logFilePath = General.CreateLogDirectoryName();

                        // Open Log, and add line.
                        LogText = new StreamWriter(logFilePath + "\\" + "Log_Dose_(" + LogTime.ToString("ddMMyy") + ").txt", true, Encoding.GetEncoding("Shift-JIS"));

                        // Create an instance
                        System.IO.StringReader rs = new System.IO.StringReader(WrittenWords);

                        // Respeated until the end of the stream.
                        while (rs.Peek() > -1)
                        {
                            // Write log.
                            LogText.WriteLine(LogTime.ToString("HH:mm:ss.fff") + " " + rs.ReadLine());
                        }
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(WrittenWords + "\r\n" + exp.Message);
                    }
                    finally
                    {
                        // Close log.
                        if (LogText != null) { LogText.Close(); }
                    }
                }));
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GoToSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GoToSuspend()
        {
            // Suspend delay 1 [sec]
            uint time = 1000;

            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // USB_SuspendDevice
            uint result = CMOS_USB.USB_SuspendDevice(DeviceHandle, time);

            // Write returned value to log file.
            WriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4"));
        }
        
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// StopSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void StopSuspend()
        {
            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // USB_ResumeDevice
            uint result = CMOS_USB.USB_ResumeDevice(DeviceHandle);

            // Write returned value to log file.
            WriteLog("USB_ResumeDevice returned 0x" + result.ToString("X4"));
        }







        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Thread function                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_TrigPd_DoWork(object sender, DoWorkEventArgs e)
        {
            // Declaring variables
            int iCoffOfTotalDose = 0;       // coeffient of total dose
            double AcquisitionTime = 0;     // Acquisition time [s]
            double Sensitivity = 0;         // Sensitivity
            double Offset = 0;              // ADC offset mode
            double TotalDose = 0;           // Total dose
            ulong bufferLength = 0;
            bool blnSuccess = false;

            CMOS_USB.UnitIntegrationParameter IntegParam;
            IntegParam.Xray_Incident_Threshold = 440;
            IntegParam.Xray_Integration_End_Threshold = 435;
            IntegParam.Integration_Time = 500;

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Stop Suspend mode.
                StopSuspend();

                // Get to coeffient of total dose
                iCoffOfTotalDose = (cboxFactor.SelectedIndex == 0) ? 1 : 2;

                // Get the parameter
                AcquisitionTime = double.Parse(txtAcqTime.Text);
                Sensitivity = int.Parse(txtSensitivity.Text);
                Offset = int.Parse(txtAdcOffset.Text);

                // Zero initialization
                TrigDataBuff = new ushort[ACQUISITION_COUNT];
            }));

            DateTime now = DateTime.Now;

            // Getting trigger pd data
            blnSuccess = GetTrigData(AcquisitionTime, bufferLength, IntegParam);

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // GetTrigData successed.
            if (blnSuccess)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    if (Sensitivity < 0 || Offset < 0)
                    {
                        txtLog.AppendText("Sensitivity or ADC offset mode of monitoring PD is invalid." + Environment.NewLine);
                        txtLog.ScrollToEnd();
                        Sensitivity = 0;
                        Offset = 0;
                    }
                }));

                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // Calculation dose rate
                CalcDoseRate(iCoffOfTotalDose, AcquisitionTime, Sensitivity, Offset, ref TotalDose);


                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // Calculation histogram
                int N = ACQUISITION_COUNT;
                int[] Counter = new int[1024];

                for (int i = 0; i < N; i++)
                {
                    if (TrigDataBuff[i] <= 0 || TrigDataBuff[i] >= 1024) { continue; }

                    Counter[(int)TrigDataBuff[i]]++;
                }

                // variable of the Histogram
                double max = 0;                     // max value
                double min = 1023;                  // min value
                int max_count = 0;                  // Offset Peak
                int max_point = 0;                  // Offset
                int iHistMaxCount = 0;              // max value show graph

                // Get value of the Histogram
                SearchCount(N, Counter, ref max, ref min, ref max_count, ref max_point);

                // Get max value of the graph
                iHistMaxCount = max_count;

                // If max value more than 440.
                if (max >= 440)
                {
                    iHistMaxCount = 0;

                    for (int i = 440; i < 1024; i++)
                    {
                        if (Counter[i] > iHistMaxCount) { iHistMaxCount = Counter[i]; }
                    }
                }

                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    /* shows histogram */
                    EnumerableDataSource<int> xx = new EnumerableDataSource<int>(Enumerable.Range(0, 1024).ToArray());
                    xx.SetXMapping(_xx => _xx);
                    Func<int, double> linearMapping = i => i;
                    Func<int, double> mapping;
                    mapping = linearMapping;

                    EnumerableDataSource<int> count;
                    count = new EnumerableDataSource<int>(Counter);
                    count.SetYMapping(mapping);

                    CompositeDataSource cDataSource = new CompositeDataSource(xx, count);
                    chpHistogram.Children.Remove(Frequency);
                    Frequency = chpHistogram.AddLineGraph(cDataSource, Colors.Red, 2, "Frequency");
                    chpHistogram.LegendVisible = false;
                    chpHistogram.FitToView();

                    // Resize graph
                    int iCoorY = (int)(iHistMaxCount * -0.1M);
                    if (max < 440) { chpHistogram.Visible = new Rect(min - 5, iCoorY, max - min + 11, iHistMaxCount - (iCoorY * 3)); }
                    else { chpHistogram.Visible = new Rect(min - 20, iCoorY, max - min + 50, iHistMaxCount - (iCoorY * 3)); }

                    txtLog.AppendText("Acquiring data succeeded. " + Environment.NewLine);
                    txtLog.AppendText("Maximum:" + max.ToString() + "   Minimum:" + min.ToString() + Environment.NewLine);
                    txtLog.AppendText(Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));

                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                string ResultMessage = "";
                int kaigyou = 0;
                for (int i = (int)min; i < (int)max + 1; i++)
                {
                    kaigyou++;
                    ResultMessage += string.Format("{0, 4}", i) + " : " + string.Format("{0, 7}", Counter[i]);
                    if (kaigyou % 2 == 0)
                    {
                        ResultMessage += Environment.NewLine;
                    }
                    else
                    {
                        ResultMessage += "\t";
                        if (i == (int)max)
                        {
                            ResultMessage += Environment.NewLine;
                        }
                    }
                }

                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    txtLog.AppendText(ResultMessage);
                    txtLog.AppendText("Dose: " + TotalDose.ToString("F1") + "uGy" + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));
                
                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // Create csv file of trigger pd data

                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    // Get the sensor serial
                    string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

                    // Folder path of csv file.
                    string folderPath = CreateDoseDirectoryName();

                    // Full path of csv file.
                    string filePath = folderPath + @"\SN" + SensorSerial + "_Monitor_PD_Data.csv";

                    using (System.IO.StreamWriter sw2 = new StreamWriter(filePath, false, Encoding.GetEncoding("ASCII")))
                    {
                        sw2.WriteLine(now.ToString());

                        // Export time-series data
                        if (chkExportData.IsChecked == true)
                        {
                            sw2.WriteLine("Time-series data,Monitor PD value,Frequency");

                            for (int i = 0; i < ACQUISITION_COUNT; i++)
                            {
                                if (TrigDataBuff[i] <= 0 && i >= 1024) { continue; }

                                if (i < 1024)
                                {
                                    sw2.Write(TrigDataBuff[i].ToString() + "," + i.ToString() + "," + Counter[i].ToString());
                                }
                                else
                                {
                                    sw2.Write(TrigDataBuff[i].ToString());
                                }

                                sw2.WriteLine();
                            }
                        }

                        // not export time-series data
                        else
                        {
                            sw2.WriteLine("Monitor PD value,Frequency");


                            for (int i = 0; i < 1024; i++)
                            {
                                sw2.Write(i.ToString() + "," + Counter[i].ToString());

                                // Add a new line.
                                sw2.WriteLine();
                            }
                        }
                    }

                }));
            }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_TrigPd_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Enables the suspended state.
            GoToSuspend();

            // Enable other than stop button.
            ChangeEnabled(true);
            btnStart.Content = "Start";

            // Add a new line.
            txtLog.AppendText(Environment.NewLine);
        }
    }
}
