using System;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;

namespace WinUsbSample
{
	/// <summary>
	///  These declarations are translated from the C declarations in various files
	///  in the Windows DDK. The files are:
	///  
	///  winddk\6001\inc\api\usb.h
	///  winddk\6001\inc\api\usb100.h
	///  winddk\6001\inc\api\winusbio.h
	///  
	///  (your home directory and release number may vary)
	/// <summary>

	sealed internal partial class WinUsbDevice
	{
		internal const UInt32 DEVICE_SPEED = ((UInt32)(1));
		internal const Byte USB_ENDPOINT_DIRECTION_MASK = ((Byte)(0X80));

		internal enum POLICY_TYPE
		{
			SHORT_PACKET_TERMINATE = 1,
			AUTO_CLEAR_STALL,
			PIPE_TRANSFER_TIMEOUT,
			IGNORE_SHORT_PACKETS,
			ALLOW_PARTIAL_READS,
			AUTO_FLUSH,
			RAW_IO,
		}

		internal enum USBD_PIPE_TYPE
		{
			UsbdPipeTypeControl,
			UsbdPipeTypeIsochronous,
			UsbdPipeTypeBulk,
			UsbdPipeTypeInterrupt,
		}

		internal enum USB_DEVICE_SPEED
		{
			UsbLowSpeed = 1,
			UsbFullSpeed,
			UsbHighSpeed,
		}

		[StructLayout(LayoutKind.Sequential)]
		internal struct USB_CONFIGURATION_DESCRIPTOR
		{
			internal Byte bLength;
			internal Byte bDescriptorType;
			internal ushort wTotalLength;
			internal Byte bNumInterfaces;
			internal Byte bConfigurationValue;
			internal Byte iConfiguration;
			internal Byte bmAttributes;
			internal Byte MaxPower;
		}

		[StructLayout(LayoutKind.Sequential)]
		internal struct USB_INTERFACE_DESCRIPTOR
		{
			internal Byte bLength;
			internal Byte bDescriptorType;
			internal Byte bInterfaceNumber;
			internal Byte bAlternateSetting;
			internal Byte bNumEndpoints;
			internal Byte bInterfaceClass;
			internal Byte bInterfaceSubClass;
			internal Byte bInterfaceProtocol;
			internal Byte iInterface;
		}

		[StructLayout(LayoutKind.Sequential)]
		internal struct WINUSB_PIPE_INFORMATION
		{
			internal USBD_PIPE_TYPE PipeType;
			internal Byte PipeId;
			internal ushort MaximumPacketSize;
			internal Byte Interval;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		internal struct WINUSB_SETUP_PACKET
		{
			internal Byte RequestType;
			internal Byte Request;
			internal ushort Value;
			internal ushort Index;
			internal ushort Length;
		}

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_ControlTransfer(IntPtr InterfaceHandle, WINUSB_SETUP_PACKET SetupPacket, Byte[] Buffer, UInt32 BufferLength, ref UInt32 LengthTransferred, IntPtr Overlapped);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_Free(IntPtr InterfaceHandle);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_Initialize(SafeFileHandle DeviceHandle, ref IntPtr InterfaceHandle);

		//  Use this declaration to retrieve DEVICE_SPEED (the only currently defined InformationType).

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_QueryDeviceInformation(IntPtr InterfaceHandle, UInt32 InformationType, ref UInt32 BufferLength, ref Byte Buffer);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_QueryInterfaceSettings(IntPtr InterfaceHandle, Byte AlternateInterfaceNumber, ref USB_INTERFACE_DESCRIPTOR UsbAltInterfaceDescriptor);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_QueryPipe(IntPtr InterfaceHandle, Byte AlternateInterfaceNumber, Byte PipeIndex, ref WINUSB_PIPE_INFORMATION PipeInformation);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_ReadPipe(IntPtr InterfaceHandle, Byte PipeID, Byte[] Buffer, UInt32 BufferLength, ref UInt32 LengthTransferred, IntPtr Overlapped);

		//  Two declarations for WinUsb_SetPipePolicy. 
		//  Use this one when the returned Value is a Byte (all except PIPE_TRANSFER_TIMEOUT):

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_SetPipePolicy(IntPtr InterfaceHandle, Byte PipeID, UInt32 PolicyType, UInt32 ValueLength, ref Byte Value);

		//  Use this alias when the returned Value is a UInt32 (PIPE_TRANSFER_TIMEOUT only):

		[DllImport("winusb.dll", SetLastError = true, EntryPoint = "WinUsb_SetPipePolicy")]
		internal static extern bool WinUsb_SetPipePolicy1(IntPtr InterfaceHandle, Byte PipeID, UInt32 PolicyType, UInt32 ValueLength, ref UInt32 Value);

		[DllImport("winusb.dll", SetLastError = true)]
		internal static extern bool WinUsb_WritePipe(IntPtr InterfaceHandle, Byte PipeID, Byte[] Buffer, UInt32 BufferLength, ref UInt32 LengthTransferred, IntPtr Overlapped);
	}

    ///<summary >
    // API declarations relating to device management (SetupDixxx and 
    // RegisterDeviceNotification functions).   
    /// </summary>

    sealed internal partial class DeviceManagement
    {
        // from dbt.h

        internal const Int32 DBT_DEVICEARRIVAL = 0X8000;
        internal const Int32 DBT_DEVICEREMOVECOMPLETE = 0X8004;
        internal const Int32 DBT_DEVTYP_DEVICEINTERFACE = 5;
        internal const Int32 DBT_DEVTYP_HANDLE = 6;
        internal const Int32 DEVICE_NOTIFY_ALL_INTERFACE_CLASSES = 4;
        internal const Int32 DEVICE_NOTIFY_SERVICE_HANDLE = 1;
        internal const Int32 DEVICE_NOTIFY_WINDOW_HANDLE = 0;
        internal const Int32 WM_DEVICECHANGE = 0X219;

        // from setupapi.h

        internal const Int32 DIGCF_PRESENT = 2;
        internal const Int32 DIGCF_DEVICEINTERFACE = 0X10;

        // Two declarations for the DEV_BROADCAST_DEVICEINTERFACE structure.

        // Use this one in the call to RegisterDeviceNotification() and
        // in checking dbch_devicetype in a DEV_BROADCAST_HDR structure:

        [StructLayout(LayoutKind.Sequential)]
        internal class DEV_BROADCAST_DEVICEINTERFACE
        {
            internal Int32 dbcc_size;
            internal Int32 dbcc_devicetype;
            internal Int32 dbcc_reserved;
            internal Guid dbcc_classguid;
            internal Int16 dbcc_name;
        }

        // Use this to read the dbcc_name String and classguid:

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        internal class DEV_BROADCAST_DEVICEINTERFACE_1
        {
            internal Int32 dbcc_size;
            internal Int32 dbcc_devicetype;
            internal Int32 dbcc_reserved;
            [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 16)]
            internal Byte[] dbcc_classguid;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 255)]
            internal Char[] dbcc_name;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal class DEV_BROADCAST_HDR
        {
            internal Int32 dbch_size;
            internal Int32 dbch_devicetype;
            internal Int32 dbch_reserved;
        }

        internal struct SP_DEVICE_INTERFACE_DATA
        {
            internal Int32 cbSize;
            internal System.Guid InterfaceClassGuid;
            internal Int32 Flags;
            internal IntPtr Reserved;
        }
/*
        internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
        {
            internal Int32 cbSize;
            internal String DevicePath;
        }

        internal struct SP_DEVINFO_DATA
        {
            internal Int32 cbSize;
            internal System.Guid ClassGuid;
            internal Int32 DevInst;
            internal Int32 Reserved;
        }
*/
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr RegisterDeviceNotification(IntPtr hRecipient, IntPtr NotificationFilter, Int32 Flags);

        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern Int32 SetupDiCreateDeviceInfoList(ref System.Guid ClassGuid, Int32 hwndParent);

        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern Int32 SetupDiDestroyDeviceInfoList(IntPtr DeviceInfoSet);

        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern bool SetupDiEnumDeviceInterfaces(IntPtr DeviceInfoSet, IntPtr DeviceInfoData, ref System.Guid InterfaceClassGuid, Int32 MemberIndex, ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData);

        [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr SetupDiGetClassDevs(ref System.Guid ClassGuid, IntPtr Enumerator, IntPtr hwndParent, Int32 Flags);

        [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern bool SetupDiGetDeviceInterfaceDetail(IntPtr DeviceInfoSet, ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData, IntPtr DeviceInterfaceDetailData, Int32 DeviceInterfaceDetailDataSize, ref Int32 RequiredSize, IntPtr DeviceInfoData);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool UnregisterDeviceNotification(IntPtr Handle);
    }

    ///  <summary>
    ///  API declarations relating to file I/O (and used by WinUsb).
    ///  </summary>

    sealed internal class FileIO
    {
        internal const Int32 FILE_ATTRIBUTE_NORMAL = 0X80;
        internal const Int32 FILE_FLAG_OVERLAPPED = 0X40000000;
        internal const Int32 FILE_SHARE_READ = 1;
        internal const Int32 FILE_SHARE_WRITE = 2;
        internal const UInt32 GENERIC_READ = 0X80000000;
        internal const UInt32 GENERIC_WRITE = 0X40000000;
        internal const Int32 INVALID_HANDLE_VALUE = -1;
        internal const Int32 OPEN_EXISTING = 3;

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern SafeFileHandle CreateFile(String lpFileName, UInt32 dwDesiredAccess, Int32 dwShareMode, IntPtr lpSecurityAttributes, Int32 dwCreationDisposition, Int32 dwFlagsAndAttributes, Int32 hTemplateFile);
    }
}
