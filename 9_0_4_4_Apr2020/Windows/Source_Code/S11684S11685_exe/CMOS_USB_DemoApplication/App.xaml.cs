﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public static bool blnWriteLog { get; private set; }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Raises the Exit event.
        /// </summary>
        /// <param name="e"></param>An ExitEventArgs that contains the event data.
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Raises the Startup event.
        /// </summary>
        /// <param name="e"></param>A StartupEventArgs that contains the event data.
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // initialization of variables
            blnWriteLog = true;

            //-----------------------------------------------------------------------------------------------------------------
            // Check comand line argument
            foreach (string arg in e.Args)
            {
                if (!CheckComandLineArgument(arg)) { this.Shutdown(); return; }
            }

            //-----------------------------------------------------------------------------------------------------------------
            // Create path of Dll files.
            string appPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            int LastIndex = appPath.LastIndexOf("\\");
            string strDddFile = appPath.Substring(0, LastIndex + 1) + "DynamicDataDisplay.dll";         // Path of DynamicDataDisplay.dll file.
            string strCuFile = appPath.Substring(0, LastIndex + 1) + "CMOS_USB.dll";                    // Path of CMOS_USB.dll file.

            //-----------------------------------------------------------------------------------------------------------------
            // Check whether the DynamicDataDisplay.dll file And the CMOS_USB.dll file.
            if ((!System.IO.File.Exists(strDddFile)) && (!System.IO.File.Exists(strCuFile)))
            {
                MessageBox.Show("\"DynamicDataDisplay.dll\" and \"CMOS_USB.dll\" are not found!!\r\n" +
                                "You need to put these files in the same directory of the application.\r\n" +
                                "Please refer to readme.txt attached to CD-ROM in order to know where these files are.");
                this.Shutdown();
                return;

            }
            
            //-----------------------------------------------------------------------------------------------------------------
            // Check whether the DynamicDataDisplay.dll file.
            else if (!System.IO.File.Exists(strDddFile))
            {
                MessageBox.Show("\"DynamicDataDisplay.dll\" is not found!!\r\n" +
                                "You need to put the file in the same directory of the application.\r\n" +
                                "Please refer to readme.txt attached to CD-ROM in order to know where the file is.");
                this.Shutdown();
                return;
            }

            //-----------------------------------------------------------------------------------------------------------------
            // Check whether the CMOS_USB.dll file.
            else if (!System.IO.File.Exists(strCuFile))
            {
                MessageBox.Show("\"CMOS_USB.dll\" is not found!!\r\n" +
                                "You need to put the file in the same directory of the application.\r\n" +
                                "Please refer to readme.txt attached to CD-ROM in order to know where the file is.");
                this.Shutdown();
                return;
            }

            //-----------------------------------------------------------------------------------------------------------------
            // Display the main window.
            bool bln = false;
            MainWindow window = new MainWindow(ref bln);
            if (bln) { window.Show(); }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Check comand line argument
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool CheckComandLineArgument(string arg)
        {
            bool blnContinu = true;
            switch (arg)
            {
                case "/log-on":
                    blnWriteLog = true;
                    break;

                case "/log-off":
                    blnWriteLog = false;
                    break;

                default:
                    MessageBox.Show("A command line argument is wrong." + Environment.NewLine +
                                    "Please enter a right command line argument.",
                                    "Wrong argument!", MessageBoxButton.OK, MessageBoxImage.Error);
                    blnContinu = false;
                    break;
            }

            return blnContinu;
        }
    }
}
