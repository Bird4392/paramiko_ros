﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// ImgAcqSetWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ImgAcqSetWindow : Window
    {

        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                   Image Acquisition Settings Window                                                                   *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************

        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public CollStruct.UintImageAcq ImageAcq = new CollStruct.UintImageAcq();              // Image Acquisition Settings
        public string SensorSerial;




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public ImgAcqSetWindow()
        {
            InitializeComponent();

            // Initialize
            SensorSerial = "";

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";

            // Show number of cal files.
            ShowCalFiles();
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // It reflects the current set value.
            cmbModeSelect.SelectedIndex = ImageAcq.Mode - 2;                        // Integration mode
            cmbImgCorrType.SelectedIndex = ImageAcq.ImgCorrType;                    // Image correction type
            chkSubStoredImage.IsChecked = ImageAcq.SubStoredImage;                  // Apply dark subtraction with stored dark image
            rdbCorrImgAcq.IsChecked = (ImageAcq.AcqTimes == 0) ? true : false;      // Correction images to acquire     [0: 3, 1: 10]
            rdbCorrImgAcq1.IsChecked = (ImageAcq.AcqTimes == 0) ? false : true;     //
            txtStartThreshold.Text = ImageAcq.StartThreshold.ToString();            // Threshold to start integration
            txtIntegTime.Text = ImageAcq.IntegTime.ToString();                      // Integration time [ms]
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void cmbModeSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Enable all items.
            ((ComboBoxItem)cmbImgCorrType.Items[0]).IsEnabled = true;       // RAW image
            ((ComboBoxItem)cmbImgCorrType.Items[1]).IsEnabled = true;       // Dark-subtracted image
            ((ComboBoxItem)cmbImgCorrType.Items[2]).IsEnabled = true;       // Flat field corrected image
            txtStartThreshold.IsEnabled = true;                             // Threshold to start integration

            switch (((ComboBox)sender).SelectedIndex)
            {
                // Mode2
                case 0:
                    txtIntegMode.Text = "The sensor automatically determines the start of integration;" + Environment.NewLine +
                                        "however, the integration time has to be set by the operator.";
                    
                    if(cmbImgCorrType.SelectedIndex == 1)
                    {
                        chkSubStoredImage.IsEnabled = true;
                        chkSubStoredImage.IsChecked = true;
                    }
                    
                    break;

                // Mode3
                case 1:
                    txtIntegMode.Text = "This mode is suitable for acquiring dark images." + Environment.NewLine +
                                        "The integration immediately starts when \"Aqcuire image\"" + Environment.NewLine +
                                        "button is pushed. The integration time has to be set by the operator.";
                    
                    // Select item "RAW image".
                    cmbImgCorrType.SelectedIndex = 0;
                    
                    // Disable "Threshold to start integration". 
                    txtStartThreshold.IsEnabled = false;

                    // Enable only the "RAW image".
                    ((ComboBoxItem)cmbImgCorrType.Items[1]).IsEnabled = false;      // Dark-subtracted image
                    ((ComboBoxItem)cmbImgCorrType.Items[2]).IsEnabled = false;      // Flat field corrected image
                    break;

                // do nothing
                default:
                    break;
            }
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void cmbImgCorrType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // For "Apply dark subtraction with stored dark image".
            if(cmbImgCorrType.SelectedIndex == 1 && cmbModeSelect.SelectedIndex == 0)
            {
                // Enable "Apply dark subtraction with stored dark image".
                chkSubStoredImage.IsEnabled = true;
                chkSubStoredImage.IsChecked = true;
            }
            else
            {
                // Disable "Apply dark subtraction with stored dark image".
                chkSubStoredImage.IsEnabled = false;
                chkSubStoredImage.IsChecked = false;
            }

            // Choose image correction type
            if ((cmbImgCorrType.SelectedIndex != 2) || (cmbModeSelect.SelectedIndex == 1))
            {
                grbCorrImage.IsEnabled = false;
            }
            else
            {
                grbCorrImage.IsEnabled = true;
            }

        }
        
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // [ Create a correction file ] button
        private void btnCreateCalFile_Click(object sender, RoutedEventArgs e)
        {
            // convert to numbers.
            // Threshold to start integration
            if ((!ushort.TryParse(txtStartThreshold.Text, out ImageAcq.StartThreshold)) ||
                (!double.TryParse(txtIntegTime.Text, out ImageAcq.IntegTime)))
            {
                MessageBox.Show("Please input an integer in \"Parameters\".",
                                "Error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            // Check the value is within the range.
            if (((ImageAcq.StartThreshold < 0) || (1023 < ImageAcq.StartThreshold)) ||
               ((ImageAcq.EndThreshold < 0) || (1023 < ImageAcq.EndThreshold)))
            {
                MessageBox.Show("Please input a value ranging from 0 to 1023 in \"Threshold\".",
                                "Error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            // Determines whether the cal file exists.
            if (CalFileExists())
            {
                MessageBoxResult result = MessageBox.Show("The gain frame for \"SN" + SensorSerial + "\" already exists." + Environment.NewLine +
                                                          "Would you like to continue?",
                                                          "Information", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                // In the calse of "No".
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }
            
            /***** Update settings. *****/
            ImageAcq.Mode = (byte)(cmbModeSelect.SelectedIndex + 2);            // Integration mode
            ImageAcq.ImgCorrType = (byte)cmbImgCorrType.SelectedIndex;          // Image correction type
            ImageAcq.SubStoredImage = (bool)chkSubStoredImage.IsChecked;        // Apply dark subtraction with stored dark image
            ImageAcq.AcqTimes = (rdbCorrImgAcq.IsChecked == true) ? 0 : 1;      // Correction images to acquire     [0: 3, 1: 10]
            ImageAcq.CreateCalFile = true;                                      // Create a correction file         [true]

            // Set the results of the dialog box.
            DialogResult = true;
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // [ Ok ] button
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            // Determines whether the cal file exists.
            if ((cmbImgCorrType.SelectedIndex == 0x02) && !CalFileExists())
            {
                MessageBoxResult result = MessageBox.Show("The gain frame for \"SN" + SensorSerial + "\" not found." + Environment.NewLine +
                                                          "Please click \"Create a gain frame\" button and make a gain frame." + Environment.NewLine,
                                                          "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            /***** Update settings. *****/
            ImageAcq.Mode = (byte)(cmbModeSelect.SelectedIndex + 2);            // Integration mode
            ImageAcq.ImgCorrType = (byte)cmbImgCorrType.SelectedIndex;          // Image correction type
            ImageAcq.SubStoredImage = (bool)chkSubStoredImage.IsChecked;        // Apply dark subtraction with stored dark image
            ImageAcq.AcqTimes = (rdbCorrImgAcq.IsChecked == true) ? 0 : 1;      // Correction images to acquire     [0: 3, 1: 10]
            ImageAcq.CreateCalFile = false;                                     // Create a correction file     [False]

            // convert to numbers.
            // Threshold to start integration
            if ((!ushort.TryParse(txtStartThreshold.Text, out ImageAcq.StartThreshold)) ||
                (!double.TryParse(txtIntegTime.Text, out ImageAcq.IntegTime)))
            {
                MessageBox.Show("Please input an integer in \"Parameters\".",
                                "Error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            // Check the value is within the range.
            if (((ImageAcq.StartThreshold < 0) || (1023 < ImageAcq.StartThreshold)) ||
               ((ImageAcq.EndThreshold < 0) || (1023 < ImageAcq.EndThreshold)))
            {
                MessageBox.Show("Please input a value ranging from 0 to 1023 in \"Threshold\".",
                                "Error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            // Set the results of the dialog box.
            DialogResult = true;
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // TextBox to the numbers only input so as not to be
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !new Regex("[0-9]").IsMatch(e.Text) && !new Regex("[.]").IsMatch(e.Text);
        }






        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Private function                                                                         =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ShowCalFiles
        ///     Show number of cal files.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ShowCalFiles()
        {
            // full path of "device" folder.
            string folder = General.CreateDeviceDirectoryName();

            // Get the total number of files that meet specified criteria.
            int fileCount = System.IO.Directory.GetFiles(folder, "*.CAL", System.IO.SearchOption.TopDirectoryOnly).Length;

            // Show total number of cal files.
            lblTotalCalFile.Content = fileCount.ToString() + " gain frame(s) found";
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CalFileExists
        ///     Determines whether the cal file exists.
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool CalFileExists()
        {
            string fullPath = ImageAcq.PathOfCalFile + ".Cal";

            // Determines whether the specified file exists.
            if (System.IO.File.Exists(fullPath))
            {
                return true;
            }

            return false;
        }

    }
}
