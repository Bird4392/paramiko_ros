﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// SelectCorrWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SelectCorrWindow : Window
    {
        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                        Select Correction Window                                                                       *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************

        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private constant                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        /* Constants for Size1 sensor */
        private const int SIZE_HORIZONTAL = 1000;                                                                   /* Horizontal size of image */
        private const int SIZE_VERTICAL = 1506;                                                                     /* Vertical size of image */
        private const int SIZE_IMAGE = SIZE_HORIZONTAL * SIZE_VERTICAL;                                             /* Size of monochrome image */
        private const int Buffer_Size = SIZE_IMAGE * 2 + 16;
        private const int SIZE1_CUMULATIVE_FREQUENCY = 5000;                                                        /* Cumulative frequency */

        /* Constants for Size2 sensor */
        private const int SIZE2_HORIZONTAL = 1300;                                                                  /* Horizontal size of image */
        private const int SIZE2_VERTICAL = 1706;                                                                    /* Vertical size of image */
        private const int SIZE2_IMAGE = SIZE2_HORIZONTAL * SIZE2_VERTICAL;                                          /* Size of monochrome image */
        private const int Buffer2_Size = SIZE2_IMAGE * 2 + 16;
        private const int SIZE2_CUMULATIVE_FREQUENCY = 10000;                                                       /* Cumulative frequency */
        

        private const Int32 WM_DEVICECHANGE = 0X219;                                                                /* This message comes when USB connection changes. */
        private const Int32 DBT_DEVICEREMOVECOMPLETE = 0X8004;                                                      /* disconnection of the sensor*/
        private const Int32 DBT_DEVICEARRIVAL = 0X8000;                                                             /* connection of the sensor*/

        private const int SENSOR_MAX = 256;                                                                         /* The maximum that sensor can use. */
        private const int MAX_RANGE = 65535;                                                                        /* The maximum that application can use. */
        private const int MSB_16BIT = 32768;                                                                        /* MSB or 16bit */



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public CollStruct.ImageCorrection ImageCorr;
        public byte[] pixContrastImage = new byte[SIZE2_IMAGE];                                                     /* Pixel array for original image data. */
        public bool blnSize;                                                                                        /* Device size flag.    true : Size1,  false : Size2 */





        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private Brush def;
        private Brush select;
        
        private byte[] pixGammaCorrImage1 = new byte[SIZE2_IMAGE];                                                  /* Pixel array for gamma1 correction image data. */
        private byte[] pixGammaCorrImage2 = new byte[SIZE2_IMAGE];                                                  /* Pixel array for gamma2 correction image data. */
        private byte[] pixGammaCorrImage3 = new byte[SIZE2_IMAGE];                                                  /* Pixel array for gamma3 correction image data. */
        private byte[] gamma1 = new byte[SENSOR_MAX];                                                               /* gamma1 table */
        private byte[] gamma2 = new byte[SENSOR_MAX];                                                               /* gamma2 table */
        private byte[] gamma3 = new byte[SENSOR_MAX];                                                               /* gamma3 table */

        private bool mblnGamma1;                                                                                    /* gamma_table_1.csv reading result flag. */
        private bool mblnGamma2;                                                                                    /* gamma_table_2.csv reading result flag. */
        private bool mblnGamma3;                                                                                    /* gamma_table_3.csv reading result flag. */



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public SelectCorrWindow()
        {
            InitializeComponent();

            def = new SolidColorBrush(Color.FromArgb(0xFF, 0xB0, 0xC4, 0xDE));
            select = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x00, 0x00));

            border.BorderBrush = def;
            border1.BorderBrush = def;
            border2.BorderBrush = def;
            border3.BorderBrush = def;
            
            this.Height = SystemParameters.WorkArea.Height;
            this.Width = SystemParameters.WorkArea.Width;
            this.ResizeMode = ResizeMode.NoResize;
            this.Top = 0;
            this.Left = 0;

            mblnGamma1 = ReadGammaTable(ref gamma1, "gamma_table_1.csv");
            mblnGamma2 = ReadGammaTable(ref gamma2, "gamma_table_2.csv");
            mblnGamma3 = ReadGammaTable(ref gamma3, "gamma_table_3.csv");

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShowAllImage();

            switch (ImageCorr)
            {
                case CollStruct.ImageCorrection.Raw:
                    border.BorderBrush = select;
                    break;

                case CollStruct.ImageCorrection.Gamma1:
                    border1.BorderBrush = select;
                    break;

                case CollStruct.ImageCorrection.Gamma2:
                    border2.BorderBrush = select;
                    break;

                case CollStruct.ImageCorrection.Gamma3:
                    border3.BorderBrush = select;
                    break;

                default:
                    border1.BorderBrush = select;
                    break;
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Set the results of the dialog box.
            DialogResult = true;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void imgAcqiredImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ImageCorr = CollStruct.ImageCorrection.Raw;
            border.BorderBrush = select;
            border1.BorderBrush = def;
            border2.BorderBrush = def;
            border3.BorderBrush = def;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void imgAcqiredImage1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ImageCorr = CollStruct.ImageCorrection.Gamma1;
            border.BorderBrush = def;
            border1.BorderBrush = select;
            border2.BorderBrush = def;
            border3.BorderBrush = def;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void imgAcqiredImage2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ImageCorr = CollStruct.ImageCorrection.Gamma2;
            border.BorderBrush = def;
            border1.BorderBrush = def;
            border2.BorderBrush = select;
            border3.BorderBrush = def;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void imgAcqiredImage3_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ImageCorr = CollStruct.ImageCorrection.Gamma3;
            border.BorderBrush = def;
            border1.BorderBrush = def;
            border2.BorderBrush = def;
            border3.BorderBrush = select;
        }




        



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Private function                                                                         =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ShowAllImage
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ShowAllImage()
        {
            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            int PixelNum = (blnSize) ? SIZE_IMAGE : SIZE2_IMAGE;

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Gamma1 image
            if (mblnGamma1)
            {
                pixGammaCorrImage1 = new byte[SIZE2_IMAGE];
                GammaCorrectionImage(pixContrastImage, gamma1, ref pixGammaCorrImage1, PixelNum);
                ImageInvert(ref pixGammaCorrImage1);
            }
            
            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Gamma2 image
            if (mblnGamma2)
            {
                pixGammaCorrImage2 = new byte[SIZE2_IMAGE];
                GammaCorrectionImage(pixContrastImage, gamma2, ref pixGammaCorrImage2, PixelNum);
                ImageInvert(ref pixGammaCorrImage2);
            }
            
            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Gamma3 image
            if (mblnGamma3)
            {
                pixGammaCorrImage3 = new byte[SIZE2_IMAGE];
                GammaCorrectionImage(pixContrastImage, gamma3, ref pixGammaCorrImage3, PixelNum);
                ImageInvert(ref pixGammaCorrImage3);
            }

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Raw image
            ImageInvert(ref pixContrastImage);


            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ShowImage();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GammaCorrectionImage
        ///     Gamma correction image data.
        /// </summary>
        /// <param name="pixImage"></param>
        /// <param name="PixelNum"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GammaCorrectionImage(byte[] pixImage, byte[] gamma, ref byte[] pixGammaCorrImage, int PixelNum)
        {
            for (int i = 0; i < PixelNum; i++)
            {
                byte pixShort = pixImage[i];
                pixGammaCorrImage[i] = gamma[pixShort];
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ShowImage
        /// </summary>
        /// <param name="pixImage"></param>
        /// <param name="blnGamma"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ShowImage()
        {
            int stride = blnSize ? 2000 : 2600;
            int PixelWidth = blnSize ? SIZE_HORIZONTAL : SIZE2_HORIZONTAL;
            int PixelHeight = blnSize ? SIZE_VERTICAL : SIZE2_VERTICAL;

            BitmapSource BmpSrc = null, BmpSrc1 = null, BmpSrc2 = null, BmpSrc3 = null;
            BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pixContrastImage, stride / 2);                              // Raw image
            if (mblnGamma1) BmpSrc1 = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pixGammaCorrImage1, stride / 2);           // Gamma1 image
            if (mblnGamma2) BmpSrc2 = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pixGammaCorrImage2, stride / 2);           // Gamma2 image
            if (mblnGamma3) BmpSrc3 = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pixGammaCorrImage3, stride / 2);           // Gamma3 image

            // Rotate the image 270 degrees and display it.
            imgAcqiredImage.Source = RotateImage(BmpSrc, 270);
            if (BmpSrc1 != null) imgAcqiredImage1.Source = RotateImage(BmpSrc1, 270);
            if (BmpSrc2 != null) imgAcqiredImage2.Source = RotateImage(BmpSrc2, 270);
            if (BmpSrc3 != null) imgAcqiredImage3.Source = RotateImage(BmpSrc3, 270);
        }
        
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ImageInvert
        /// </summary>
        /// <param name="pixImage"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ImageInvert(ref byte[] pixImage)
        {
            int PixelNum = (blnSize) ? SIZE_IMAGE : SIZE2_IMAGE;

            for (int i = 0; i < PixelNum; i++)
            {
                pixImage[i] = (byte)((SENSOR_MAX - 1) - pixImage[i]);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// RotateImage
        /// </summary>
        /// <param name="BmpSrc"></param>
        /// <param name="RotationAngle"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private FormatConvertedBitmap RotateImage(BitmapSource BmpSrc, uint RotationAngle)
        {
            TransformedBitmap myRotatedBitmapSource = new TransformedBitmap();
            myRotatedBitmapSource.BeginInit();
            myRotatedBitmapSource.Source = BmpSrc;
            myRotatedBitmapSource.Transform = new RotateTransform(RotationAngle);
            myRotatedBitmapSource.EndInit();

            FormatConvertedBitmap newFormatedBitmapSource = new FormatConvertedBitmap();
            newFormatedBitmapSource.BeginInit();
            newFormatedBitmapSource.Source = myRotatedBitmapSource;
            newFormatedBitmapSource.DestinationFormat = PixelFormats.Gray8;
            newFormatedBitmapSource.EndInit();

            return newFormatedBitmapSource;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReadGammaCsv
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ReadGammaTable(ref byte[] gamma, string FileName)
        {
            int iCount = 0;
            Microsoft.VisualBasic.FileIO.TextFieldParser parser = null;

            try
            {
                string file = @"C:\HAMAMATSU\S11684S11685\gamma\" + FileName;
                parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(file, Encoding.GetEncoding("Shift_JIS"));
                parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                parser.SetDelimiters(",");

                // 0 clear
                gamma = new byte[256];

                while (!parser.EndOfData)
                {
                    string[] cols = parser.ReadFields();

                    if (cols.Length != 2) { break; }
                    else if (cols[1].Length == 0) { break; }
                    else if (!byte.TryParse(cols[1], out gamma[iCount])) { break; }
                    else
                    {
                        iCount++;
                    }
                }

                if (iCount != 256)
                {
                    return false;
                }

                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
            finally
            {
                if (parser != null)
                {
                    parser.Close();
                    parser.Dispose();
                }

            }
        }

    }
}
