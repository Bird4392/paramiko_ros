﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMOS_USB_DemoApplication
{
    public partial class LogWindow : Window
    {
        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                              Log Window                                                                               *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************

        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public bool blnDose;                                                                    // Flag for caller to determine Main Window or Dose Window 



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public LogWindow()
        {
            InitializeComponent();
            txtLog.Clear();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is laid out, rendered, and ready for interantion.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Get current time.
            DateTime LogTime = DateTime.Now;
            string Directory = @"C:\HAMAMATSU\S11684S11685\log";
            string FileName = "";

            // When the caller is "Main" window.
            if (!blnDose)
            {
                // Path
                FileName = Directory + @"\Log_(" + LogTime.ToString("ddMMyy") + @").txt";
            }

            // When the caller is "Dose rate measurement" window.
            else
            {
                // Path
                FileName = Directory + @"\Log_Dose_(" + LogTime.ToString("ddMMyy") + @").txt";
            }

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";

            // Initializes a new instance of the StreamReader class for the specified stream,
            // with the specified character encoding.
            StreamReader sr = new StreamReader(FileName, System.Text.Encoding.Default);

            // To view the log.
            txtLog.Text = sr.ReadToEnd();

            // releases instance.
            sr.Close();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Folder open
        private void btnOpenLogFolder_Click(object sender, RoutedEventArgs e)
        {
            // folder in Windows Explorer "C:\HAMAMATSU\S11684S11685\log" the open rather than
            System.Diagnostics.Process.Start("EXPLORER.EXE", @"C:\HAMAMATSU\S11684S11685\log");
        }
    }
}
