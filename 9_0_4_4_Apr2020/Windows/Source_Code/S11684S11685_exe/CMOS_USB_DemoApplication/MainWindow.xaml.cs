﻿using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WinUsbSample;



namespace CMOS_USB_DemoApplication
{
    public partial class MainWindow : Window
    {
        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                              Main Window                                                                              *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************

        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private constant                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private const byte MAXIMUM_DEVICE_COUNT = 3;                                                                // Set maximum number of devices being operated at a time
        private const string WINUSB_DEVICEINTERFACE_GUID_STRING = "{6C477250-0AED-44a8-95B7-992DCA6C7CB9}";         // CMOS Device of GUID

        /* Constants for Size1 sensor */
        private const int SIZE_HORIZONTAL = 1000;                                                                   /* Horizontal size of image */
        private const int SIZE_VERTICAL = 1506;                                                                     /* Vertical size of image */
        private const int SIZE_IMAGE = SIZE_HORIZONTAL * SIZE_VERTICAL;                                             /* Size of monochrome image */
        private const int Buffer_Size = SIZE_IMAGE * 2 + 16;
        private const int SIZE1_CUMULATIVE_FREQUENCY = 5000;                                                        /* Cumulative frequency */

        /* Constants for Size2 sensor */
        private const int SIZE2_HORIZONTAL = 1300;                                                                  /* Horizontal size of image */
        private const int SIZE2_VERTICAL = 1706;                                                                    /* Vertical size of image */
        private const int SIZE2_IMAGE = SIZE2_HORIZONTAL * SIZE2_VERTICAL;                                          /* Size of monochrome image */
        private const int Buffer2_Size = SIZE2_IMAGE * 2 + 16;
        private const int SIZE2_CUMULATIVE_FREQUENCY = 10000;                                                       /* Cumulative frequency */


        private const Int32 WM_DEVICECHANGE = 0X219;                                                                /* This message comes when USB connection changes. */
        private const Int32 DBT_DEVICEREMOVECOMPLETE = 0X8004;                                                      /* disconnection of the sensor*/
        private const Int32 DBT_DEVICEARRIVAL = 0X8000;                                                             /* connection of the sensor*/

        private const int SENSOR_MAX = 256;                                                                         /* The maximum that sensor can use. */
        private const int MAX_RANGE = 65535;                                                                        /* The maximum that application can use. */
        private const int MSB_16BIT = 32768;                                                                        /* MSB or 16bit */



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public IntPtr[] DeviceHandle_List = new IntPtr[MAXIMUM_DEVICE_COUNT];
        public IntPtr DeviceHandle = new IntPtr(-1);

        public CollStruct.UintImageAcq[] ImageAcq = new CollStruct.UintImageAcq[MAXIMUM_DEVICE_COUNT];              // Image Acquisition Settings
        public CollStruct.ImageCorrection ImageCorr;




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
        private byte DEVICE_CONNECTED = 0;                                                                          /* Number of connected devices */

        private DeviceManagement myDeviceManagement = new DeviceManagement();                                       /* Classes for receiving events of the sensors */
        private String myDevicePathName;
        private IntPtr deviceNotificationHandle;
        private bool myDeviceDetected = false;

        private BackgroundWorker Worker_SearchSesor;                                                                // USB Device Serch
        private BackgroundWorker Worker_AcqImage;                                                                   // Acquire image
        private BackgroundWorker Worker_XrayImage;                                                                  // HPK_GetXrayImage / HPK_GetCorrectionImage
        private BackgroundWorker Worker_StopAcq;                                                                    // Stop Acquiring image
        private CMOS_USB.UnitXrayImage XrayImage;

        private byte[] XryaImageBuffer = new byte[4450000];                                                         /* Data buffer for storing image data */
        private ulong BuffLength = 0;                                                                               /* Received data size */

        private ushort[] pixEditImage = new ushort[SIZE2_IMAGE];                                                    /* Pixel array for editing.*/
        private ushort[] pixOrigImage = new ushort[SIZE2_IMAGE];                                                    /* Pixel array for original image data.*/
        private byte[] pix8bitImage = new byte[SIZE2_IMAGE];                                                        /* Pixel array for 8bit image data.*/
        private byte[] pixAutoContImage = new byte[SIZE2_IMAGE];                                                    /* Pixel array for auto contrast image data.*/
        private byte[] pixGammaCorrImage = new byte[SIZE2_IMAGE];                                                   /* Pixel array for gamma correction image data. */
        private ushort[] pixDarkImage = new ushort[SIZE2_IMAGE];                                                    /* Pixel array for dark image data.*/
        private ushort[] pixFFFrameImage = new ushort[SIZE2_IMAGE];                                                 /* Pixel array for Flat Filed Frame image data.*/
        private ushort[] pixFFCorrImage = new ushort[SIZE2_IMAGE];                                                  /* Pixel array for Flat Filed Correction image data.*/
        private double[] pixAverageImage = new double[SIZE2_IMAGE];                                                 /* Pixel array for an averaged image. */
        private byte[] gamma = new byte[256];                                                                       /* gamma table */

        private ContrastWindow windowContrast = null;                                                               // ContrastWindow
        private LogWindow windowLog = null;                                                                         // LogWindow
        private byte mbLowContrast;                                                                                 // Low Contrast
        private byte mbHighContrast;                                                                                // High Contrast
        private LineGraph ImageFrequency;                                                                           // LineGraph Frequency;
        private byte DisplayRange = 0;
        /*****DisplayRange*****/
        /*
         * DisplayRange = 0 -> Fit to view
         * DisplayRange = 1 -> 100%
         * DisplayRange = 2 -> 200%
         */

        private String PathToDesktop;                                                                               // file pass to desktop
        private bool mblnGetSs;                                                                                     // Flag indicating whether sensor serial was acquired.
        private bool mblnXrayImage;                                                                                 // True: Image acquire thread has ended.
        private bool mblnStop;                                                                                      // True:Stop button was clicked during Trigger waiting state
        private bool mblnSubStoredImage;                                                                            // Dark subtraction with stored dark image
        private bool mblnCalFile;                                                                                   // Cal file creation flag.
        private bool mblnCorrImg;                                                                                   // Flat Field Correction image file creation flag.
        private bool mblnSize;                                                                                      // Device size flag.    true : Size1,  false : Size2
        private bool mblnInvert;                                                                                    // Invert image flag.
        private bool mblnGamma;                                                                                     // gamma correction.
        private Decimal dCoeff = 0;                                                                                 // A coefficient to calculate "Total X-ray dose".
        private string ResultImage = "";                                                                            // Storing result of image function temporarily

        private System.Timers.Timer timer = null;                                                                   // For timeout during initialization.
        private bool mblnTimeout;                                                                                   //
        private string mMessageTitle = "";                                                                          // Title of HPK_GetXrayImage or HPK_GetXrayCorrectionImage error message.

        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public MainWindow(ref bool blnIni)
        {
            InitializeComponent();

            // Disable control
            ChangeEnabled();

            // Initial setting
            mnuFitToWin.IsChecked = true;           // Select menu [Image] -> [Zoom] -> [Fit to window]
            mnuFitToWin_context.IsChecked = true;   //
            mnuInvert.IsChecked = true;             // Select menu [Image] -> [Invert]
            mnuInvert_context.IsChecked = true;     //

            // Initialize flag
            mblnGetSs = false;
            mblnSubStoredImage = true;
            mblnCalFile = false;
            mblnStop = true;
            mblnSize = true;
            mblnInvert = true;

            // Initialization of the variable.
            mbLowContrast = 0;
            mbHighContrast = 0;
            ImageCorr = CollStruct.ImageCorrection.Gamma1;

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";

            // Initialization of structure.
            InitialStruct();

            // Initialization of BackgroundWorker
            InitBackgroundWorker();

            // Get pass to desktop
            PathToDesktop = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            // Create the Device folder and log folder.
            General.CreateDeviceDirectoryName();
            General.CreateLogDirectoryName();

            // Determines whether the log file exists.
            if (!LogFileExists())
            {
                // Get the system information
                string strInfo = "";
                General.PcSystemInfo(out strInfo);
                WriteLog(strInfo);
            }

            // Enable only the power button.
            ChangeEnabled(true);

            // Read gamma table.
            mblnGamma = ReadGammaTable("gamma_table_1.csv");

            // Set a flag
            blnIni = true;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = !mblnStop;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is about to close.
        private void Window_Closed(object sender, EventArgs e)
        {
            // Close device
            CloseAllDevice();
            WriteLog("Window Closed");

            // Close Contrast Window
            if ((windowContrast != null) && windowContrast.IsActive)
            {
                windowContrast = Application.Current.Windows.OfType<ContrastWindow>().FirstOrDefault();
                windowContrast.Close();
            }

            // Close Log Window
            if ((windowLog != null) && windowLog.IsActive)
            {
                windowLog = Application.Current.Windows.OfType<LogWindow>().FirstOrDefault();
                windowLog.Close();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Click power button
        private void btnPower_Click(object sender, RoutedEventArgs e)
        {
            // Disable control
            ChangeEnabled();

            if (DEVICE_CONNECTED != 0)
            {
                // Close device
                CloseAllDevice();

                // Enable only the power button.
                ChangeEnabled(true);

                // Set the image source.
                mblnGetSs = false;
                ChangePowerButton();

                // Clear the sensor serial combobox
                cboxSensor.Items.Clear();

                return;
            }

            // Change of status displaying
            lblStatus.Content = "Initializing";

            // Register the device in order to receive notifications
            ConnectDevice();

            // Initialize flag
            mblnTimeout = false;

            // Starts execution of a background operation.
            Worker_SearchSesor.RunWorkerAsync();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Start of image acquisition.
        private void btnAcquire_Click(object sender, RoutedEventArgs e)
        {
            DateTime dtNow = DateTime.Now;

            // Read gain frame image
            if (ImageAcq[cboxSensor.SelectedIndex].ImgCorrType == 0x02)
            {
                if (!ReadFlatFieldFrame())
                    return;
            }

            // Read dark image
            pixDarkImage = new ushort[SIZE2_IMAGE];
            if (ImageAcq[cboxSensor.SelectedIndex].SubStoredImage)
            {
                if (!ReadDarkImage())
                    return;
            }

            // Initialize flag
            mblnStop = false;
            mblnXrayImage = false;
            mblnSubStoredImage = ImageAcq[cboxSensor.SelectedIndex].SubStoredImage;
            mblnCalFile = false;
            mblnCorrImg = (ImageAcq[cboxSensor.SelectedIndex].ImgCorrType == 0x02);

            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // Enable only the stop button.
            ChangeEnabled(blnStop: true);

            // Write to log file.
            WriteLog("**********************************************************************************");

            // When asynchronous operation is not running.
            if (Worker_AcqImage.IsBusy == false)
            {
                // Starts execution of a background operation.
                Worker_AcqImage.RunWorkerAsync();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Stop image acquisition.
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            // Disable control
            ChangeEnabled();

            // set a flag
            mblnStop = true;

            // clear a flag.
            mblnXrayImage = false;

            // Image acquisition in progress
            if (Worker_XrayImage.IsBusy)
            {
                // terminate image acquisition
                uint result = 0;
                result = CMOS_USB.HPK_AbortBulkPipe(DeviceHandle);

                // Write returned value to log file.
                WriteLog("HPK_AbortBulkPipe returned 0x" + result.ToString("X4"));
                if (mblnCalFile) { FlatFieldWriteLog("HPK_AbortBulkPipe returned 0x" + result.ToString("X4")); }

                // Successful termination of image acquisition.
                if (result == 0)
                {
                    // Requests cancellation of a pending background operation.
                    Worker_XrayImage.CancelAsync();

                    // When asynchronous operation is not running.
                    if (Worker_StopAcq.IsBusy == false)
                    {
                        // Requests cancellation of a pending background operation.
                        Worker_StopAcq.RunWorkerAsync();
                    }
                }
            }
            else
            {
                // Requests cancellation of a pending background operation.
                Worker_AcqImage.CancelAsync();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Save image
        private void btnSaveImage_Click(object sender, RoutedEventArgs e)
        {
            SaveImage();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Save image
        private void mnuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveImage();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Selected device
        private void cboxSensor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mblnGetSs) { WriteLogSelectDevice(); }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Take screenshot
        private void mnuScreenCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Desktop = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string FileName = Desktop + @"\Screenshot " + now.ToString("yyyy-MM-dd HH.mm.ss") + @".jpg";

                using (var fs = new FileStream(FileName, FileMode.Create))
                {
                    BitmapEncoder enc = new JpegBitmapEncoder();
                    BitmapSource ibmp = General.CopyScreen();

                    if (ibmp != null)
                    {
                        enc.Frames.Add(BitmapFrame.Create(ibmp));
                        enc.Save(fs);
                    }

                    fs.Close();
                }
            }
            catch
            {
                return;
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Manually closes a window.
        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Display "Image Acquisition Settings" window
        private void mnuImageAcq_Click(object sender, RoutedEventArgs e)
        {
            // Get the sensor serial
            string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

            // Preparation for displaying the window.
            ImgAcqSetWindow window = new ImgAcqSetWindow();
            window.Owner = Window.GetWindow(this);

            // Disable the main window
            this.IsEnabled = false;

            // Pass the current setting value.
            window.ImageAcq = ImageAcq[cboxSensor.SelectedIndex];
            window.SensorSerial = SensorSerial;

            // Display "Image Acquisition Settings" window in dialog mode.
            window.ShowDialog();

            // in the case of the "OK" button was clicked.
            if (window.DialogResult == true)
            {
                if (mblnGetSs)
                {
                    byte newImgCorrType = window.ImageAcq.ImgCorrType;
                    double newIntegTime = window.ImageAcq.IntegTime;
                    bool newSubStoredImage = window.ImageAcq.SubStoredImage;
                    
                    bool changeIntegTime = ImageAcq[cboxSensor.SelectedIndex].IntegTime == newIntegTime;
                    bool changeSubStoredImage = ImageAcq[cboxSensor.SelectedIndex].SubStoredImage == newSubStoredImage;
                    
                    // update setting parameter
                    ImageAcq[cboxSensor.SelectedIndex] = window.ImageAcq;

                    // If type "Dark-subtracted image" was selected.
                    if (newImgCorrType == 0x01)
                    {
                        if (newSubStoredImage)
                        {
                            // In the case of settings are updated
                            if (!changeIntegTime || !changeSubStoredImage)
                            {
                                // Display window.
                                ExposureXray MessageWindow = new ExposureXray();
                                MessageWindow.Owner = Window.GetWindow(this);
                                MessageWindow.blnUpdateDark = true;
                                MessageWindow.Show();

                                // Setup timer
                                timer = new System.Timers.Timer();
                                timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsed_Timer);
                                timer.Interval = 10000;

                                // Update dark image data
                                AcquireDarkImage(cboxSensor.SelectedIndex);


                                // Close window
                                if (MessageWindow != null) MessageWindow.Close();
                            }
                        }
                    }
                }
            }

            // Close "Image Acquisition Settings" window
            window.Close();

            // Enable the main window
            this.IsEnabled = true;

            // In the case of create a new gain frame.
            if (window.ImageAcq.CreateCalFile)
            {
                // Delete the map image file
                DeleteMapImage();

                // Start of image acquisition.
                RunWorkerFlatFieldFrameImage();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void mnuShowHist_Click(object sender, RoutedEventArgs e)
        {
            // Confirm whethre a Contrast Window is showed.
            windowContrast = Application.Current.Windows.OfType<ContrastWindow>().FirstOrDefault();

            if (windowContrast != null)
            {
                // Attempts to bring the window to the foreground and activates it.
                windowContrast.Activate();
            }
            else
            {
                // Instance of class created.
                windowContrast = new ContrastWindow();
                windowContrast.setParent(this, mbLowContrast, mbHighContrast);
                windowContrast.Show();
            }

            // Update of the histogram.
            updatePixelHistogram(pixOrigImage);
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Display "Log" window.
        private void mnuOpenLog_Click(object sender, RoutedEventArgs e)
        {
            // Confirm whethre a Contrast Window is showed.
            windowLog = Application.Current.Windows.OfType<LogWindow>().FirstOrDefault();

            if (windowLog != null)
            {
                // Attempts to bring the window to the foreground and activates it.
                windowLog.Activate();
            }
            else
            {
                // Instance of class created.
                windowLog = new LogWindow();
                windowLog.blnDose = false;
                windowLog.Show();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Display "Dose rate measurement" window
        private void mnuDoseRate_Click(object sender, RoutedEventArgs e)
        {
            // Preparation for displaying the window.
            DoseRateMeasWindow window = new DoseRateMeasWindow();
            window.Owner = Window.GetWindow(this);

            // Receive a necessary value.
            for (int i = 0; i < cboxSensor.Items.Count; i++)
            {
                window.SensorSerial[i] = cboxSensor.Items[i].ToString();        // sensor serial
                window.DeviceHandle_List[i] = DeviceHandle_List[i];             // device handle
            }

            // Disable the main window
            this.IsEnabled = false;

            // Display "Dose rate measurement" window in dialog mode.
            window.ShowDialog();

            // Close "Dose rate measurement" window
            window.Close();

            // Enable the main window
            this.IsEnabled = true;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Scale the displayed image
        private void mnuZoomDisplay_Click(object sender, RoutedEventArgs e)
        {
            // Get selected item
            MenuItem selectItem = (MenuItem)sender;

            // toggle check
            // in the case of [Fit to window]
            if (selectItem == mnuFitToWin ||
                selectItem == mnuFitToWin_context)
            {
                // Check
                mnuFitToWin.IsChecked = true;
                mnuFitToWin_context.IsChecked = true;

                // Uncheck
                mnuFullReso.IsChecked = false;
                mnuExtImg.IsChecked = false;
                mnuFullReso_context.IsChecked = false;
                mnuExtImg_context.IsChecked = false;

                // Carry out a function
                FitToView();
            }

            // in the case of [100%]
            else if (selectItem == mnuFullReso ||
                     selectItem == mnuFullReso_context)
            {
                // Check
                mnuFullReso.IsChecked = true;
                mnuFullReso_context.IsChecked = true;

                // Uncheck
                mnuFitToWin.IsChecked = false;
                mnuExtImg.IsChecked = false;
                mnuFitToWin_context.IsChecked = false;
                mnuExtImg_context.IsChecked = false;

                // Carry out a function
                Display100image();
            }

            // in the case of [200%]
            else
            {
                // Check
                mnuExtImg.IsChecked = true;
                mnuExtImg_context.IsChecked = true;

                // Uncheck
                mnuFitToWin.IsChecked = false;
                mnuFullReso.IsChecked = false;
                mnuFitToWin_context.IsChecked = false;
                mnuFullReso_context.IsChecked = false;

                // Carry out a function
                Display200image();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Invert image
        private void mnuInvert_Click(object sender, RoutedEventArgs e)
        {
            mblnInvert = ((MenuItem)sender).IsChecked;
            mnuInvert.IsChecked = mblnInvert;
            mnuInvert_context.IsChecked = mblnInvert;

            if (imgAcqiredImage.Source != null)
            {
                //-----Show image-----//
                ShowImage(pixAutoContImage, mblnGamma);
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Select correction image
        private void mnuSelectCorr_Click(object sender, RoutedEventArgs e)
        {
            SelectCorrWindow window = new SelectCorrWindow();
            window.Owner = Window.GetWindow(this);

            // Disable the main window
            this.IsEnabled = false;

            // Receive a necessary value.
            Array.Copy(pixAutoContImage, window.pixContrastImage, SIZE2_IMAGE);
            window.blnSize = mblnSize;
            window.ImageCorr = ImageCorr;

            // Display "Image Acquisition Settings" window in dialog mode.
            window.ShowDialog();

            // in the case of the "OK" button was clicked.
            if (window.DialogResult == true)
            {
                ImageCorr = window.ImageCorr;

                // Reload gamma table.
                switch (ImageCorr)
                {
                    case CollStruct.ImageCorrection.Raw:
                        mblnGamma = false;
                        break;

                    case CollStruct.ImageCorrection.Gamma1:
                        mblnGamma = ReadGammaTable("gamma_table_1.csv");
                        break;

                    case CollStruct.ImageCorrection.Gamma2:
                        mblnGamma = ReadGammaTable("gamma_table_2.csv");
                        break;

                    case CollStruct.ImageCorrection.Gamma3:
                        mblnGamma = ReadGammaTable("gamma_table_3.csv");
                        break;

                    default:
                        mblnGamma = false;
                        break;
                }

                if (mblnGamma)
                {
                    int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
                    pixGammaCorrImage = new byte[SIZE2_IMAGE];
                    GammaCorrectionImage(pixAutoContImage, PixelNum);
                }

                //----- Show image -----//
                ShowImage(pixAutoContImage, mblnGamma);
            }

            // Close "Image Acquisition Settings" window
            window.Close();

            // Enable the main window
            this.IsEnabled = true;

        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Display "About" window
        private void mnuAbout_Click(object sender, RoutedEventArgs e)
        {
            // Preparation for displaying the window.
            AboutWindow window = new AboutWindow();
            window.Owner = Window.GetWindow(this);

            // Disable the main window
            this.IsEnabled = false;

            // Display "About" window in dialog mode.
            window.ShowDialog();

            // Close "About" window
            window.Close();

            // Enable the main window
            this.IsEnabled = true;
        }





        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Public function                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// SetLowContrast
        /// </summary>
        /// <param name="bLowContrast"></param>
        /// <param name="bHighContrast"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public void SetLowContrast(byte bLowContrast, byte bHighContrast)
        {
            // RAW image or Dark-subtracted image or Flat Field Correction image
            UpdateImageContrast(pixOrigImage, bLowContrast, bHighContrast);

            //-----Gamma correction.-----//
            if (mblnGamma)
            {
                int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
                pixGammaCorrImage = new byte[SIZE2_IMAGE];              // 0 clear
                GammaCorrectionImage(pixAutoContImage, PixelNum);
            }

            // Show image
            ShowImage(pixAutoContImage, mblnGamma);

            // Change the magnification of the image.
            if (DisplayRange == 0x0) { FitToView(); }
            else if (DisplayRange == 0x1) { Display100image(); }
            else if (DisplayRange == 0x2) { Display200image(); }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// SetHighContrast
        /// </summary>
        /// <param name="bLowContrast"></param>
        /// <param name="bHighContrast"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public void SetHighContrast(byte bLowContrast, byte bHighContrast)
        {
            // RAW image or Dark-subtracted image or Flat Field Correction image
            UpdateImageContrast(pixOrigImage, bLowContrast, bHighContrast);

            //-----Gamma correction.-----//
            if (mblnGamma)
            {
                int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
                pixGammaCorrImage = new byte[SIZE2_IMAGE];              // 0 clear
                GammaCorrectionImage(pixAutoContImage, PixelNum);
            }

            // Show image
            ShowImage(pixAutoContImage, mblnGamma);

            // Change the magnification of the image.
            if (DisplayRange == 0x0) { FitToView(); }
            else if (DisplayRange == 0x1) { Display100image(); }
            else if (DisplayRange == 0x2) { Display200image(); }
        }







        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Private function                                                                         =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ChangeEnabled
        ///     Enable / disable control.
        /// </summary>
        /// <param name="blnPower"></param>
        /// <param name="blnAcquire"></param>
        /// <param name="blnStop"></param>
        /// <param name="blnOther"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ChangeEnabled(bool blnPower = false, bool blnAcquire = false, bool blnStop = false, bool blnOther = false)
        {
            // Button control
            btnPower.IsEnabled = blnPower;                  // Power button
            btnAcquire.IsEnabled = blnAcquire;              // Acquire image button
            btnStop.IsEnabled = blnStop;                    // Stop acquiring button

            // Other controls
            lblStatus.IsEnabled = blnPower;                 // Status label
            gpbSensor.IsEnabled = blnOther;                 // Sensor serial groupbox
            dockImagePanel.IsEnabled = blnOther;            // Image dockPanel
            mnuExit.IsEnabled = blnPower;                   // [Menu] -> [File] -> [Exit]
            mnuSettings.IsEnabled = blnOther;               // [Menu] -> [Settings]
            mnuAdvanced.IsEnabled = blnOther;               // [Menu] -> [Advanced]

            // Select Gamma Correction & Save image
            bool blnEnabled = blnAcquire && (imgAcqiredImage.Source != null);
            btnSaveImage.IsEnabled = blnEnabled;            // Save Image button
            mnuSaveAs.IsEnabled = blnEnabled;               // [Menu] -> [File] -> [Save As...]
            mnuShowHist.IsEnabled = blnEnabled;             // [Menu] -> [Image] -> [Show Histogram]
            mnuShowHist_context.IsEnabled = blnEnabled;     // [Menu] -> [Image] -> [Show Histogram]
            mnuSelectCorr.IsEnabled = blnEnabled;           // [Menu] -> [Image] -> [Select Gamma Correction]
            mnuSelectCorr_context.IsEnabled = blnEnabled;   // [Right button click] -> [Select Gamma Correction]
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// InitBackgroundWorker
        ///     Initialization of each BackgroundWorker.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void InitBackgroundWorker()
        {
            // For USB Device Serch
            Worker_SearchSesor = new BackgroundWorker();
            Worker_SearchSesor.DoWork += new DoWorkEventHandler(Worker_SearchSesor_DoWork);
            Worker_SearchSesor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_SearchSesor_RunWorkerCompleted);
            Worker_SearchSesor.WorkerSupportsCancellation = true;


            // For acquire image
            Worker_AcqImage = new BackgroundWorker();
            Worker_AcqImage.DoWork += new DoWorkEventHandler(Worker_AcqImage_DoWork);
            Worker_AcqImage.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_AcqImage_RunWorkerCompleted);
            Worker_AcqImage.WorkerSupportsCancellation = true;


            // For executing a HPK_GetXrayImage / HPK_GetXrayCorrectionImage.
            Worker_XrayImage = new BackgroundWorker();
            Worker_XrayImage.DoWork += new DoWorkEventHandler(Worker_XrayImage_DoWork);
            Worker_XrayImage.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_XrayImage_RunWorkerCompleted);
            Worker_XrayImage.WorkerSupportsCancellation = true;


            // For stop acquire image
            Worker_StopAcq = new BackgroundWorker();
            Worker_StopAcq.DoWork += new DoWorkEventHandler(Worker_StopAcq_DoWork);
            Worker_StopAcq.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_StopAcq_RunWorkerCompleted);
            Worker_StopAcq.WorkerSupportsCancellation = true;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// InitialStruct
        ///     Initialization of structure.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void InitialStruct()
        {
            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                ImageAcq[i].Mode = 0x02;                                                // Integration mode                 [Mode2]
                ImageAcq[i].StartThreshold = 450;                                       // Threshold to start integration   [Dark-subtracted image]
                ImageAcq[i].EndThreshold = 435;                                         // Threshold to end integration
                ImageAcq[i].IntegTime = 500;                                            // Integration time [ms]
                ImageAcq[i].ImgCorrType = 0x01;                                         // Image correction type
                ImageAcq[i].AcqTimes = 0;                                               // Correction images to acquire     [3]
                ImageAcq[i].CreateCalFile = false;                                      // Create a correction file
                ImageAcq[i].PathOfCalFile = General.CreateDeviceDirectoryName();        // Full path of cal file            ["C:\HAMAMATSU\S11684S11685\Device\"]
                ImageAcq[i].SubStoredImage = true;                                      // Dark subtraction with stored dark image
                ImageAcq[i].PathOfDarkFile = General.CreateDarkDirectoryName();         // Full path of dark file           ["C:\HAMAMATSU\S11684S11685\Dark\"]
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OnSourceInitialized
        /// </summary>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        protected override void OnSourceInitialized(EventArgs e)
        {
            HwndSource source = (HwndSource)HwndSource.FromVisual(this);
            source.AddHook(new HwndSourceHook(WndProc));
            base.OnSourceInitialized(e);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WndProc
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // Reopening device handles when sensors are connected or disconnected.
            if (msg == WM_DEVICECHANGE)
            {
                handled = true;

                // USB devices is disconnected.
                if (wParam.ToInt32() == DBT_DEVICEREMOVECOMPLETE)
                {
                    if (myDeviceManagement.DeviceNameMatch(lParam, myDevicePathName) == true)
                    {
                        // Disable control
                        ChangeEnabled();

                        // wait
                        while (Worker_AcqImage.IsBusy) ;

                        // Releases all device handle.
                        CloseAllDevice();

                        // Enable only the power button.
                        ChangeEnabled(true);

                        // Set the image source.
                        mblnGetSs = false;
                        ChangePowerButton();

                        // Clear the sensor serial combobox
                        cboxSensor.Items.Clear();
                    }
                }

            }
            return IntPtr.Zero;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ConnectDevice
        ///     Searching for sensors with the device interface GUID. Registering the device so that application software can receive notifications.
        ///     Create  :   15/11/19
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ConnectDevice()
        {
            bool deviceFound;
            String devicePathName = "";
            bool success;

            try
            {
                if (myDeviceDetected != false)
                {
                    myDeviceManagement.StopReceivingDeviceNotifications(deviceNotificationHandle);
                    myDeviceDetected = false;
                }

                // Convert the device interface GUID String to a GUID object:
                System.Guid winUsbDemoGuid = new System.Guid(WINUSB_DEVICEINTERFACE_GUID_STRING);

                // Fill an array with the device path names of all attached devices with matching GUIDs.
                deviceFound = myDeviceManagement.FindDeviceFromGuid(winUsbDemoGuid, ref devicePathName);

                if (deviceFound == true)
                {
                    myDeviceDetected = true;

                    // Save DevicePathName so WndProc(,,,,) knows which name is my device.
                    myDevicePathName = devicePathName;
                }

                if (myDeviceDetected == true)
                {
                    // Handle to window.
                    HwndSource source = HwndSource.FromVisual(this) as HwndSource;

                    // Register to receive notifications when the device is removed or attached.
                    success = myDeviceManagement.RegisterForDeviceNotifications(myDevicePathName,
                                                                                source.Handle,
                                                                                winUsbDemoGuid,
                                                                                ref deviceNotificationHandle);
                }

                return myDeviceDetected;
            }
            catch (Exception) { throw; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ChangePowerButton
        ///     Change image of power button
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ChangePowerButton()
        {
            // Create source.
            BitmapImage image = new BitmapImage();
            image.BeginInit();

            // When the flag is set.
            if (mblnGetSs)
            {
                image.UriSource = new Uri("power_button_on.png", UriKind.Relative);
            }

            // When the flag is not set.
            else
            {
                image.UriSource = new Uri("power_button_off.png", UriKind.Relative);
            }

            image.EndInit();

            // Set the image source.
            imgPower.Source = image;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// LogFileExists
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool LogFileExists()
        {
            DateTime LogTime = DateTime.Now;
            string Directory = General.CreateLogDirectoryName();
            string logFileName = Directory + "\\" + "Log_(" + LogTime.ToString("ddMMyy") + ").txt";

            // Determines whether the specified file exists.
            if (File.Exists(logFileName))
            {
                return true;
            }

            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WriteLog
        /// </summary>
        /// <param name="WrittenWords"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void WriteLog(String WrittenWords)
        {
            if (App.blnWriteLog)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    StreamWriter LogText = null;
                    try
                    {
                        // Get current time.
                        DateTime LogTime = DateTime.Now;

                        // Path
                        string logFilePath = General.CreateLogDirectoryName();

                        // Open Log, and add line.
                        LogText = new StreamWriter(logFilePath + "\\" + "Log_(" + LogTime.ToString("ddMMyy") + ").txt", true, Encoding.GetEncoding("Shift-JIS"));

                        // Create an instance
                        System.IO.StringReader rs = new System.IO.StringReader(WrittenWords);

                        // Respeated until the end of the stream.
                        while (rs.Peek() > -1)
                        {
                            // Write log.
                            LogText.WriteLine(LogTime.ToString("HH:mm:ss.fff") + " " + rs.ReadLine());
                        }
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(WrittenWords + "\r\n" + exp.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    finally
                    {
                        // Close log.
                        if (LogText != null) { LogText.Close(); }
                    }
                }));
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// FlatFieldWriteLog
        /// </summary>
        /// <param name="WrittenWords"></param>
        /// <param name="blnAppend"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void FlatFieldWriteLog(string WrittenWords, bool blnAppend = true)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Get Serial Number.
                string strSelect = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

                // Get current time.
                DateTime LogTime = DateTime.Now;

                // CreateDirectoryName
                string strDirectory = General.CreateDeviceDirectoryName();

                // Open Log, and add line.
                StreamWriter LogText = new StreamWriter(strDirectory + strSelect + "_Log.txt", blnAppend, Encoding.GetEncoding("Shift-JIS"));

                // Create an instance
                System.IO.StringReader rs = new System.IO.StringReader(WrittenWords);

                // Respeated until the end of the stream.
                while (rs.Peek() > -1)
                {
                    // Write log.
                    LogText.WriteLine(LogTime.ToString("HH:mm:ss.fff") + " " + rs.ReadLine());
                }

                // Close log.
                LogText.Close();
            }));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReadFlatFieldFrame
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ReadFlatFieldFrame()
        {
            // full path of "SNxxxxxx.tif" file.
            string fullPath = ImageAcq[cboxSensor.SelectedIndex].PathOfCalFile + ".tif";

            try
            {
                // Check whether the correction "SNxxxxxx.tif" file exists.
                if (!System.IO.File.Exists(fullPath))
                {
                    string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();
                    MessageBox.Show("The gain frame for \"SN" + SensorSerial + "\" not found." + Environment.NewLine +
                                    "Please make a gain frame." + Environment.NewLine,
                                    "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }

                else
                {
                    int stride = mblnSize ? 2000 : 2600;

                    Stream imageStreamSource = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    TiffBitmapDecoder decoder = new TiffBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    BitmapSource bitmapSourceFFC = decoder.Frames[0];
                    bitmapSourceFFC.CopyPixels(pixFFFrameImage, stride, 0);

                    return true;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Reading the gain frame failed." + Environment.NewLine +
                                exp.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReadDarkImage
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ReadDarkImage()
        {
            // full path of "SNxxxxxx_dark.tif" file.
            string fullPath = ImageAcq[cboxSensor.SelectedIndex].PathOfDarkFile + ".tif";

            try
            {
                // Check whether the correction "SNxxxxxx_dark.tif" file exists.
                if (!System.IO.File.Exists(fullPath))
                {
                    string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();
                    MessageBox.Show("The dark image of \"SN" + SensorSerial + "\" not found." + Environment.NewLine +
                                    "Please reconnect all the sensors and click the power button.",
                                    "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }

                else
                {
                    int stride = mblnSize ? 2000 : 2600;

                    Stream imageStreamSource = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    TiffBitmapDecoder decoder = new TiffBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    BitmapSource bitmapSource = decoder.Frames[0];
                    bitmapSource.CopyPixels(pixDarkImage, stride, 0);

                    return true;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Reading the dark image failed." + Environment.NewLine +
                                exp.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// DeleteMapImage
        ///     Delete the map image file
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void DeleteMapImage()
        {
            // Get the sensor serial
            string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

            // Delete start
            for (int i = 1; i <= 10; i++)
            {
                // [ "C:\HAMAMATSU\S11684S11685\Device\xxxxxx_frame_y_forCal.tif" ]
                string fileName = SensorSerial + "_frame" + i.ToString() + "_forCal.tif";
                string FileName = General.CreateDeviceDirectoryName() + fileName;

                // Delete the file
                System.IO.File.Delete(FileName);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// RunWorkerFlatFieldFrameImage
        ///     Start of Gain Frame image acquisition.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void RunWorkerFlatFieldFrameImage()
        {
            // Initialize flag
            mblnStop = false;
            mblnXrayImage = false;
            mblnSubStoredImage = false;
            mblnCalFile = ImageAcq[cboxSensor.SelectedIndex].CreateCalFile;
            mblnCorrImg = false;

            // Enable only the stop button.
            ChangeEnabled(blnStop: true);

            // Get the system information
            string strInfo = "";
            General.PcSystemInfo(out strInfo);

            // Write to log file of Gain Frame.
            DateTime now = DateTime.Now;
            FlatFieldWriteLog(now.ToString("yyyy/MM/dd"), false);
            FlatFieldWriteLog(strInfo);
            WriteLog("**********************************************************************************");
            FlatFieldWriteLog("**********************************************************************************");

            // When asynchronous operation is not running.
            if (Worker_AcqImage.IsBusy == false)
            {
                // Get the Device handle
                DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

                // Initialize average array
                for (int i = 0; i < SIZE2_IMAGE; i++)
                {
                    pixAverageImage[i] = 65535;
                }

                // Starts execution of a background operation.
                Worker_AcqImage.RunWorkerAsync();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// PreparationImgAcq
        /// </summary>
        /// <param name="WrittenWords"></param>
        /// <param name="blnAppend"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool PreparationImgAcq()
        {
            // Declaring variables
            CMOS_USB.UnitSensorInformation SensorInformation;           // for HPK_GetSensorInformation

            // Get serial number of CMOS device
            uint result = GetSensorInfo(DeviceHandle, out SensorInformation);

            // Write returned value to log file.
            WriteLog("HPK_GetSensorInformation returned 0x" + result.ToString("X4"));
            if (mblnCalFile) { FlatFieldWriteLog("HPK_GetSensorInformation returned 0x" + result.ToString("X4")); }

            // When the sensor information was acquired successfully.
            if (result == 0)
            {
                byte SensorType = SensorInformation.Sensor_Type;

                // Check the size of the device
                if (SensorType < 0x80) { mblnSize = true; }     // Size1
                else { mblnSize = false; }                      // Size2

                // Get the last digit.
                string strByte = SensorType.ToString("X");
                strByte = strByte[strByte.Length - 1].ToString();
                byte lastDigit = (byte)Convert.ToInt32(strByte, 16);

                // Get the coefficient to calculate "Total X-ray dose".
                // FOS / GOS
                if (lastDigit < 0x08)
                {
                    if ((lastDigit == 0x00) || (lastDigit == 0x02) || (lastDigit == 0x04) || (lastDigit == 0x06))
                    {
                        dCoeff = 13.6M;
                    }
                    else { dCoeff = 10.0M; }
                }
                else
                {
                    // Size 1
                    if (mblnSize)
                    {
                        if (SensorType < 0x40) { dCoeff = 13.6M; }
                        else { dCoeff = 10.0M; }
                    }

                    // Size 2
                    else
                    {
                        if (SensorType < 0xC0) { dCoeff = 13.6M; }
                        else { dCoeff = 10.0M; }
                    }
                }

                // Proceed to the next step.
                return true;
            }

            // Stop acquire image.
            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CalculateContrast
        ///     Calculate the contrast image data.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CalculateContrast(ushort[] pixImage, int PixelNum)
        {
            int[] hist = new int[MAX_RANGE];
            ushort max = 0;
            ushort min = MAX_RANGE;
            int count = 0;
            ushort[] pixImgBuffer = new ushort[pixImage.Length];
            int CumulFreq = mblnSize ? SIZE1_CUMULATIVE_FREQUENCY : SIZE2_CUMULATIVE_FREQUENCY;

            // copy
            Array.Copy(pixImage, pixImgBuffer, pixImage.Length);

            // Preparation for calculating the contrast
            PreparatCalcContrast(ref pixImgBuffer, PixelNum);

            // make histogram
            for (int i = 0; i < PixelNum; i++)
            {
                hist[pixImgBuffer[i]]++;
            }

            // calculate min
            for (ushort i = 0; i < MAX_RANGE; i++)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    min = i;
                    break;
                }
            }

            // 0 clear
            count = 0;

            // calculate max
            for (ushort i = (MAX_RANGE - 1); i < MAX_RANGE; i--)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    max = i;
                    break;
                }
            }

            int diff = max - min;
            if (diff == 0) { diff = 1; }

            // holds the values.
            mbLowContrast = 0;
            mbHighContrast = 255;

            // 0 clear
            pixAutoContImage = new byte[SIZE2_IMAGE];

            // Calculate the contrast image data.
            for (int i = 0; i < PixelNum; i++)
            {
                ushort pixShort = pixImgBuffer[i];

                // Confirm that a value is.
                if (pixShort > max) { pixShort = max; }
                else if (pixShort < min) { pixShort = min; }

                int value = (SENSOR_MAX * (pixShort - min) / diff);

                // if less than 0 or more than 255
                if (255 < value) { pixAutoContImage[i] = 255; }
                else if (0 > value) { pixAutoContImage[i] = 0; }
                else { pixAutoContImage[i] = (byte)value; }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GammaCorrectionImage
        ///     Gamma correction image data.
        /// </summary>
        /// <param name="pixImage"></param>
        /// <param name="PixelNum"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GammaCorrectionImage(byte[] pixImage, int PixelNum)
        {
            for (int i = 0; i < PixelNum; i++)
            {
                byte pixShort = pixImage[i];
                pixGammaCorrImage[i] = gamma[pixShort];
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// PreparatCalcContrast
        ///     Preparation for calculating the contrast
        /// </summary>
        /// <param name="pixImage"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void PreparatCalcContrast(ref ushort[] pixImage, int PixelNum)
        {
            ushort[] hist = new ushort[MAX_RANGE];
            ushort max = 0;
            ushort value = 0;

            // Making the value of not active pixels 0 LSB.
            NotActivePixels(ref pixImage);

            // make histogram
            for (int i = 0; i < PixelNum; i++)
            {
                hist[pixImage[i]]++;
            }

            // calculate max
            for (ushort i = (MAX_RANGE - 1); i < MAX_RANGE && i > 0; i--)
            {
                if (hist[i] > max)
                {
                    max = hist[i];
                    value = i;
                }
            }

            // In the case of size1 image.
            if (mblnSize)
            {
                ReplacementArray(ref pixImage, 0, value, 3000);
                ReplacementArray(ref pixImage, 1503000, value, 3000);

                for (int i = 0; i < 115; i++)
                {
                    int index = 3000 + i * 1000;
                    int index2 = 3885 + i * 1001;
                    int length = 115 - i;

                    ReplacementArray(ref pixImage, index, value, length);
                    ReplacementArray(ref pixImage, index2, value, length);
                }
            }

            // In the case of size2 image
            else
            {
                ReplacementArray(ref pixImage, 0, value, 3900);
                ReplacementArray(ref pixImage, 2213900, value, 3900);

                for (int i = 0; i < 270; i++)
                {
                    int index = 3900 + i * 1300;
                    int index2 = 4930 + i * 1301;
                    int length = 270 - i;

                    ReplacementArray(ref pixImage, index, value, length);
                    ReplacementArray(ref pixImage, index2, value, length);
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReplacementArray
        ///     Replace the value in the specified range of the array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="length"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ReplacementArray(ref ushort[] array, int index, ushort value, int length)
        {
            for (int i = 0; i < length; i++)
            {
                array[index + i] = value;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ShowImage
        /// </summary>
        /// <param name="pixImage"></param>
        /// <param name="blnGamma"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ShowImage(byte[] pixImage, bool blnGamma)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                int stride = mblnSize ? 2000 : 2600;
                int PixelWidth = mblnSize ? SIZE_HORIZONTAL : SIZE2_HORIZONTAL;
                int PixelHeight = mblnSize ? SIZE_VERTICAL : SIZE2_VERTICAL;

                byte[] pixShowImage = new byte[SIZE2_IMAGE];

                if (!blnGamma) { Array.Copy(pixImage, pixShowImage, SIZE2_IMAGE); }
                else { Array.Copy(pixGammaCorrImage, pixShowImage, SIZE2_IMAGE); }

                if (mblnInvert)
                {
                    ImageInvert(ref pixShowImage);
                }

                BitmapSource BmpSrc;
                BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pixShowImage, stride / 2);


                // Rotate the image 270 degrees and display it.
                imgAcqiredImage.Source = RotateImage(BmpSrc, 270);
            }));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ImageInvert
        /// </summary>
        /// <param name="pixImage"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ImageInvert(ref byte[] pixImage)
        {
            int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;

            for (int i = 0; i < PixelNum; i++)
            {
                pixImage[i] = (byte)((SENSOR_MAX - 1) - pixImage[i]);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// RotateImage
        /// </summary>
        /// <param name="BmpSrc"></param>
        /// <param name="RotationAngle"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private FormatConvertedBitmap RotateImage(BitmapSource BmpSrc, uint RotationAngle)
        {
            TransformedBitmap myRotatedBitmapSource = new TransformedBitmap();
            myRotatedBitmapSource.BeginInit();
            myRotatedBitmapSource.Source = BmpSrc;
            myRotatedBitmapSource.Transform = new RotateTransform(RotationAngle);
            myRotatedBitmapSource.EndInit();

            FormatConvertedBitmap newFormatedBitmapSource = new FormatConvertedBitmap();
            newFormatedBitmapSource.BeginInit();
            newFormatedBitmapSource.Source = myRotatedBitmapSource;
            newFormatedBitmapSource.DestinationFormat = PixelFormats.Gray8;
            newFormatedBitmapSource.EndInit();

            return newFormatedBitmapSource;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OutputRangeAndDoseCurrent
        ///     Output range and dose
        /// </summary>
        /// <param name="PixelNum"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void OutputRangeAndDoseCurrent(int PixelNum)
        {
            if (!mblnStop)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    // Obtain an averaged image
                    double[] pixels = new double[SIZE2_IMAGE];
                    for (int i = 0; i < PixelNum; i++) { pixels[i] = pixOrigImage[i]; }

                    // Mean value used for flat field correction
                    double FlatMean = 0;
                    AverageFlatMean(ref FlatMean, pixels);

                    // Write Log
                    WriteLog("Average output : " + FlatMean.ToString("0") + " [LSB]");
                    if (mblnCalFile) { FlatFieldWriteLog("Average output : " + FlatMean.ToString("0") + " [LSB]"); }

                    // Excepting mode 3.
                    if (ImageAcq[cboxSensor.SelectedIndex].Mode != 0x03)
                    {
                        // MicroGray[uGy] = FlatMean[LSB] / Sensitivity in datasheet[LSB/uGy]
                        double dbMicroGray;

                        // XrayCorrectionImage  MicroGray[uGy] = FlatMean[LSB] / Sensitivity in datasheet[LSB/uGy]
                        if (ImageAcq[cboxSensor.SelectedIndex].ImgCorrType != 0x00)
                        {
                            dbMicroGray = FlatMean / (double)dCoeff;
                        }

                        // XrayImage            MicroGray[uGy] = FlatMean[LSB] - 5000 / Sensitivity in datasheet[LSB/uGy]
                        else
                        {
                            FlatMean = FlatMean - 5000;
                            if (FlatMean < 0) { FlatMean = 0; }
                            dbMicroGray = FlatMean / (double)dCoeff;
                        }

                        // Write Log
                        WriteLog("Total X-ray dose : " + dbMicroGray.ToString("0") + " [uGy]");
                        if (mblnCalFile) { FlatFieldWriteLog("Total X-ray dose : " + dbMicroGray.ToString("0") + " [uGy]"); }
                    }
                }));

            }
            else
            {
                Worker_AcqImage.CancelAsync();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// AverageFlatMean
        ///     Calculated with the center 100x100 pixel area
        /// </summary>
        /// <param name="FlatMean"></param>
        /// <param name="dblValue"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void AverageFlatMean(ref double FlatMean, double[] dblValue)
        {
            double Flatsum = 0;
            double[] SquareAve = new double[10000];

            // Mean value of 100*100 pixel, center of a image.
            // In the case of Size1.
            if (mblnSize)
            {
                for (int i = 0; i < 100; i++)
                {
                    // from (450,700) to (550,800)
                    int a = 700450 + i * 1000;
                    int b = i * 100;
                    Array.Copy(dblValue, a, SquareAve, b, 100);
                }
            }

            // In the case of Size2.
            else
            {
                for (int i = 0; i < 100; i++)
                {
                    // from (600,700) to (700,800)
                    int a = 910600 + i * 1300;
                    int b = i * 100;
                    Array.Copy(dblValue, a, SquareAve, b, 100);
                }
            }

            for (int i = 0; i < 10000; i++)
            {
                Flatsum += SquareAve[i];
            }

            // Mean value used for flat field correction
            FlatMean = Flatsum / 10000;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ObtainAveImg
        ///     Obtain an averaged image
        /// </summary>
        /// <param name="FlatMean"></param>
        /// <param name="dblValue"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ObtainAveImg(ref int AcqCount)
        {
            int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
            for (int i = 0; i < PixelNum; i++)
            {
                if (AcqCount == 1) { pixAverageImage[i] = pixOrigImage[i]; }
                else { pixAverageImage[i] = (pixAverageImage[i] + pixOrigImage[i]); }
            }

            // increment the number of times.
            AcqCount++;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// NotActivePixels
        ///     Making the value of not active pixels 0 LSB.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void NotActivePixels(ref ushort[] pixImage)
        {
            // In the case of size1 image.
            if (mblnSize)
            {
                Array.Clear(pixImage, 0, 3000);
                Array.Clear(pixImage, 1503000, 3000);

                for (int i = 0; i < 115; i++)
                {
                    int index = 3000 + i * 1000;
                    int index2 = 3885 + i * 1001;
                    int length = 115 - i;

                    Array.Clear(pixImage, index, length);
                    Array.Clear(pixImage, index2, length);
                }
            }

            // In the case of size2 image
            else
            {
                Array.Clear(pixImage, 0, 3900);
                Array.Clear(pixImage, 2213900, 3900);
                for (int i = 0; i < 270; i++)
                {
                    int index = 3900 + i * 1300;
                    int index2 = 4930 + i * 1301;
                    int length = 270 - i;

                    Array.Clear(pixImage, index, length);
                    Array.Clear(pixImage, index2, length);
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// FitToView
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void FitToView()
        {
            // Set a display range flag.
            DisplayRange = 0;

            if (imgAcqiredImage.Source != null)
            {
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                imgAcqiredImage.Width = scrollViewer.Width;
                imgAcqiredImage.Height = scrollViewer.Height;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Display100image
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Display100image()
        {
            // Set a display range flag.
            DisplayRange = 1;

            if (imgAcqiredImage.Source != null)
            {
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

                // in the case Size1
                if (mblnSize)
                {
                    imgAcqiredImage.Width = SIZE_VERTICAL;
                    imgAcqiredImage.Height = SIZE_HORIZONTAL;
                }

                // in the case Size2
                else
                {
                    imgAcqiredImage.Width = SIZE2_VERTICAL;
                    imgAcqiredImage.Height = SIZE2_HORIZONTAL;
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Display200image
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Display200image()
        {
            // Set a display range flag.
            DisplayRange = 2;

            if (imgAcqiredImage.Source != null)
            {
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

                // in the case Size1
                if (mblnSize)
                {
                    imgAcqiredImage.Width = (2 * SIZE_VERTICAL);
                    imgAcqiredImage.Height = (2 * SIZE_HORIZONTAL);
                }

                // in the case Size2
                else
                {
                    imgAcqiredImage.Width = (2 * SIZE2_VERTICAL);
                    imgAcqiredImage.Height = (2 * SIZE2_HORIZONTAL);
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// UpdateImageContrast
        ///     Update the contrast of the image,
        /// </summary>
        /// <param name="PixelData"></param>
        /// <param name="LowLevel"></param>
        /// <param name="HighLevel"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void UpdateImageContrast(ushort[] PixelData, byte LowLevel, byte HighLevel)
        {
            // Vertical size of image
            int[] hist = new int[MAX_RANGE];
            ushort max = 0;
            ushort min = MAX_RANGE;
            int count = 0;
            int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
            ushort[] pixImgBuffer = new ushort[PixelData.Length];
            int CumulFreq = mblnSize ? SIZE1_CUMULATIVE_FREQUENCY : SIZE2_CUMULATIVE_FREQUENCY;

            // copy
            Array.Copy(PixelData, pixImgBuffer, PixelData.Length);

            // Preparation for calculating the contrast
            PreparatCalcContrast(ref pixImgBuffer, PixelNum);

            // make histogram
            for (int i = 0; i < PixelNum; i++)
            {
                hist[pixImgBuffer[i]]++;
            }

            // calculate min
            for (ushort i = 0; i < MAX_RANGE; i++)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    min = i;
                    break;
                }
            }

            // 0 clear
            count = 0;

            // calculate max
            for (ushort i = (MAX_RANGE - 1); i < MAX_RANGE; i--)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    max = i;
                    break;
                }
            }

            int diff = max - min;
            if (diff == 0) { diff = 1; }

            // 0 clear
            pixAutoContImage = new byte[SIZE2_IMAGE];

            // Calculate the contrast image data.
            for (int i = 0; i < PixelNum; i++)
            {
                ushort pixShort = pixImgBuffer[i];

                // Confirm that a value is.
                if (pixShort > max) { pixShort = max; }
                else if (pixShort < min) { pixShort = min; }

                int value = (256 * (pixShort - min) / diff);

                // if less than 0 or more than 255
                if (HighLevel < value) { pixAutoContImage[i] = HighLevel; }
                else if (LowLevel > value) { pixAutoContImage[i] = LowLevel; }
                else { pixAutoContImage[i] = (byte)value; }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// updatePixelHistogram
        ///     Update of the histogram.
        /// </summary>
        /// <param name="pixelData"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void updatePixelHistogram(ushort[] pixelData)
        {
            /* Draw histogram */
            int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
            uint[] HistoCounter = new uint[SENSOR_MAX];     // Y axis data
            int[] xAxis = new int[SENSOR_MAX];              // X axis data

            //-----Calculate the contrast image data.-----//
            CalculateContrast(pixelData, PixelNum);

            for (int i = 0; i < PixelNum; i++)
            {
                // this is histogram.
                HistoCounter[pixAutoContImage[i]]++;

                // Numbering xAxis.
                if (i < SENSOR_MAX) { xAxis[i] = i; }
            }

            double maxPixel = 0;
            double minPixel = SENSOR_MAX;

            for (int i = 0; i < PixelNum; i++)
            {
                if (pixAutoContImage[i] > maxPixel) { maxPixel = pixAutoContImage[i]; }
                if (pixAutoContImage[i] < minPixel) { minPixel = pixAutoContImage[i]; }
            }

            uint maxPixel_count = 0;
            int maxPixel_point = 0;

            for (int i = 0; i < SENSOR_MAX; i++)
            {
                if (HistoCounter[i] > maxPixel_count)
                {
                    maxPixel_count = HistoCounter[i];
                    maxPixel_point = i;
                }
            }

            // making x data source constractor.
            var xAxisDataSource = new EnumerableDataSource<int>(xAxis);

            // data on the graph must be double type.
            xAxisDataSource.SetXMapping(xxx => Convert.ToDouble(xxx));

            // making y data source constractor.
            var yAxisDataSource = new EnumerableDataSource<uint>(HistoCounter);

            // data on the graph must be double type.
            yAxisDataSource.SetYMapping(yyy => Convert.ToDouble(yyy));

            // making datasource of x and y.
            CompositeDataSource compositedatasource1 = new CompositeDataSource(xAxisDataSource, yAxisDataSource);
            windowContrast.plotter3.Children.Remove(ImageFrequency);

            // Drawing graph.
            ImageFrequency = windowContrast.plotter3.AddLineGraph(compositedatasource1, Colors.Green, 3, "image histogram");

            // Resize graph
            double dCoorX = SENSOR_MAX * -0.02;
            double dCoorY = maxPixel_count * -0.1;

            windowContrast.plotter3.Visible = new Rect(0 + dCoorX, dCoorY, SENSOR_MAX - (dCoorX * 5), (double)maxPixel_count - (dCoorY * 2));
            windowContrast.plotter3.LegendVisible = false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// SaveImage
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void SaveImage()
        {
            CollStruct.UintImageAcq unitImageAcq = ImageAcq[cboxSensor.SelectedIndex];

            // Get the sensor serial
            string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

            // Get the image correction type
            string ImageCorr = "";
            if (unitImageAcq.ImgCorrType == 0x00) { ImageCorr = "RAW"; }
            else if (unitImageAcq.ImgCorrType == 0x01) { ImageCorr = "Dark-subtracted"; }
            else { ImageCorr = "Flat field corrected"; }

            // Create the file name.
            string FileName = SensorSerial + "_" +
                              ImageCorr;

            if (unitImageAcq.Mode != 0x01)
            {
                FileName += "_" + unitImageAcq.IntegTime.ToString() + "ms";
            }

            // SaveFileDialog an instance of the class created.
            SaveFileDialog sfd = new SaveFileDialog();

            // "file name" in the display is a string a specified to.
            sfd.FileName = FileName + ".tif";

            // [file type] to display the choices the designation to
            // specify not (empty of the string) of the time is, the current directory is displayed is.
            sfd.Filter = "16bit TIFF Image(*.tif)|*.tif|8bit TIFF Image(*.tif)|*.tif";

            // [file type] to start in selecting those which are specified to
            // 2-th of " 16bit TIFF Image " is selected so that has been,
            sfd.FilterIndex = 2;

            // the title set to.
            sfd.Title = "Save As";

            // Sets a value indicating whether the dialog box restores the directory to the previously selected directory before closing.
            sfd.RestoreDirectory = false;

            // Shows the form as a modal dialog box.
            if (sfd.ShowDialog() == true)
            {
                string FullPath = sfd.FileName;
                switch (sfd.FilterIndex)
                {
                    case 1:
                        CreateTifImage(FullPath);
                        break;
                    case 2:
                        EightBitConversion();
                        CreateTifImage(FullPath, bln8bit: true);
                        break;
                    default:
                        break;
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CreateTif
        ///     Create a tiff image file
        /// </summary>
        /// <param name="FileName"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CreateTifImage(string FileName, bool blnForCal = false, bool bln8bit = false, bool blnDark = false)
        {
            // Declaring variables
            int stride = mblnSize ? 2000 : 2600;
            int PixelWidth = mblnSize ? SIZE_HORIZONTAL : SIZE2_HORIZONTAL;
            int PixelHeight = mblnSize ? SIZE_VERTICAL : SIZE2_VERTICAL;
            int Name_Length = 0;
            int LastIndex = 0;
            string NewFileName;


            // "Raw image" or "Dark-subtracted image"
            BitmapSource BmpSrc;
            BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray16, null, pixEditImage, stride);

            // Saving the Dark image
            if (blnDark)
            {
                BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray16, null, pixOrigImage, stride);
            }

            if (!blnDark)
            {
                // Saving the flat filed frame image
                if (mblnCalFile && !blnForCal)
                {
                    BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray16, null, pixFFFrameImage, stride);
                }

                // Saving the image with flat field correction
                if (!mblnCalFile && ImageAcq[cboxSensor.SelectedIndex].ImgCorrType == 0x02)
                {
                    BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray16, null, pixFFCorrImage, stride);
                }

                // Saving the 8bit image
                if (bln8bit)
                {
                    BmpSrc = BitmapSource.Create(PixelWidth, PixelHeight, 96, 96, PixelFormats.Gray8, null, pix8bitImage, stride / 2);
                }
            }

            TiffBitmapEncoder TifEnc = new TiffBitmapEncoder();
            TifEnc.Compression = TiffCompressOption.None;
            TifEnc.Frames.Add(BitmapFrame.Create(BmpSrc));

            // Create the file name
            Name_Length = FileName.Length;
            LastIndex = FileName.LastIndexOf(".tif");

            if (LastIndex == (Name_Length - 4)) { NewFileName = FileName; }
            else { NewFileName = FileName + ".tif"; }

            // Save image
            using (FileStream FS = new FileStream(NewFileName, FileMode.Create))
            {
                TifEnc.Save(FS);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CreateCalFile
        ///     Create a cal file
        /// </summary>
        /// <param name="FileName"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CreateCalFile(string FileName)
        {
            try
            {
                byte[] FlatCALtemp;         // data for saving CAL data
                byte[] HeaderCAL;
                byte[] temp = new byte[2];
                int iImageSize = 0;

                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // In the case of Size1
                if (mblnSize == true)
                {
                    FlatCALtemp = new byte[Buffer_Size];
                    HeaderCAL = new byte[] { 0x02, 0x00, 0x00, 0x00, 0xE8, 0x03, 0x00, 0x00, 0xE2, 0x05, 0x00, 0x00 };
                    iImageSize = SIZE_IMAGE;
                }

                // In the case of Size2
                else
                {
                    FlatCALtemp = new byte[Buffer2_Size];
                    HeaderCAL = new byte[] { 0x02, 0x00, 0x00, 0x00, 0x14, 0x05, 0x00, 0x00, 0xAA, 0x06, 0x00, 0x00 };
                    iImageSize = SIZE2_IMAGE;
                }

                for (int i = 0; i < iImageSize; i++)
                {
                    temp = BitConverter.GetBytes(pixFFFrameImage[i]);
                    FlatCALtemp[i * 2] = temp[0];
                    FlatCALtemp[i * 2 + 1] = temp[1];
                }


                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                byte[] FlatCAL = new byte[FlatCALtemp.Length];

                for (int i = 0; i < 12; i++)
                {
                    FlatCAL[i] = HeaderCAL[i];
                }

                for (int i = 12; i < FlatCAL.Length; i++)
                {
                    FlatCAL[i] = FlatCALtemp[i - 12];
                }

                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                int Name_Length = FileName.Length;
                string NewFileName = FileName + ".CAL";
                int BuffSize = FlatCAL.Length;

                System.IO.FileStream fs = new System.IO.FileStream(
                    NewFileName,
                    System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                fs.Write(FlatCAL, 0, BuffSize);
                fs.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show("Saving .CAL file failed." + Environment.NewLine + Environment.NewLine +
                                exp.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// EightBitConversion
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void EightBitConversion()
        {
            int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;
            int[] hist = new int[MAX_RANGE];
            ushort max = 0;
            ushort min = MAX_RANGE;
            int count = 0;
            int CumulFreq = mblnSize ? SIZE1_CUMULATIVE_FREQUENCY : SIZE2_CUMULATIVE_FREQUENCY;

            ushort[] pixImgBuffer = new ushort[pixOrigImage.Length];

            // copy
            Array.Copy(pixOrigImage, pixImgBuffer, pixOrigImage.Length);

            // Preparation for calculating the contrast
            PreparatCalcContrast(ref pixImgBuffer, PixelNum);

            // make histogram
            for (int i = 0; i < PixelNum; i++)
            {
                hist[pixImgBuffer[i]]++;
            }

            // calculate min
            for (ushort i = 0; i < MAX_RANGE; i++)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    min = i;
                    break;
                }
            }

            // 0 clear
            count = 0;

            // calculate max
            for (ushort i = (MAX_RANGE - 1); i < MAX_RANGE; i--)
            {
                if (count < CumulFreq)
                {
                    count += hist[i];
                }

                // Set the minimum value.
                if (count >= CumulFreq)
                {
                    max = i;
                    break;
                }
            }

            int diff = max - min;
            if (diff == 0) { diff = 1; }

            // 0 clear
            pix8bitImage = new byte[SIZE2_IMAGE];

            // 8bit conversion.
            for (int i = 0; i < PixelNum; i++)
            {
                ushort pixValue = pixImgBuffer[i];

                if (max < pixValue) { pixValue = max; }
                if (min > pixValue) { pixValue = min; }

                int value = (256 * (pixValue - min) / diff);

                // if less than 0 or more than 255
                if (255 < value) { pix8bitImage[i] = 255; }
                else if (0 > value) { pix8bitImage[i] = 0; }
                else { pix8bitImage[i] = (byte)value; }
            }
            
            // Invert
            ImageInvert(ref pix8bitImage);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReadGammaCsv
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ReadGammaTable(string FileName)
        {
            int iCount = 0;
            Microsoft.VisualBasic.FileIO.TextFieldParser parser = null;

            try
            {
                string file = @"C:\HAMAMATSU\S11684S11685\gamma\" + FileName;
                parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(file, Encoding.GetEncoding("Shift_JIS"));
                parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                parser.SetDelimiters(",");

                // 0 clear
                gamma = new byte[256];

                while (!parser.EndOfData)
                {
                    string[] cols = parser.ReadFields();

                    if (cols.Length != 2) { break; }
                    else if (cols[1].Length == 0) { break; }
                    else if (!byte.TryParse(cols[1], out gamma[iCount])) { break; }
                    else
                    {
                        iCount++;
                    }
                }

                if (iCount != 256)
                {
                    return false;
                }

                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
            finally
            {
                if (parser != null)
                {
                    parser.Close();
                    parser.Dispose();
                }

            }
        }








        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OpenDevice
        ///     Opens the sensors
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool OpenDevice()
        {
            // Preparation of variables.
            IntPtr devhandle = new IntPtr(-1);
            DEVICE_CONNECTED = 0;

            // Search for connected device
            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                // Open a device handle
                devhandle = CMOS_USB.USB_OpenDevice(0x4400);

                // Write the return value to the log file.
                WriteLog("USB_OpenDevice returned 0x" + devhandle.ToString("X4"));

                // return value is not -1.
                if (devhandle != INVALID_HANDLE_VALUE)
                {
                    // store the device handle in a variable.
                    DeviceHandle_List[DEVICE_CONNECTED] = devhandle;
                    DEVICE_CONNECTED++;
                }
            }

            // There are one or more devices.
            if (DEVICE_CONNECTED != 0)
            {
                return true;
            }

            // There is no device
            MessageBox.Show("No sensor is detected." + Environment.NewLine +
                            "Please make sure to connect a sensor.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetPipeHandle
        ///     Get pipe handle
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetPipeHandle()
        {
            // variable for loop
            int iLoop = 0;

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // for return value
                IntPtr result = new IntPtr(-1);

                // Open a pipe handle
                result = CMOS_USB.USB_OpenPipe(DeviceHandle_List[iLoop]);

                // Write result to log file.
                WriteLog("USB_OpenPipe returned 0x" + result.ToString("X4"));

                // Failed to get a pipe handle.
                if (result == INVALID_HANDLE_VALUE)
                {
                    // show error message
                    MessageBox.Show("Initialization failed." + Environment.NewLine +
                                    "Please reconnect all the sensors and click the power button.",
                                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    // If USB_OpenPipe failed, it may be necessary to reconnect the sensor to another USB port.
                    cboxSensor.Items.Add("**reconnect the sensor**");
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetSensorSerial
        ///     Get sensor serial
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetSensorSerial()
        {
            // Declaring variables
            CMOS_USB.UnitSensorInformation SensorInformation;           // for HPK_GetSensorInformation
            int iLoop = 0;                                              // for loop

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // Get serial number of CMOS device
                uint result = GetSensorInfo(DeviceHandle_List[iLoop], out SensorInformation);

                // The sensor information was acquired successfully.
                if (result == 0x00)
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        string SensorSerial = SensorInformation.Sensor_Type.ToString("X2") + SensorInformation.Lot_Serial_No.ToString("X4");

                        // Add sensor serial to combobox
                        cboxSensor.Items.Add(SensorSerial);

                        // Full path of cal file   ["C:\HAMAMATSU\S11684S11685\Device\SNxxxxxx"]
                        ImageAcq[iLoop].PathOfCalFile += "SN" + SensorSerial;

                        // Write returned value to log.
                        WriteLog("HPK_GetSensorInformation returned 0x0");
                    }));
                }
                else
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        // show error message
                        MessageBox.Show("Initialization failed." + Environment.NewLine +
                                        "Please reconnect all the sensors and click the power button.",
                                        "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                        // If HPK_GetSensorInformation failed, it may be necessary to reconnect the sensor to another USB port.
                        cboxSensor.SelectedIndex = -1;
                    }));

                    // Leave the loop.
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetDarkImage
        ///     Get dark image
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetDarkImage()
        {
            int iLoop = 0;                                                  // for loop

            // Setup timer
            timer = new System.Timers.Timer();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsed_Timer);
            timer.Interval = 5000;

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                if (!AcquireDarkImage(iLoop)) { break; }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// AcquireDarkImage
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool AcquireDarkImage(int Index)
        {
            uint result = 0;                                                // Return value of the function.

            // Get serial number of CMOS device
            CMOS_USB.UnitSensorInformation SensorInformation;
            result = GetSensorInfo(DeviceHandle_List[Index], out SensorInformation);

            if (result == 0)
            {
                if (SensorInformation.Sensor_Type < 0x80) { mblnSize = true; }
                else { mblnSize = false; }

                // Setting parameter
                CMOS_USB.UnitXrayImage DarkImage;
                DarkImage.Mode = 0x03;                                                  // for dark image
                DarkImage.IntegParam.Xray_Incident_Threshold = 440;                     // Threshold to start integration
                DarkImage.IntegParam.Xray_Integration_End_Threshold = 437;              // Threshold to end integration
                DarkImage.IntegParam.Integration_Time = ImageAcq[Index].IntegTime;      // default : 500 [ms]

                string SensorSerial = SensorInformation.Sensor_Type.ToString("X2") + SensorInformation.Lot_Serial_No.ToString("X4");
                BuffLength = 0;                         // Received data size
                XryaImageBuffer = new byte[4450000];    // Data buffer for storing image data

                // Timer start
                timer.Start();

                // HPK_GetXrayImage
                result = CMOS_USB.HPK_GetXrayImage(DeviceHandle_List[Index], XryaImageBuffer, out BuffLength, ref DarkImage);

                // Timer stop
                timer.Stop();

                // If the flag is set.
                if (mblnTimeout)
                {
                    // Write error message to log.
                    WriteLog("Acquiring dark image for SN" + SensorSerial + " failed (time-out error).");

                    // Leave the loop.
                    return false;
                }

                // The sensor information was acquired successfully.
                if (result == 0x00)
                {
                    if ((BuffLength == Buffer_Size && mblnSize) ||
                        (BuffLength == Buffer2_Size && !mblnSize))
                    {

                        ushort pixShort;
                        int j = 0;
                        int Loop_Limit = (mblnSize) ? Buffer_Size : Buffer2_Size;       // Size1->Buffer_Size / Size2->Buffer2_Size

                        // 0 clear
                        pixOrigImage = new ushort[SIZE2_IMAGE];

                        // Get the pixel data of the image.
                        for (int i = 14; i < (Loop_Limit - 2); i += 2)
                        {
                            pixShort = (ushort)BitConverter.ToUInt16(XryaImageBuffer, i);
                            pixOrigImage[j++] = pixShort;
                        }

                        // [ "C:\HAMAMATSU\S11684S11685\Dark\SNxxxxxx_dark.tif" ]
                        ImageAcq[Index].PathOfDarkFile = General.CreateDarkDirectoryName();         // Initialize
                        ImageAcq[Index].PathOfDarkFile += "SN" + SensorSerial + "_dark";

                        // Create dark image
                        CreateTifImage(ImageAcq[Index].PathOfDarkFile, blnDark: true);

                        // Write message to log.
                        WriteLog("HPK_GetXrayImage returned 0x" + result.ToString("X4") + Environment.NewLine +
                                 "BufferLength: " + BuffLength.ToString() + Environment.NewLine +
                                 "Acquiring dark image for SN" + SensorSerial + " succeeded.");

                        return true;
                    }
                    else
                    {
                        // Write error message to log.
                        WriteLog("HPK_GetXrayImage returned 0x" + result.ToString("X4") + Environment.NewLine +
                                 "BufferLength: " + BuffLength.ToString() + Environment.NewLine +
                                 "Acquiring dark image for SN" + SensorSerial + " failed.");

                        // show error message
                        MessageBox.Show("Initialization failed." + Environment.NewLine +
                                        "Please reconnect all the sensors and click the power button.",
                                        "BufferLength error", MessageBoxButton.OK, MessageBoxImage.Error);

                        // Leave the loop.
                        return false;
                    }
                }
                else
                {
                    // Write error message to log.
                    WriteLog("HPK_GetXrayImage returned 0x" + result.ToString("X4") + Environment.NewLine +
                             "Acquiring dark image for SN" + SensorSerial + " failed.");

                    // show error message
                    MessageBox.Show("Initialization failed." + Environment.NewLine +
                                    "Please reconnect all the sensors and click the power button.",
                                    "Image acquisition error", MessageBoxButton.OK, MessageBoxImage.Error);

                    // Leave the loop.
                    return false;
                }
            }
            else
            {
                // Leave the loop.
                return false;
            }
        }
        
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OnElapsed_Timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void OnElapsed_Timer(object sender, System.Timers.ElapsedEventArgs e)
        {
            // タイマー停止
            timer.Stop();

            mblnTimeout = true;
            MessageBox.Show("Initialization failed." + Environment.NewLine +
                            "Please reconnect all the sensors and click the power button.",
                            "Timeout error", MessageBoxButton.OK, MessageBoxImage.Error);
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetSensorInfo
        ///     Function to carry out HPK_GetSensorInformation.
        /// </summary>
        /// <param name="devHandle"></param>
        /// <param name="SensorInformation"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private uint GetSensorInfo(IntPtr devHandle, out CMOS_USB.UnitSensorInformation SensorInformation)
        {
            uint result = 1;
            CMOS_USB.UnitIntegrationParameter IntegParam;

            // Initialization for UnitSensorInformation.
            SensorInformation.Lot_Serial_No = 0x0000;
            SensorInformation.Sensor_Type = 0x00;
            SensorInformation.Firmware_Version = 0x00;

            // Initialization for UnitIntegrationParameter.
            IntegParam.Xray_Incident_Threshold = 440;
            IntegParam.Xray_Integration_End_Threshold = 437;
            IntegParam.Integration_Time = 1;

            // Get the Sensor information
            result = CMOS_USB.HPK_GetSensorInformation(devHandle, ref IntegParam, out SensorInformation);

            return result;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WriteLogSelectDevice
        ///     Write the selected device in the log.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void WriteLogSelectDevice()
        {
            uint success = 0;
            IntPtr DeviceHandle = new IntPtr(-1);
            CMOS_USB.UnitIntegrationParameter IntegParam;
            CMOS_USB.UnitSensorInformation SensorInfo = new CMOS_USB.UnitSensorInformation();

            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];
            IntegParam.Xray_Incident_Threshold = 440;
            IntegParam.Xray_Integration_End_Threshold = 437;
            IntegParam.Integration_Time = 1;

            success = CMOS_USB.HPK_GetSensorInformation(DeviceHandle, ref IntegParam, out SensorInfo);

            if (success == 0)
            {
                if (SensorInfo.Sensor_Type < 0x80) { mblnSize = true; }
                else { mblnSize = false; }
            }
            else { return; }

            string strSelect = SensorInfo.Sensor_Type.ToString("X2") + SensorInfo.Lot_Serial_No.ToString("X4");
            WriteLog("Select Device SN: " + strSelect);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CloseAllDevice
        ///     Releases all device handle.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CloseAllDevice()
        {
            // Write to log file.
            WriteLog("**********************************************************************************");

            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                // Releases a device handle.
                CMOS_USB.USB_CloseDevice(DeviceHandle_List[i]);

                // Write returned value to log file.
                WriteLog("HPK_Closedevice was called.");
            }

            // 0 clear
            DEVICE_CONNECTED = 0;

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Change of status displaying
                lblStatus.Content = "Click left icon";
                lblStatus.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x91, 0x91, 0x91));
            }));

            // Initialization for UintImageAcq.
            InitialStruct();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GoToSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GoToSuspend()
        {
            // Suspend delay 1 [sec]
            uint time = 1000;

            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // USB_SuspendDevice
            uint result = CMOS_USB.USB_SuspendDevice(DeviceHandle, time);

            // Write returned value to log file.
            WriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4"));
            if (mblnCalFile) { FlatFieldWriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4")); }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// AllDeviceSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool AllDeviceSuspend()
        {
            uint time = 1000;       // Suspend delay 1 [sec]
            int iLoop = 0;          // variable for loop

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // USB_SuspendDevice
                uint result = CMOS_USB.USB_SuspendDevice(DeviceHandle_List[iLoop], time);

                // Write returned value to log file.
                WriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4"));

                // Failed to get a pipe handle.
                if (result != 0x00)
                {
                    // show error message
                    MessageBox.Show("USB_SuspendDevice failed." + Environment.NewLine +
                                    "Please restart operation with the following steps." + Environment.NewLine +
                                    "1st; Click OK of this popup." + Environment.NewLine +
                                    "2nd; Disconnect the sensor." + Environment.NewLine +
                                    "3rd; Close S11684S11685.exe." + Environment.NewLine +
                                    "4th; Connect the sensor to another USB port." + Environment.NewLine +
                                    "5th; Execute S11684S11685.exe.",
                                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    // If USB_SuspendDevice failed, it may be necessary to reconnect the sensor to another USB port.
                    cboxSensor.Items.Add("**reconnect the sensor**");
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// StopSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void StopSuspend()
        {
            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // USB_ResumeDevice
            uint result = CMOS_USB.USB_ResumeDevice(DeviceHandle);

            // Write returned value to log file.
            WriteLog("USB_ResumeDevice returned 0x" + result.ToString("X4"));
            if (mblnCalFile) { FlatFieldWriteLog("USB_ResumeDevice returned 0x" + result.ToString("X4")); }
        }











        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Thread function                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_SearchSesor_DoWork(object sender, DoWorkEventArgs e)
        {
            // Initialize flag
            mblnGetSs = false;

            // Write to log file.
            WriteLog("**********************************************************************************");

            // Opens the sensors
            if (!OpenDevice()) { CloseAllDevice(); }

            // Get pipe handle 
            else if (!GetPipeHandle()) { CloseAllDevice(); }

            // Get sensor serial
            else if (!GetSensorSerial()) { CloseAllDevice(); }

            // Get dark image
            else if (!GetDarkImage()) { CloseAllDevice(); }

            // Enables the suspended state.
            else if (!AllDeviceSuspend()) { CloseAllDevice(); }

            // If there is no problem set the flag.
            else { mblnGetSs = true; }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_SearchSesor_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // When the flag is set.
            if (mblnGetSs)
            {
                // Enable other than stop button.
                ChangeEnabled(true, true, blnOther: true);

                // Select the first sensor.
                cboxSensor.SelectedIndex = 0;

                // Change of status displaying
                lblStatus.Content = "Ready";
                lblStatus.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xFF, 0x00));
            }

            // When the flag is not set.
            else
            {
                // Enable only the power button.
                ChangeEnabled(true);
            }

            // Set the image source.
            ChangePowerButton();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_AcqImage_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_AcqImage_DoWork(object sender, DoWorkEventArgs e)
        {
            int AcquisitionTime = 0;
            int AcqCount = 0;
            string SensorSerial = "";
            bool blnMapImg = false;

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Stop Suspend mode.
                StopSuspend();

                /* Preparations for imange acquisition. */
                PreparationImgAcq();

                // Setting parameter
                XrayImage.Mode = ImageAcq[cboxSensor.SelectedIndex].Mode;                                                   // Integration mode
                XrayImage.IntegParam.Xray_Incident_Threshold = ImageAcq[cboxSensor.SelectedIndex].StartThreshold;           // Threshold to start integration
                XrayImage.IntegParam.Xray_Integration_End_Threshold = ImageAcq[cboxSensor.SelectedIndex].EndThreshold;      // Threshold to end integration
                XrayImage.IntegParam.Integration_Time = ImageAcq[cboxSensor.SelectedIndex].IntegTime;                       // Integration time [ms]
                if (mblnCalFile)
                {
                    AcquisitionTime = (ImageAcq[cboxSensor.SelectedIndex].AcqTimes == 0) ? 3 : 10;                          // Correction images to acquire

                    // Get the sensor serial
                    SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();
                }
            }));

            // When background operation is not canceled.
            while (Worker_AcqImage.CancellationPending == false)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    // for Frat Field Frame.
                    if (mblnCalFile)
                    {
                        lblMessage.Content = ("Expose an X-ray without any object." + Environment.NewLine);

                        if (AcqCount == 0) { lblMessage.Content += ("1st of " + AcquisitionTime.ToString() + " images "); }
                        else if (AcqCount == 1) { lblMessage.Content += ("2nd of " + AcquisitionTime.ToString() + " images "); }
                        else if (AcqCount == 2) { lblMessage.Content += ("3rd of " + AcquisitionTime.ToString() + " images "); }
                        else { lblMessage.Content += ((AcqCount + 1).ToString() + "th of " + AcquisitionTime.ToString() + " images "); }
                    }
                }));

                // Acquiring an image in background operation.
                Worker_XrayImage.RunWorkerAsync();

                // set a flag
                mblnXrayImage = true;

                // Wait until the finish
                while (Worker_XrayImage.IsBusy) { System.Threading.Thread.Sleep(10); }
                while (mblnXrayImage) { System.Threading.Thread.Sleep(10); }

                // Stop button was not clicked.
                if (!mblnStop)
                {
                    if ((BuffLength == Buffer_Size && mblnSize) || (BuffLength == Buffer2_Size && !mblnSize))
                    {
                        int PixelNum = (mblnSize) ? SIZE_IMAGE : SIZE2_IMAGE;

                        //-----Output range and dose-----//
                        OutputRangeAndDoseCurrent(PixelNum);

                        //----- Create a "Gain Frame" image ----//
                        if (mblnCalFile)
                        {
                            // Obtain an averaged image
                            ObtainAveImg(ref AcqCount);

                            // set a flag
                            blnMapImg = true;

                            // Save the flat field map image.
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                            {
                                // [ "C:\HAMAMATSU\S11684S11685\Device\xxxxxx_frame_y_forCal.tif" ]
                                string fileName = SensorSerial + "_frame" + AcqCount.ToString() + "_forCal";
                                string FileName = General.CreateDeviceDirectoryName() + fileName;
                                CreateTifImage(FileName, blnMapImg);

                                // Write Log
                                WriteLog("Create file of \"" + fileName + ".tif\"");
                                FlatFieldWriteLog("Create file of \"" + fileName + ".tif\"");
                            }));

                            // If it is finished for the specified number of times.
                            if (AcqCount == AcquisitionTime)
                            {
                                // Calculate the average.
                                for (int i = 0; i < PixelNum; i++)
                                {
                                    // Averaged image
                                    pixAverageImage[i] = (pixAverageImage[i] / AcquisitionTime);
                                }

                                // Mean value used for flat field correction
                                double FlatMean = 0;
                                AverageFlatMean(ref FlatMean, pixAverageImage);

                                // This is the map image of flat field correction.
                                for (int i = 0; i < PixelNum; i++)
                                {
                                    if (pixAverageImage[i] == 0) { pixAverageImage[i] = 1; }

                                    // Multiplying 1000 in order to make integer data.
                                    pixFFFrameImage[i] = (ushort)(1000.0 / pixAverageImage[i] * FlatMean);
                                }

                                // Making the value of not active pixels 0 LSB.
                                NotActivePixels(ref pixFFFrameImage);

                                // flag clear
                                blnMapImg = false;

                                // Save the gain frame image.
                                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                                {
                                    // folder path [ "C:\HAMAMATSU\S11684S11685\Device\" ]
                                    string FileName = General.CreateDeviceDirectoryName() + "SN" + SensorSerial;
                                    CreateTifImage(FileName, blnMapImg);        // create a "SNxxxxxx.tif" image file.
                                    CreateCalFile(FileName);                    // create a "SNxxxxxx.CAL" file.

                                    // Write Log
                                    WriteLog("Create file of \"SN" + SensorSerial + ".tif\"");
                                    WriteLog("Create file of \"SN" + SensorSerial + ".cal\"");
                                    FlatFieldWriteLog("Create file of \"SN" + SensorSerial + ".tif\"");
                                    FlatFieldWriteLog("Create file of \"SN" + SensorSerial + ".cal\"");
                                }));

                                // End of the image acquisition.
                                Worker_AcqImage.CancelAsync();
                            }
                        }

                        else
                        {
                            // Create a Flat Field Correction image.
                            if (mblnCorrImg)
                            {
                                for (int i = 0; i < PixelNum; i++)
                                {
                                    // This is the image with flat field correction.
                                    pixFFCorrImage[i] = (ushort)(0x8000 | (ushort)(pixOrigImage[i] * pixFFFrameImage[i] / 1000.0));
                                    pixOrigImage[i] = (ushort)(pixOrigImage[i] * pixFFFrameImage[i] / 1000.0);
                                }
                            }

                            // End of the image acquisition.
                            Worker_AcqImage.CancelAsync();
                        }

                        //-----Calculate the contrast image data.-----//
                        CalculateContrast(pixOrigImage, PixelNum);

                        //-----Gamma correction.-----//
                        if (mblnGamma)
                        {
                            // 0 clear
                            pixGammaCorrImage = new byte[SIZE2_IMAGE];
                            GammaCorrectionImage(pixAutoContImage, PixelNum);
                        }

                        //-----Show image-----//
                        ShowImage(pixAutoContImage, mblnGamma);
                    }
                    else
                    {
                        WriteLog("Image acquisition failed.");
                        MessageBox.Show("Image acquisition failed.", mMessageTitle, MessageBoxButton.OK, MessageBoxImage.Error);
                        Worker_AcqImage.CancelAsync();
                        break;
                    }
                }
                else
                {
                    Worker_AcqImage.CancelAsync();
                    break;
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_AcqImage_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_AcqImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Stop button was not clicked.
            if (!mblnStop)
            {
                // Enables the suspended state.
                GoToSuspend();

                // Enable other than stop button.
                ChangeEnabled(true, true, blnOther: true);

                // set a stop flag
                mblnStop = true;

                // Confirm whethre a Contrast Window is showed.
                windowContrast = Application.Current.Windows.OfType<ContrastWindow>().FirstOrDefault();

                if (windowContrast != null)
                {
                    // Update a parameter.
                    windowContrast.updateParameter(mbLowContrast, mbHighContrast);

                    // Update of the histogram.
                    updatePixelHistogram(pixOrigImage);
                }

                // Change the magnification of the image.
                if (DisplayRange == 0x0) { FitToView(); }
                else if (DisplayRange == 0x1) { Display100image(); }
                else if (DisplayRange == 0x2) { Display200image(); }

                // Show message
                if (mblnCalFile)
                {
                    MessageBox.Show("The gain frame has created successfully.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

            // clear flag
            ImageAcq[cboxSensor.SelectedIndex].CreateCalFile = false;

            // clear label
            lblMessage.Content = "";
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_XrayImage_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_XrayImage_DoWork(object sender, DoWorkEventArgs e)
        {
            // Preparation of variables.
            uint result = 0;                    // Return value of the function.
            byte ImgCorr = 0;                   // HPK_GetXrayImage / HPK_GetXrayCorrectionImage
            ExposureXray window = null;         // Exposure an X-ray window
            BuffLength = 0;
            XryaImageBuffer = new byte[4450000];


            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Setting parameter
                ImgCorr = ImageAcq[cboxSensor.SelectedIndex].ImgCorrType;

                // Display window.
                window = new ExposureXray();
                window.Owner = Window.GetWindow(this);
                window.Show();
            }));

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // RAW image
            if (ImgCorr == 0x00 || mblnSubStoredImage)
            {
                // HPK_GetXrayImage
                result = CMOS_USB.HPK_GetXrayImage(DeviceHandle, XryaImageBuffer, out BuffLength, ref XrayImage);

                // store result of HPK_GetXrayImage before writing log.
                ResultImage = ("HPK_GetXrayImage returned 0x" + result.ToString("X4") + Environment.NewLine + "BufferLength: " + BuffLength.ToString());
            }

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Dark-subtracted image
            else
            {
                // HPK_GetXrayCorrectionImage
                result = CMOS_USB.HPK_GetXrayCorrectionImage(DeviceHandle, XryaImageBuffer, out BuffLength, ref XrayImage);

                // store result of HPK_GetXrayImage before writing log.
                ResultImage = ("HPK_GetXrayCorrectionImage returned 0x" + result.ToString("X4") + Environment.NewLine + "BufferLength: " + BuffLength.ToString());
            }

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Close window
                if (window != null) window.Close();
            }));

            // Write result of image function to log file.
            WriteLog(ResultImage);
            if (mblnCalFile) { FlatFieldWriteLog(ResultImage); }

            // Title of error message.
            if (result != 0) { mMessageTitle = "Error"; }
            else { mMessageTitle = "BufferLength error"; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_XrayImage_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_XrayImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((!mblnStop) && (XrayImage.Mode == 0x01 || XrayImage.Mode == 0x02 || XrayImage.Mode == 0x03))
            {
                if (XryaImageBuffer[1] == (XrayImage.Mode << 4))
                {
                    ushort pixShort, pixDark = 0;
                    int j = 0;
                    int Loop_Limit = (mblnSize) ? Buffer_Size : Buffer2_Size;       // Size1->Buffer_Size / Size2->Buffer2_Size

                    // 0 clear
                    pixOrigImage = new ushort[SIZE2_IMAGE];
                    pixEditImage = new ushort[SIZE2_IMAGE];

                    // Get the pixel data of the image.
                    for (int i = 14; i < (Loop_Limit - 2); i += 2)
                    {
                        pixShort = (ushort)BitConverter.ToUInt16(XryaImageBuffer, i);
                        if (mblnSubStoredImage) { pixDark = pixDarkImage[j]; }

                        if (pixShort >= pixDark)
                            pixShort = (ushort)(pixShort - pixDark);
                        else
                            pixShort = 0;

                        pixOrigImage[j] = pixShort;
                        pixEditImage[j++] = (ushort)(0x8000 | pixShort);
                    }
                }
            }

            else
            {
                // Requests cancellation of a ending background operation.
                Worker_AcqImage.CancelAsync();
            }

            // clear a flag.
            mblnXrayImage = false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_StopAcq_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_StopAcq_DoWork(object sender, DoWorkEventArgs e)
        {
            // Wait if an asynchronous operation is running.
            while (Worker_XrayImage.IsBusy) ;

            uint result = 0;
            XrayImage.Mode = 0x08;
            result = CMOS_USB.HPK_ForceTrigAndGetDummy(DeviceHandle, XryaImageBuffer, out BuffLength, ref XrayImage);

            // Write returned value to log file.
            WriteLog("HPK_ForceTrigAndGetDummy returned 0x" + result.ToString("X4"));
            if (mblnCalFile) { FlatFieldWriteLog("HPK_ForceTrigAndGetDummy returned 0x" + result.ToString("X4")); }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_StopAcq_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_StopAcq_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Enables the suspended state.
            GoToSuspend();

            // Enable other than stop button.
            ChangeEnabled(true, true, blnOther: true);

            // set a flag
            mblnStop = true;
        }

    }
}
