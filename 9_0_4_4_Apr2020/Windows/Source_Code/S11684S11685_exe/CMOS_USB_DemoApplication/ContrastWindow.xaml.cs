﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// ContrastWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ContrastWindow : Window
    {
        //=====================================================================================================================================
        //=                                                                                                                                   =
        //=                                                        Private constant                                                           =
        //=                                                                                                                                   =
        //=====================================================================================================================================
        private const int SENSOR_MAX = 255;                                                 /* The maximum that sensor can use. */



        //=====================================================================================================================================
        //=                                                                                                                                   =
        //=                                                        Private variable                                                           =
        //=                                                                                                                                   =
        //=====================================================================================================================================
        private byte mbLowContrast;                                                         /* Low Contrast */
        private byte mbHighContrast;                                                        /* High Contrast */

        private bool mblnInit;


        //=====================================================================================================================================
        //=                                                                                                                                   =
        //=                                                        Public variable                                                            =
        //=                                                                                                                                   =
        //=====================================================================================================================================
        public MainWindow main;






        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //------------------------------------------------------------------------------------------------------------------------------------
        public ContrastWindow()
        {
            mblnInit = false;

            InitializeComponent();

            sliderHighCon.Maximum = SENSOR_MAX;
            sliderHighCon.Minimum = 0;
            sliderLowCon.Maximum = SENSOR_MAX;
            sliderLowCon.Minimum = 0;
            sliderHighCon.Value = byte.Parse(lblHighContrast.Content.ToString());
            sliderLowCon.Value = byte.Parse(lblLowContrast.Content.ToString());
            sliderHighCon.SmallChange = 1;
            sliderHighCon.LargeChange = 5;
            sliderLowCon.SmallChange = 1;
            sliderLowCon.LargeChange = 5;

            mblnInit = true;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is laid out, rendered, and ready for interantion.
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblHighContrast.Content = mbHighContrast;
            lblLowContrast.Content = mbLowContrast;
            sliderHighCon.Value = mbHighContrast;
            sliderLowCon.Value = mbLowContrast;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        // Restore button
        private void mnuReset_Click(object sender, RoutedEventArgs e)
        {
            // Restore value
            sliderHighCon.Value = mbHighContrast;
            sliderLowCon.Value = mbLowContrast;
            main.SetLowContrast((byte)sliderLowCon.Value, (byte)sliderHighCon.Value);
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        // Exit button
        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //======================================================================================================================================
        //=                                                                                                                                    =
        //=                                                          Public function                                                           =
        //=                                                                                                                                    =
        //======================================================================================================================================

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// setParent
        /// </summary>
        /// <param name="MainWindow"></param>
        /// <param name="bLowContrast"></param>
        /// <param name="bHighContrast"></param>
        //------------------------------------------------------------------------------------------------------------------------------------
        public void setParent(MainWindow MainWindow, byte bLowContrast, byte bHighContrast)
        {
            main = MainWindow;

            // on the left.
            this.Left = 0;
            this.Top = 0;

            mbLowContrast = bLowContrast;
            mbHighContrast = bHighContrast;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// updateParameter
        ///     Update of the parameter.
        /// </summary>
        /// <param name="bLowContrast"></param>
        /// <param name="bHighContrast"></param>
        //------------------------------------------------------------------------------------------------------------------------------------
        public void updateParameter(byte bLowContrast, byte bHighContrast)
        {
            // The setting of the parameter.
            mbLowContrast = bLowContrast;
            mbHighContrast = bHighContrast;

            // The update of the parameter.
            lblHighContrast.Content = mbHighContrast;
            lblLowContrast.Content = mbLowContrast;
            sliderHighCon.Value = mbHighContrast;
            sliderLowCon.Value = mbLowContrast;
        }


        //======================================================================================================================================
        //=                                                                                                                                    =
        //=                                                          Private function                                                          =
        //=                                                                                                                                    =
        //======================================================================================================================================

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// sliderLowCon_ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //------------------------------------------------------------------------------------------------------------------------------------
        private void sliderLowCon_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (mblnInit)
            {
                lblLowContrast.Content = (byte)sliderLowCon.Value;
                if (sliderLowCon.Value > sliderHighCon.Value)
                {
                    sliderHighCon.Value = sliderLowCon.Value;
                }
                main.SetLowContrast((byte)sliderLowCon.Value, (byte)sliderHighCon.Value);
            }
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// sliderHighCon_ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //------------------------------------------------------------------------------------------------------------------------------------
        private void sliderHighCon_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (mblnInit)
            {
                lblHighContrast.Content = (byte)sliderHighCon.Value;
                if (sliderLowCon.Value > sliderHighCon.Value)
                {
                    sliderLowCon.Value = sliderHighCon.Value;
                }
                main.SetHighContrast((byte)sliderLowCon.Value, (byte)sliderHighCon.Value);
            }
        }       

    }
}
