﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.ComponentModel;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using WinUsbSample;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// TrigPdTesting.xaml の相互作用ロジック
    /// </summary>
    public partial class TrigPdTesting : Window
    {
        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private constant                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private const byte MAXIMUM_DEVICE_COUNT = 3;                                                                // Set maximum number of devices being operated at a time
        private const string WINUSB_DEVICEINTERFACE_GUID_STRING = "{6C477250-0AED-44a8-95B7-992DCA6C7CB9}";         // CMOS Device of GUID

        private const Int32 WM_DEVICECHANGE = 0X219;                                                                /* This message comes when USB connection changes. */
        private const Int32 DBT_DEVICEREMOVECOMPLETE = 0X8004;                                                      /* disconnection of the sensor*/
        private const Int32 DBT_DEVICEARRIVAL = 0X8000;                                                             /* connection of the sensor*/




        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                            Public variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        public IntPtr[] DeviceHandle_List = new IntPtr[MAXIMUM_DEVICE_COUNT];
        public IntPtr DeviceHandle = new IntPtr(-1);





        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           Private variable                                                                            =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================
        private IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
        private byte DEVICE_CONNECTED = 0;                                                                          /* Number of connected devices */

        private DeviceManagement myDeviceManagement = new DeviceManagement();                                       /* Classes for receiving events of the sensors */
        private String myDevicePathName;
        private IntPtr deviceNotificationHandle;
        private bool myDeviceDetected = false;

        private BackgroundWorker Worker_SearchSesor;                                                                // USB Device Serch
        private BackgroundWorker Worker_TrigPd;                                                                     // Get Trig Pd Data
        private BackgroundWorker Worker_DisplayCount;                                                               // Displays the number of got trig pd data.


        private LineGraph TrigPD_Graph;                                                                             // dose rate graph
        private LineGraph Frequency;                                                                                // histogram graph

        private uint AcquisitionCount = 2092;                                                                       // Acquistion Count
        private uint DataCount;
        private ushort[] TrigDataBuff = new ushort[2092];                                                           // Data buffer for storing trigger pd data
        private bool mblnDisplayCount;

        private LogWindow windowLog = null;                                                                         // LogWindow
        private bool mblnGetSs;                                                                                     // Flag indicating whether sensor serial was acquired.
        private byte mbyStopFlag;                                                                                   // 1: Stop acquiring Trig PD Data.



        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public TrigPdTesting()
        {
            InitializeComponent();

            // Disable control
            ChangeEnabled();

            // Initialize flag
            mblnGetSs = false;
            mbyStopFlag = 1;
            mblnDisplayCount = false;

            // Log textbox clear
            txtLog.Clear();

            // Add version to title
            this.Title += " (ver " + General.GetFileVersion() + ")";

            // Initialization of BackgroundWorker
            InitBackgroundWorker();

            // Determines whether the log file exists.
            string strInfo = "";
            if (!LogFileExists())
            {
                // Get the system information
                General.PcSystemInfo(out strInfo);
                WriteLog(strInfo);
            }

            // Log the file version.
            strInfo = "";
            General.SoftwareInfo(out strInfo);
            WriteLog(strInfo);

            // Enable only the power button.
            ChangeEnabled(true);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = Worker_TrigPd.IsBusy;

        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Occus when the window is about to close.
        private void Window_Closed(object sender, EventArgs e)
        {
            // Close device
            CloseAllDevice();

            // Log the driver info.
            string strInfo = "";
            General.DriverInfo(out strInfo);
            WriteLog(strInfo);
            WriteLog("Window Closed");
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Click power button
        private void btnPower_Click(object sender, RoutedEventArgs e)
        {
            // Disable control
            ChangeEnabled();

            if (DEVICE_CONNECTED != 0)
            {
                // Close device
                CloseAllDevice();

                // Enable only the power button.
                ChangeEnabled(true);

                // Set the image source.
                mblnGetSs = false;
                ChangePowerButton();

                // Clear the sensor serial combobox
                cboxSensor.Items.Clear();

                return;
            }

            // Change of status displaying
            lblStatus.Content = "Initializing";

            // Register the device in order to receive notifications
            ConnectDevice();

            // Starts execution of a background operation.
            Worker_SearchSesor.RunWorkerAsync();
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Start of get trig pd data.
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // Enable only the stop button.
            ChangeEnabled(blnStop: true);
            btnStart.Content = "Now Acquiring";

            // Write to log file.
            WriteLog("**********************************************************************************");

            // When asynchronous operation is not running.
            if (Worker_TrigPd.IsBusy == false)
            {
                // Starts execution of a background operation.
                Worker_TrigPd.RunWorkerAsync();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Stop trig pd data.
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            // Disable control
            ChangeEnabled();

            // set a flag
            mbyStopFlag = 1;

            uint result = 0;
            result = CMOS_OPTION.HPK_AbortBulkPipe(DeviceHandle);

            // Write returned value to log file.
            WriteLog("HPK_AbortBulkPipe returned 0x" + result.ToString("X4"));

            // Requests cancellation of a pending background operation.
            Worker_TrigPd.CancelAsync();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void mnuScreenCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Desktop = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string FileName = Desktop + @"\Screenshot " + now.ToString("yyyy-MM-dd HH.mm.ss") + @".jpg";

                using (var fs = new FileStream(FileName, FileMode.Create))
                {
                    BitmapEncoder enc = new JpegBitmapEncoder();
                    BitmapSource ibmp = General.CopyScreen();

                    if (ibmp != null)
                    {
                        enc.Frames.Add(BitmapFrame.Create(ibmp));
                        enc.Save(fs);
                    }

                    fs.Close();
                }
            }
            catch
            {
                return;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void mnuOpenLog_Click(object sender, RoutedEventArgs e)
        {
            // Confirm whethre a Contrast Window is showed.
            windowLog = Application.Current.Windows.OfType<LogWindow>().FirstOrDefault();

            if (windowLog != null)
            {
                // Attempts to bring the window to the foreground and activates it.
                windowLog.Activate();
            }
            else
            {
                // Instance of class created.
                windowLog = new LogWindow();
                windowLog.blnTrigPdData = true;
                windowLog.Show();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void mnuClearLog_Click(object sender, RoutedEventArgs e)
        {
            txtLog.Clear();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // TextBox to the numbers only input so as not to be
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !new Regex("[0-9]").IsMatch(e.Text);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void txtAcqCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((txtAcqCount.Text.Length > 0) && (mblnGetSs))
            {
                if (int.Parse(txtAcqCount.Text) > 60000)
                {
                    chkExportData.IsChecked = false;
                    chkExportData.IsEnabled = false;
                }
                else
                {
                    chkExportData.IsEnabled = true;
                }
            }
        }





















        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Private function                                                                         =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ChangeEnabled
        ///     Enable / disable control.
        /// </summary>
        /// <param name="blnPower"></param>
        /// <param name="blnAcquire"></param>
        /// <param name="blnStop"></param>
        /// <param name="blnOther"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ChangeEnabled(bool blnPower = false, bool blnStart = false, bool blnStop = false, bool blnOther = false)
        {
            // Button control
            btnPower.IsEnabled = blnPower;                  // Power button
            btnStart.IsEnabled = blnStart;                  // Start button
            btnStop.IsEnabled = blnStop;                    // Exit image button

            // Other controls
            gpbSensor.IsEnabled = blnOther;                 // Sensor serial groupbox
            grbParameters.IsEnabled = blnOther;             // Parameters groupbox
            gpbOptions.IsEnabled = blnOther;                // Options groupbox
            mnuFile.IsEnabled = blnPower;                   // [menu] -> [File]
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// InitBackgroundWorker
        ///     Initialization of each BackgroundWorker.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void InitBackgroundWorker()
        {
            // For USB Device Serch
            Worker_SearchSesor = new BackgroundWorker();
            Worker_SearchSesor.DoWork += new DoWorkEventHandler(Worker_SearchSesor_DoWork);
            Worker_SearchSesor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_SearchSesor_RunWorkerCompleted);
            Worker_SearchSesor.WorkerSupportsCancellation = true;


            // For monitoring pd data
            Worker_TrigPd = new BackgroundWorker();
            Worker_TrigPd.DoWork += new DoWorkEventHandler(Worker_TrigPd_DoWork);
            Worker_TrigPd.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_TrigPd_RunWorkerCompleted);
            Worker_TrigPd.WorkerSupportsCancellation = true;


            // For acquire image
            Worker_DisplayCount = new BackgroundWorker();
            Worker_DisplayCount.DoWork += new DoWorkEventHandler(Worker_DisplayCount_DoWork);
            Worker_DisplayCount.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_DisplayCount_RunWorkerCompleted);
            Worker_DisplayCount.WorkerSupportsCancellation = true;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// LogFileExists
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool LogFileExists()
        {
            DateTime LogTime = DateTime.Now;
            string Directory = General.CreateLogDirectoryName();
            string logFileName = Directory + "\\" + "Log_Dose_(" + LogTime.ToString("ddMMyy") + ").txt";

            // Determines whether the specified file exists.
            if (File.Exists(logFileName))
            {
                return true;
            }

            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WriteLog
        /// </summary>
        /// <param name="WrittenWords"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void WriteLog(String WrittenWords)
        {
            if (App.blnWriteLog)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    StreamWriter LogText = null;
                    try
                    {
                        // Get current time.
                        DateTime LogTime = DateTime.Now;

                        // Path
                        string logFilePath = General.CreateLogDirectoryName();

                        // Open Log, and add line.
                        LogText = new StreamWriter(logFilePath + "\\" + "Log_TrigData_(" + LogTime.ToString("ddMMyy") + ").txt", true, Encoding.GetEncoding("Shift-JIS"));

                        // Create an instance
                        System.IO.StringReader rs = new System.IO.StringReader(WrittenWords);

                        // Respeated until the end of the stream.
                        while (rs.Peek() > -1)
                        {
                            // Write log.
                            LogText.WriteLine(LogTime.ToString("HH:mm:ss.fff") + " " + rs.ReadLine());
                        }
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(WrittenWords + "\r\n" + exp.Message);
                    }
                    finally
                    {
                        // Close log.
                        if (LogText != null) { LogText.Close(); }
                    }
                }));
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ChangePowerButton
        ///     Change image of power button
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ChangePowerButton()
        {
            // Create source.
            BitmapImage image = new BitmapImage();
            image.BeginInit();

            // When the flag is set.
            if (mblnGetSs)
            {
                image.UriSource = new Uri("power_button_on.png", UriKind.Relative);
            }

            // When the flag is not set.
            else
            {
                image.UriSource = new Uri("power_button_off.png", UriKind.Relative);
            }

            image.EndInit();

            // Set the image source.
            imgPower.Source = image;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OnSourceInitialized
        /// </summary>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        protected override void OnSourceInitialized(EventArgs e)
        {
            HwndSource source = (HwndSource)HwndSource.FromVisual(this);
            source.AddHook(new HwndSourceHook(WndProc));
            base.OnSourceInitialized(e);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WndProc
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // Reopening device handles when sensors are connected or disconnected.
            if (msg == WM_DEVICECHANGE)
            {
                handled = true;

                // USB devices is disconnected.
                if (wParam.ToInt32() == DBT_DEVICEREMOVECOMPLETE)
                {
                    if (myDeviceManagement.DeviceNameMatch(lParam, myDevicePathName) == true)
                    {
                        // Disable control
                        ChangeEnabled();

                        // wait
                        while (Worker_TrigPd.IsBusy) ;

                        // Releases all device handle.
                        CloseAllDevice();

                        // Enable only the power button.
                        ChangeEnabled(true);

                        // Set the image source.
                        mblnGetSs = false;
                        ChangePowerButton();

                        // Clear the sensor serial combobox
                        cboxSensor.Items.Clear();
                    }
                }

            }
            return IntPtr.Zero;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ConnectDevice
        ///     Searching for sensors with the device interface GUID. Registering the device so that application software can receive notifications.
        ///     Create  :   15/11/19
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ConnectDevice()
        {
            bool deviceFound;
            String devicePathName = "";
            bool success;

            try
            {
                if (myDeviceDetected != false)
                {
                    myDeviceManagement.StopReceivingDeviceNotifications(deviceNotificationHandle);
                    myDeviceDetected = false;
                }

                // Convert the device interface GUID String to a GUID object:
                System.Guid winUsbDemoGuid = new System.Guid(WINUSB_DEVICEINTERFACE_GUID_STRING);

                // Fill an array with the device path names of all attached devices with matching GUIDs.
                deviceFound = myDeviceManagement.FindDeviceFromGuid(winUsbDemoGuid, ref devicePathName);

                if (deviceFound == true)
                {
                    myDeviceDetected = true;

                    // Save DevicePathName so WndProc(,,,,) knows which name is my device.
                    myDevicePathName = devicePathName;
                }

                if (myDeviceDetected == true)
                {
                    // Handle to window.
                    HwndSource source = HwndSource.FromVisual(this) as HwndSource;

                    // Register to receive notifications when the device is removed or attached.
                    success = myDeviceManagement.RegisterForDeviceNotifications(myDevicePathName,
                                                                                source.Handle,
                                                                                winUsbDemoGuid,
                                                                                ref deviceNotificationHandle);
                }

                return myDeviceDetected;
            }
            catch (Exception) { throw; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ShowCountNumber
        /// </summary>
        /// <param name="strMessage"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void ShowCountNumber(string strDisplay)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                lblCount.Content = strDisplay;
            }));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GenerateHistograms
        ///     Generate histograms of valid data.
        /// </summary>
        /// <param name="TrigPdData"></param>
        /// <param name="Histograms"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GenerateHistograms(ushort[] TrigPdData, int[] Histograms)
        {
            for (int i = 0; i < AcquisitionCount; i++)
            {
                Histograms[TrigPdData[i]]++;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetOffsetMode
        ///     Get ADC offset mode.
        /// </summary>
        /// <param name="Histograms"></param>
        /// <param name="offset"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GetOffsetMode(int[] Histograms, ref int offset)
        {
            int max = 0;

            // Find the peak value of the histogram.
            for (int i = 400; i < 1024; i++)
            {
                if (Histograms[i] >= max)
                {
                    max = Histograms[i];
                    offset = i;
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetSigma
        ///     Calculate sigma.
        /// </summary>
        /// <param name="TrigPdData"></param>
        /// <param name="sigma"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GetSigma(ushort[] TrigPdData, ref decimal sigma)
        {
            int sum = 0;
            uint total = AcquisitionCount;
            double Average = 0.00D;
            double StandardDeviation = 0.00D;
            ushort[] tempTrigData = new ushort[total];

            // Copy
            Array.Copy(TrigPdData, tempTrigData, total);

            // Calculate the sum.
            for (int i = 0; i < total; i++)
            {
                sum += tempTrigData[i];
            }

            // Calculate the average value.
            Average = (double)sum / total;

            // Calculate standard deviation.
            foreach (double Value in tempTrigData)
            {
                StandardDeviation += (Value - Average) * (Value - Average);
            }

            // Calculate sigma
            sigma = (decimal)Math.Sqrt(StandardDeviation / total);
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// UpdateTrigDataGraph
        ///     Update the trig pd data graph.
        /// </summary>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void UpdateTrigDataGraph()
        {
            int iCount = 0;
            for (int i = 0; i < AcquisitionCount; i++)
            {
                if (TrigDataBuff[i] != 0) { iCount++; }
            }

            int j = 0;
            ulong k = 0;
            double[] x = new double[iCount];
            double[] y = new double[iCount];
            double max_data = 0;
            double min_data = 1024;

            while (k < AcquisitionCount)
            {
                if (TrigDataBuff[k] != 0 && TrigDataBuff[k] < 1024)
                {
                    // 1 Trig data takes 47.48 usec @15MHz.
                    x[j] = (j + 1);
                    y[j] = TrigDataBuff[k];

                    if (max_data < y[j])
                        max_data = y[j];

                    if (min_data > y[j])
                        min_data = y[j];

                    j++;
                    k++;
                }
                else
                    k++;
            }

            /* shows trig data */
            var xDataSource = x.AsXDataSource();
            var yDataSource = y.AsYDataSource();

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                CompositeDataSource compositeDataSource = xDataSource.Join(yDataSource);
                chpNumOfData.Visible = new Rect(0, 0, iCount, 1024);
                chpNumOfData.Children.Remove(TrigPD_Graph);
                chpNumOfData.FitToView();
                TrigPD_Graph = chpNumOfData.AddLineGraph(compositeDataSource, Colors.Blue, 0.5D, "Trig. PD Data");

                // Resize graph
                double dCoorX = AcquisitionCount * -0.025f;
                double dCoorY = 100;

                double pointX = dCoorX;
                double pointY = min_data - dCoorY;
                double width = AcquisitionCount - (dCoorX * 2);
                double height = (max_data - min_data) + (dCoorY * 2);

                chpNumOfData.Visible = new Rect(pointX, pointY, width, height);
                chpNumOfData.LegendVisible = false;
            }));

        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// SearchCount
        ///     Get value of the Histogram
        /// </summary>
        /// <param name="N"></param>
        /// <param name="Histograms"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="max_count"></param>
        /// <param name="max_point"></param>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void SearchCount(int[] Histograms, ref double max, ref double min, ref int max_count, ref int max_point)
        {
            for (int i = 0; i < AcquisitionCount; i++)
            {
                if (TrigDataBuff[i] <= 0) { continue; }

                if (TrigDataBuff[i] > max) { max = TrigDataBuff[i]; }
                if (TrigDataBuff[i] < min) { min = TrigDataBuff[i]; }
            }

            for (int i = 0; i < 1024; i++)
            {
                if (Histograms[i] > max_count)
                {
                    max_count = Histograms[i];
                    max_point = i;
                }
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// UpdateHistogramGraph
        ///     Update the histogram graph.
        /// </summary>
        /// <param name="Histograms"></param>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void UpdateHistogramGraph(int[] Histograms)
        {
            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            double max = 0;                     // max value
            double min = 1023;                  // min value
            int max_count = 0;                  // Offset Peak
            int max_point = 0;                  // Offset
            int iHistMaxCount = 0;              // max value show graph

            // Get value of the Histogram
            SearchCount(Histograms, ref max, ref min, ref max_count, ref max_point);

            // Get max value of the graph
            iHistMaxCount = max_count;

            // If max value more than 440.
            if (max >= 440)
            {
                iHistMaxCount = 0;

                for (int i = 440; i < 1024; i++)
                {
                    if (Histograms[i] > iHistMaxCount) { iHistMaxCount = Histograms[i]; }
                }
            }

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                /* shows histogram */
                EnumerableDataSource<int> xx = new EnumerableDataSource<int>(Enumerable.Range(0, 1024).ToArray());
                xx.SetXMapping(_xx => _xx);
                Func<int, double> linearMapping = i => i;
                Func<int, double> mapping;
                mapping = linearMapping;

                EnumerableDataSource<int> count;
                count = new EnumerableDataSource<int>(Histograms);
                count.SetYMapping(mapping);

                CompositeDataSource cDataSource = new CompositeDataSource(xx, count);
                chpHistogram.Children.Remove(Frequency);
                Frequency = chpHistogram.AddLineGraph(cDataSource, Colors.Red, 2, "Frequency");
                chpHistogram.LegendVisible = false;
                chpHistogram.FitToView();

                // Resize graph
                int iCoorY = (int)(iHistMaxCount * -0.1M);
                if (max < 440) { chpHistogram.Visible = new Rect(min - 5, iCoorY, max - min + 11, iHistMaxCount - (iCoorY * 3)); }
                else { chpHistogram.Visible = new Rect(min - 20, iCoorY, max - min + 50, iHistMaxCount - (iCoorY * 3)); }

                txtLog.AppendText("Maximum:" + max.ToString() + "   Minimum:" + min.ToString() + Environment.NewLine);
                txtLog.AppendText(Environment.NewLine);
                txtLog.ScrollToEnd();

                // Write returned value to log file.
                WriteLog("Maximum: " + max.ToString() + Environment.NewLine +
                         "Minimum: " + min.ToString());
            }));

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            string ResultMessage = "";
            int kaigyou = 0;
            for (int i = (int)min; i < (int)max + 1; i++)
            {
                kaigyou++;
                ResultMessage += string.Format("{0, 4}", i) + " : " + string.Format("{0, 7}", Histograms[i]);
                if (kaigyou % 2 == 0)
                {
                    ResultMessage += Environment.NewLine;
                }
                else
                {
                    ResultMessage += "\t";
                    if (i == (int)max)
                    {
                        ResultMessage += Environment.NewLine;
                    }
                }
            }

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                txtLog.AppendText(ResultMessage);
                txtLog.ScrollToEnd();
            }));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CreateDoseDirectoryName
        ///     It returns the full path to "dose" folder.
        /// </summary>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private string CreateTrigDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\Monitoring PD Data";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }














        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                           CMOS_OPTION.dll function                                                                       =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// OpenDevice
        ///     Opens the sensors
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool OpenDevice()
        {
            // Preparation of variables.
            IntPtr devhandle = new IntPtr(-1);
            DEVICE_CONNECTED = 0;

            // Search for connected device
            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                // Open a device handle
                devhandle = CMOS_OPTION.USB_OpenDevice(0x4400);

                // Write the return value to the log file.
                WriteLog("USB_OpenDevice returned 0x" + devhandle.ToString("X4"));

                // return value is not -1.
                if (devhandle != INVALID_HANDLE_VALUE)
                {
                    // store the device handle in a variable.
                    DeviceHandle_List[DEVICE_CONNECTED] = devhandle;
                    DEVICE_CONNECTED++;
                }
            }

            // There are one or more devices.
            if (DEVICE_CONNECTED != 0)
            {
                return true;
            }

            // There is no device
            MessageBox.Show("No sensor is detected." + Environment.NewLine +
                            "Please make sure to connect a sensor.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            return false;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetPipeHandle
        ///     Get pipe handle
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetPipeHandle()
        {
            // variable for loop
            int iLoop = 0;

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // for return value
                IntPtr result = new IntPtr(-1);

                // Open a pipe handle
                result = CMOS_OPTION.USB_OpenPipe(DeviceHandle_List[iLoop]);

                // Write result to log file.
                WriteLog("USB_OpenPipe returned 0x" + result.ToString("X4"));

                // Failed to get a pipe handle.
                if (result == INVALID_HANDLE_VALUE)
                {
                    // show error message
                    MessageBox.Show("Initialization failed." + Environment.NewLine +
                                    "Please reconnect all the sensors and click the power button.",
                                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    // If USB_OpenPipe failed, it may be necessary to reconnect the sensor to another USB port.
                    cboxSensor.Items.Add("**reconnect the sensor**");
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetSensorSerial
        ///     Get sensor serial
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetSensorSerial()
        {
            // Declaring variables
            CMOS_OPTION.UnitSensorInformation SensorInformation;           // for HPK_GetSensorInformation
            int iLoop = 0;                                              // for loop

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // Get serial number of CMOS device
                uint result = GetSensorInfo(DeviceHandle_List[iLoop], out SensorInformation);

                // The sensor information was acquired successfully.
                if (result == 0x00)
                {
                    string SerialNumber = SensorInformation.Sensor_Type.ToString("X2") + SensorInformation.Lot_Serial_No.ToString("X4");

                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        // Add sensor serial to combobox
                        cboxSensor.Items.Add(SerialNumber);

                        // Write returned value to log.
                        WriteLog("ST: " + SensorInformation.Sensor_Type.ToString("X2") + "   SS: " + SensorInformation.Lot_Serial_No.ToString("X4"));
                    }));
                }
                else
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        // show error message
                        MessageBox.Show("Initialization failed." + Environment.NewLine +
                                        "Please reconnect all the sensors and click the power button.",
                                        "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                        // If HPK_GetSensorInformation failed, it may be necessary to reconnect the sensor to another USB port.
                        cboxSensor.SelectedIndex = -1;
                    }));

                    // Leave the loop.
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetSensorInfo
        ///     Function to carry out HPK_GetSensorInformation.
        /// </summary>
        /// <param name="devHandle"></param>
        /// <param name="SensorInformation"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private uint GetSensorInfo(IntPtr devHandle, out CMOS_OPTION.UnitSensorInformation SensorInformation)
        {
            uint result = 1;
            CMOS_OPTION.UnitIntegrationParameter IntegParam;

            // Initialization for UnitSensorInformation.
            SensorInformation.Lot_Serial_No = 0x0000;
            SensorInformation.Sensor_Type = 0x00;
            SensorInformation.Firmware_Version = 0x00;

            // Initialization for UnitIntegrationParameter.
            IntegParam.Xray_Incident_Threshold = 440;
            IntegParam.Xray_Integration_End_Threshold = 437;
            IntegParam.Integration_Time = 1;

            // Get the Sensor information
            result = CMOS_OPTION.HPK_GetSensorInformation(devHandle, ref IntegParam, out SensorInformation);

            // Write returned value to log.
            WriteLog("HPK_GetSensorInformation returned 0x" + result.ToString("X4"));

            if (result == 0)
            {
                // Read the serial number
                byte ST = 0;
                ushort SS = 0;
                if (ReadSerialNumber(devHandle, ref ST, ref SS))
                {
                    SensorInformation.Lot_Serial_No = SS;
                    SensorInformation.Sensor_Type = ST;
                }
            }

            return result;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// ReadSerialNumber
        ///     Read the serial number from a specific area.
        /// </summary>
        /// <param name="devHandle"></param>
        /// <param name="ST"></param>
        /// <param name="SS"></param>
        /// <returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool ReadSerialNumber(IntPtr devHandle, ref byte ST, ref ushort SS, bool blnSelect = false)
        {
            uint result = 1;
            byte[] Buffer = new byte[32];
            bool blnRead = false;
            byte[] code = Encoding.ASCII.GetBytes("SNU");


            // Read ST and SS from dedicated area.
            result = CMOS_OPTION.HPK_ReadCustomerCode(devHandle, Buffer, 2147483648, code);
            if (result == 0)
            {
                // Initialize with 0xFF
                byte[] dBuffer = Enumerable.Repeat<byte>(0xFF, 32).ToArray();

                // Check that the obtained value is not the initial value.
                if (!Buffer.SequenceEqual(dBuffer))
                {
                    SS = (ushort)((Buffer[1] << 8) + Buffer[2]);
                    ST = Buffer[0];
                    blnRead = true;
                }
            }

            // Write returned value to log.
            if (!blnSelect) WriteLog("HPK_ReadCustomerCode returned 0x" + result.ToString("X4"));

            return blnRead;
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// WriteLogSelectDevice
        ///     Write the selected device in the log.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void WriteLogSelectDevice()
        {
            uint success = 0;
            IntPtr DeviceHandle = new IntPtr(-1);
            CMOS_OPTION.UnitIntegrationParameter IntegParam;
            CMOS_OPTION.UnitSensorInformation SensorInfo = new CMOS_OPTION.UnitSensorInformation();

            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];
            IntegParam.Xray_Incident_Threshold = 440;
            IntegParam.Xray_Integration_End_Threshold = 437;
            IntegParam.Integration_Time = 1;

            success = CMOS_OPTION.HPK_GetSensorInformation(DeviceHandle, ref IntegParam, out SensorInfo);
            if (success == 0)
            {
                // Read the serial number
                byte ST = 0;
                ushort SS = 0;
                if (ReadSerialNumber(DeviceHandle, ref ST, ref SS, true))
                {
                    SensorInfo.Lot_Serial_No = SS;
                    SensorInfo.Sensor_Type = ST;
                }
            }
            else { return; }

            string strSelect = SensorInfo.Sensor_Type.ToString("X2") + SensorInfo.Lot_Serial_No.ToString("X4");
            WriteLog("Select Device SN: " + strSelect);
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// CloseAllDevice
        ///     Releases all device handle.
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void CloseAllDevice()
        {
            // Write to log file.
            WriteLog("**********************************************************************************");

            for (int i = 0; i < MAXIMUM_DEVICE_COUNT; i++)
            {
                // Releases a device handle.
                CMOS_OPTION.USB_CloseDevice(DeviceHandle_List[i]);

                // Write returned value to log file.
                WriteLog("HPK_Closedevice was called.");
            }

            // 0 clear
            DEVICE_CONNECTED = 0;

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Change of status displaying
                lblStatus.Content = "Click left icon";
                lblStatus.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x91, 0x91, 0x91));
            }));
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GoToSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void GoToSuspend()
        {
            if (cboxSensor.Items.Count > 0)
            {
                // Suspend delay 1 [sec]
                uint time = 1000;

                // Get the Device handle
                DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

                // USB_SuspendDevice
                uint result = CMOS_OPTION.USB_SuspendDevice(DeviceHandle, time);

                // Write returned value to log file.
                WriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4"));
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// AllDeviceSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool AllDeviceSuspend()
        {
            uint time = 1000;       // Suspend delay 1 [sec]
            int iLoop = 0;          // variable for loop

            // by the number of devices.
            for (iLoop = 0; iLoop < DEVICE_CONNECTED; iLoop++)
            {
                // USB_SuspendDevice
                uint result = CMOS_OPTION.USB_SuspendDevice(DeviceHandle_List[iLoop], time);

                // Write returned value to log file.
                WriteLog("USB_SuspendDevice returned 0x" + result.ToString("X4"));

                // Failed to get a pipe handle.
                if (result != 0x00)
                {
                    // show error message
                    MessageBox.Show("USB_SuspendDevice failed." + Environment.NewLine +
                                    "Please restart operation with the following steps." + Environment.NewLine +
                                    "1st; Click OK of this popup." + Environment.NewLine +
                                    "2nd; Disconnect the sensor." + Environment.NewLine +
                                    "3rd; Close S11684S11685.exe." + Environment.NewLine +
                                    "4th; Connect the sensor to another USB port." + Environment.NewLine +
                                    "5th; Execute S11684S11685.exe.",
                                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    // If USB_SuspendDevice failed, it may be necessary to reconnect the sensor to another USB port.
                    cboxSensor.Items.Add("**reconnect the sensor**");
                    break;
                }
            }

            // return value
            if (iLoop >= DEVICE_CONNECTED) { return true; }
            else { return false; }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// StopSuspend
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void StopSuspend()
        {
            // Get the Device handle
            DeviceHandle = DeviceHandle_List[cboxSensor.SelectedIndex];

            // USB_ResumeDevice
            uint result = CMOS_OPTION.USB_ResumeDevice(DeviceHandle);

            // Write returned value to log file.
            WriteLog("USB_ResumeDevice returned 0x" + result.ToString("X4"));
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// GetTrigData
        ///     Getting trigger pd data
        /// </summary>
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private bool GetTrigData(uint Interval)
        {
            uint result = 0;
            result = CMOS_OPTION.HPK_TrigPdTesting(DeviceHandle, AcquisitionCount, Interval, TrigDataBuff, ref DataCount, ref mbyStopFlag);

            // Write returned value to log file.
            WriteLog("HPK_TrigPdTesting returned 0x" + result.ToString("X4"));

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                txtLog.AppendText("***HPK_TrigPdTesting***  returned 0x" + result.ToString("X4") + Environment.NewLine);
                txtLog.AppendText("Finished at " + DateTime.Now.ToString("HH:mm:ss.fff") + "." + Environment.NewLine);
                txtLog.ScrollToEnd();
            }));

            // HPK_TrigPdTesting failed.
            if ((result != 0) && (result != 0x3E3))
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    txtLog.AppendText("Acquiring data failed." + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));

                return false;
            }

            // In the case of stopped.
            else if ((result == 0x3E3) && (mbyStopFlag == 1))
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    txtLog.AppendText("Acquisition of \"Monitoring PD Data\" has been stopped." + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));
                return false;
            }

            // HPK_TrigPdTesting success.
            else
            {
                return true;
            }
        }

















        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Thread function                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_SearchSesor_DoWork(object sender, DoWorkEventArgs e)
        {
            // Initialize flag
            mblnGetSs = false;

            // Write to log file.
            WriteLog("**********************************************************************************");

            // Opens the sensors
            if (!OpenDevice()) { CloseAllDevice(); }

            // Get pipe handle 
            else if (!GetPipeHandle()) { CloseAllDevice(); }

            // Get sensor serial
            else if (!GetSensorSerial()) { CloseAllDevice(); }

            // Enables the suspended state.
            else if (!AllDeviceSuspend()) { CloseAllDevice(); }

            // If there is no problem set the flag.
            else { mblnGetSs = true; }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_SearchSesor_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // When the flag is set.
            if (mblnGetSs)
            {
                // Enable other than stop button.
                ChangeEnabled(true, true, blnOther: true);

                // Select the first sensor.
                cboxSensor.SelectedIndex = 0;

                // Change of status displaying
                lblStatus.Content = "Ready";
                lblStatus.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xFF, 0x00));
            }

            // When the flag is not set.
            else
            {
                // Enable only the power button.
                ChangeEnabled(true);
            }

            // Set the image source.
            ChangePowerButton();
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_DoWork
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_TrigPd_DoWork(object sender, DoWorkEventArgs e)
        {
            uint Interval = 1;
            DateTime dtNow;
            bool blnSuccess = false;

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                // Stop Suspend mode.
                StopSuspend();

                // Get the parameter
                AcquisitionCount = uint.Parse(txtAcqCount.Text);
                Interval = uint.Parse(txtInterval.Text);

                WriteLog("=======Parameters=======" + Environment.NewLine +
                         General.PcPower() + Environment.NewLine +
                         "Number of data: " + AcquisitionCount + Environment.NewLine +
                         "Interval  (ms): " + Interval + Environment.NewLine +
                         "========================");

                // Time update
                dtNow = DateTime.Now;
                txtLog.AppendText("Start at " + dtNow.ToString("HH:mm:ss.fff") + "." + Environment.NewLine);
                txtLog.ScrollToEnd();
            }));

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // Initialize
            TrigDataBuff = new ushort[AcquisitionCount];
            DataCount = 0;
            mbyStopFlag = 0;

            // Start the thread to display the number of got trig pd data.
            if (Worker_DisplayCount.IsBusy == false)
            {
                Worker_DisplayCount.RunWorkerAsync();
            }

            // set the flag
            mblnDisplayCount = true;

            // Getting monitoring pd data
            blnSuccess = GetTrigData(Interval);

            // Waiting for the thread termination.
            while (Worker_DisplayCount.IsBusy) System.Threading.Thread.Sleep(10);
            while (mblnDisplayCount) System.Threading.Thread.Sleep(10);

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // successed.
            if (blnSuccess)
            {
                int[] Histograms = new int[1024];
                int AdcOffset = 0;
                decimal sigma = 0.0M;

                // Update the trig pd data graph
                UpdateTrigDataGraph();

                // Generate histograms.
                GenerateHistograms(TrigDataBuff, Histograms);

                // Get "ADC offset".
                GetOffsetMode(Histograms, ref AdcOffset);

                // Calculate sigma.
                GetSigma(TrigDataBuff, ref sigma);

                // Write returned value to log file.
                WriteLog("ADC offset: " + AdcOffset.ToString() + Environment.NewLine +
                         "Sigma: " + sigma.ToString("F2"));

                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    txtLog.AppendText("Acquiring data succeeded. " + Environment.NewLine);
                    txtLog.AppendText("ADC offset : " + AdcOffset.ToString() + "   Sigma : " + sigma.ToString("F2") + Environment.NewLine);
                    txtLog.ScrollToEnd();
                }));

                // Update the histogram graph
                UpdateHistogramGraph(Histograms);

                //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // Create csv file of trigger pd data
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    // Get the sensor serial
                    string SensorSerial = cboxSensor.Items[cboxSensor.SelectedIndex].ToString();

                    // Folder path of csv file.
                    string folderPath = CreateTrigDirectoryName();

                    // Full path of csv file.
                    string filePath = folderPath + @"\SN" + SensorSerial + "_Monitor_PD_Data.csv";

                    using (System.IO.StreamWriter sw2 = new StreamWriter(filePath, false, Encoding.GetEncoding("ASCII")))
                    {
                        sw2.WriteLine(DateTime.Now.ToString());

                        // Export time-series data
                        if (chkExportData.IsChecked == true)
                        {
                            sw2.WriteLine("Data number,Acquired Data,Monitor PD value,Frequency");

                            for (int i = 0; i < AcquisitionCount; i++)
                            {
                                if (TrigDataBuff[i] <= 0 && i >= 1024) { continue; }

                                if (i < 1024)
                                {
                                    sw2.Write((i + 1).ToString() + "," + TrigDataBuff[i].ToString() + "," + i.ToString() + "," + Histograms[i].ToString());
                                }
                                else
                                {
                                    sw2.Write((i + 1).ToString() + "," + TrigDataBuff[i].ToString());
                                }

                                sw2.WriteLine();
                            }
                        }

                        // not export time-series data
                        else
                        {
                            sw2.WriteLine("Monitor PD value,Frequency");


                            for (int i = 0; i < 1024; i++)
                            {
                                sw2.Write(i.ToString() + "," + Histograms[i].ToString());

                                // Add a new line.
                                sw2.WriteLine();
                            }
                        }
                    }

                }));
            }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_SearchSesor_RunWorkerCompleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_TrigPd_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Enables the suspended state.
            GoToSuspend();

            // Enable other than stop button.
            if (mblnGetSs)
                ChangeEnabled(true, true, blnOther: true);

            btnStart.Content = "Start";

            // Add a new line.
            txtLog.AppendText(Environment.NewLine);
        }

        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_DisplayAcqCount_DoWork
        /// 
        /// 　　Create  :  20/01/30
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_DisplayCount_DoWork(object sender, DoWorkEventArgs e)
        {
            do
            {
                System.Threading.Thread.Sleep(10);
                ShowCountNumber(DataCount + " / " + AcquisitionCount);

            }
            while ((AcquisitionCount > DataCount) && (mbyStopFlag == 0));
        }


        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Worker_DisplayAcqCount_RunWorkerCompleted
        /// 
        /// 　　Create  :  20/01/30
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        void Worker_DisplayCount_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mblnDisplayCount = false;
        }

    }
}
