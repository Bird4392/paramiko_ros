﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CMOS_USB_DemoApplication
{
    class CMOS_USB
    {
        //Error handle
        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        [StructLayout(LayoutKind.Sequential)]
        public struct UnitIntegrationParameter
        {
            public ushort Xray_Incident_Threshold;
            public ushort Xray_Integration_End_Threshold;
            public double Integration_Time;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitXrayImage
        {
            public byte Mode;
            public UnitIntegrationParameter IntegParam;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitSensorInformation
        {
            public UnitIntegrationParameter IntegParam;
            public ushort Lot_Serial_No;
            public byte Sensor_Type;
            public byte Firmware_Version;
        }

        //Functions
        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenDevice(ushort ProductID);

        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenTargetDevice(ushort ProductID, ushort SerialNo);

        [DllImport("CMOS_USB.dll")]
        public extern static void USB_CloseDevice(System.IntPtr DeviceHandle);

        [DllImport("CMOS_USB.dll")]
        public extern static System.IntPtr USB_OpenPipe(System.IntPtr DeviceHandle);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetXrayImage(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetTrigPdData(IntPtr DeviceHandle, double AcquisitionTime, ushort[] TrigPdData, out ulong bufferLength, ref UnitIntegrationParameter IntegParam);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetSensorInformation(IntPtr DeviceHandle, ref UnitIntegrationParameter IntegParam, out UnitSensorInformation SensorInfo);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_StopTrigPdData(IntPtr DeviceHandle, byte[] Buffer, out ulong bufferLength, ref UnitIntegrationParameter IntegParam);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ForceTrigAndGetDummy(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_AbortBulkPipe(IntPtr DeviceHandle);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_GetXrayCorrectionImage(IntPtr DeviceHandle, byte[] ImageData, out ulong bufferLength, ref UnitXrayImage XrayImage);

        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_SuspendDevice(IntPtr DeviceHandle, uint SuspendDelay);

        [DllImport("CMOS_USB.dll")]
        public extern static uint USB_ResumeDevice(IntPtr DeviceHandle);

        [DllImport("CMOS_USB.dll")]
        public extern static uint HPK_ReadCustomerCode(IntPtr DeviceHandle, byte[] Buffer, uint Index, byte[] Code);
    }
}
