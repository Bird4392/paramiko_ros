﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CMOS_USB_DemoApplication
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //*************************************************************************************************************************************************************************
        //*                                                                                                                                                                       *
        //*                                                                              Main Window                                                                              *
        //*                                                                                                                                                                       *
        //*************************************************************************************************************************************************************************





        //=========================================================================================================================================================================
        //=                                                                                                                                                                       =
        //=                                                                              Event procesure                                                                          =
        //=                                                                                                                                                                       =
        //=========================================================================================================================================================================

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        public MainWindow()
        {
            InitializeComponent();
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void btnImgAcquisition_Click(object sender, RoutedEventArgs e)
        {
            bool bln = false;
            ImageAcquisition window = new ImageAcquisition(ref bln);

            if (!bln)
                window.Close();

            else
            {
                window.Show();
                this.Close();
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void btnTrigPdTesting_Click(object sender, RoutedEventArgs e)
        {
            TrigPdTesting window = new TrigPdTesting();

            window.Show();
            this.Close();
        }
    }
}
