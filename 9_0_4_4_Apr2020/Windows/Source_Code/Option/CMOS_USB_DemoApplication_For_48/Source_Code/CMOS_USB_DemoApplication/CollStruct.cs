﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMOS_USB_DemoApplication
{
    public class CollStruct
    {
        public struct UintImageAcq
        {
            public byte Mode;                       // Integration mode
            public ushort StartThreshold;           // Threshold to start integration
            public ushort EndThreshold;             // Threshold to end integration
            public double IntegTime;                // Integration time [ms]
            public byte ImgCorrType;                // Image correction type
            public int AcqTimes;                    // Correction images to acquire
            public bool CreateCalFile;              // Create a correction file
            public string PathOfCalFile;            // Full path of cal file.
            public bool SubStoredImage;             // Dark subtraction with stored dark image
            public string PathOfDarkFile;           // Full path of dark file.

            public UintImageAcq(byte p1, ushort p2, ushort p3, double p4, byte p5, int p6, bool p7, string p8, bool p9, string p10)
            {
                Mode = p1;
                StartThreshold = p2;
                EndThreshold = p3;
                IntegTime = p4;
                ImgCorrType = p5;
                AcqTimes = p6;
                CreateCalFile = p7;
                PathOfCalFile = p8;
                SubStoredImage = p9;
                PathOfDarkFile = p10;
            }
        }

        public enum ImageCorrection
        {
            Raw,                                    // Raw image
            Gamma1,                                 // Gamma1 correction image
            Gamma2,                                 // Gamma2 correction image
            Gamma3                                  // Gamma3 correction image
        }
    }
}
