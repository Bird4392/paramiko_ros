﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace CMOS_USB_DemoApplication
{
    public class General
    {
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get version of application and retrun it as string
        public static string GetFileVersion()
        {
            // Get application version information
            System.Diagnostics.FileVersionInfo ver = System.Diagnostics.FileVersionInfo.GetVersionInfo(
                System.Reflection.Assembly.GetExecutingAssembly().Location);

            // Return file version
            return ver.FileVersion;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Screen Shot
        public static BitmapSource CopyScreen()
        {
            System.Windows.Forms.SendKeys.SendWait("%{PRTSC}");

            System.Threading.Thread.Sleep(100);

            IDataObject dobj = Clipboard.GetDataObject();
            if(dobj.GetDataPresent(DataFormats.Bitmap) == true)
            {
                System.Windows.Interop.InteropBitmap ibmp = (System.Windows.Interop.InteropBitmap)dobj.GetData(DataFormats.Bitmap);
                return ibmp;
            }

            return null;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "Device" folder.
        public static string CreateDeviceDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\Device\";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "log" folder.
        public static string CreateLogDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\log";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // It returns the full path to "Dark" folder.
        public static string CreateDarkDirectoryName()
        {
            string strDirectory = @"C:\HAMAMATSU\S11684S11685\Dark\";

            if (!System.IO.Directory.Exists(strDirectory))
            {
                System.IO.Directory.CreateDirectory(strDirectory);
            }

            return strDirectory;
        }
        
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get the power information
        public static string PcPower()
        {
            string result = "";

            System.Windows.Forms.PowerLineStatus pls = System.Windows.Forms.SystemInformation.PowerStatus.PowerLineStatus;
            switch (pls)
            {
                case System.Windows.Forms.PowerLineStatus.Offline:
                    result = "Battery power";

                    System.Windows.Forms.BatteryChargeStatus bcs = System.Windows.Forms.SystemInformation.PowerStatus.BatteryChargeStatus;
                    if (bcs == System.Windows.Forms.BatteryChargeStatus.Unknown)
                        result += " (remaining unknown)";
                    else
                    {
                        float blp = System.Windows.Forms.SystemInformation.PowerStatus.BatteryLifePercent;
                        result += " (remaining " + (blp * 100) + "%)";
                    }
                    break;

                case System.Windows.Forms.PowerLineStatus.Online:
                    result = "AC power";
                    break;
            }

            return result;
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get the driver information
        public static void DriverInfo(out string strInfo)
        {
            string info = "";

            strInfo = "=========================================================================================" + Environment.NewLine;

            //================================================================================================================================================
            // ドライババージョン
            System.Management.ManagementObjectSearcher objSearcher = new System.Management.ManagementObjectSearcher("Select * from Win32_PnPSignedDriver");
            System.Management.ManagementObjectCollection objCollection = objSearcher.Get();

            foreach (System.Management.ManagementObject obj in objCollection)
            {
                string str = (string)obj["DeviceName"];
                if (str == "Intra-oral X-ray image sensor")
                {
                    info = "Driver Name\t\t = " + obj["DeviceName"] + Environment.NewLine +
                           "Driver Manufacturer\t\t = " + obj["Manufacturer"] + Environment.NewLine +
                           "Driver Version\t\t = " + obj["DriverVersion"] + Environment.NewLine;
                    break;
                }
            }

            objCollection.Dispose();
            objSearcher.Dispose();

            if (info == "")
                strInfo = "";
            else
                strInfo += info + "=========================================================================================";
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get the software information
        public static void SoftwareInfo(out string strInfo)
        {
            strInfo = "=========================================================================================" + Environment.NewLine;

            //================================================================================================================================================
            // アプリケーションバージョン
            strInfo += "Apllication Version\t\t = " + GetFileVersion() + Environment.NewLine;

            //================================================================================================================================================
            // DLLバージョン
            System.Diagnostics.FileVersionInfo vi = System.Diagnostics.FileVersionInfo.GetVersionInfo("CMOS_USB.dll");
            strInfo += "CMOS_USB.dll Version\t = " + vi.FileVersion + Environment.NewLine;

            strInfo += "=========================================================================================";
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Get the system information
        public static void PcSystemInfo(out string strInfo)
        {
            strInfo = "System Information\r\n" +
                      "=========================================================================================\r\n";

            System.Management.ManagementClass mcOperation = null;
            System.Management.ManagementClass mcProcessor = null;
            System.Management.ManagementClass mcComputer = null;
            System.Management.ManagementClass mVideo = null;
            System.Management.ManagementClass mUsb = null;

            try
            {
                //--------------------------------------------------------------------------------------------------------------------------------
                System.OperatingSystem os = System.Environment.OSVersion;

                mcOperation = new System.Management.ManagementClass("Win32_OperatingSystem");
                System.Management.ManagementObjectCollection moc = mcOperation.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "OS\t\t\t\t = " + mo["Caption"] + "\r\n";                            // OS
                    strInfo += "Version\t\t\t = " + mo["Version"] + mo["CSDVersion"] + "\r\n";      // Version
                    strInfo += "Memory Size\t\t = " + mo["TotalVisibleMemorySize"] + "\r\n";        // RAM
                    strInfo += "System Name\t\t = " + mo["CSName"] + "\r\n";                        // System Name
                    strInfo += "OS Manufacturer\t\t = " + mo["Manufacturer"] + "\r\n";              // OS Manufacturer

                    if (os.Version.Major >= 6)
                    {
                        strInfo += "OS Architecture\t\t = " + mo["OSArchitecture"] + "\r\n";
                    }
                }
                moc.Dispose();
                mcOperation.Dispose();

                //--------------------------------------------------------------------------------------------------------------------------------
                mcProcessor = new System.Management.ManagementClass("Win32_Processor");
                moc = mcProcessor.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "Processor\t\t\t = " + mo["Name"] + "\r\n";                            // Processor
                }
                moc.Dispose();
                mcProcessor.Dispose();

                //--------------------------------------------------------------------------------------------------------------------------------
                mcComputer = new System.Management.ManagementClass("Win32_ComputerSystem");
                moc = mcComputer.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "System Manufacturer\t = " + mo["Manufacturer"] + "\r\n";            // System Manufacturer
                    strInfo += "System Model\t\t = " + mo["Model"] + "\r\n";                        // System Model
                }
                moc.Dispose();
                mcComputer.Dispose();
                
                //--------------------------------------------------------------------------------------------------------------------------------
                mVideo = new System.Management.ManagementClass("Win32_VideoController");
                moc = mVideo.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "Graphics\t\t\t = " + mo["Name"] + "\r\n";                        // Graphics
                }
                moc.Dispose();
                mVideo.Dispose();
                
                //--------------------------------------------------------------------------------------------------------------------------------
                mUsb = new System.Management.ManagementClass("Win32_USBController");
                moc = mUsb.GetInstances();
                int count = 1;
                foreach (System.Management.ManagementObject mo in moc)
                {
                    strInfo += "USB Controller" + count + "\t\t = " + mo["Name"] + "\r\n";          // USB Controller
                    count++;
                }
                moc.Dispose();
                mUsb.Dispose();
            }
            finally
            {
                if (mUsb != null) { mUsb.Dispose(); }
                if (mVideo != null) { mVideo.Dispose(); }
                if (mcComputer != null) { mcComputer.Dispose(); }
                if (mcProcessor != null) { mcProcessor.Dispose(); }
                if (mcOperation != null) { mcOperation.Dispose(); }

                strInfo += "=========================================================================================";
            }
        }
        
    }
}
