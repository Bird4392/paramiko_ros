﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CMOS_USB_DemoApplication
{
    class CMOS_OPTION
    {
        //Error handle
        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        [StructLayout(LayoutKind.Sequential)]
        public struct UnitIntegrationParameter
        {
            public ushort Xray_Incident_Threshold;
            public ushort Xray_Integration_End_Threshold;
            public double Integration_Time;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitXrayImage
        {
            public byte Mode;
            public UnitIntegrationParameter IntegParam;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct UnitSensorInformation
        {
            public UnitIntegrationParameter IntegParam;
            public ushort Lot_Serial_No;
            public byte Sensor_Type;
            public byte Firmware_Version;
        }

        //Functions
        [DllImport("CMOS_OPTION.dll")]
        public extern static System.IntPtr USB_OpenDevice(ushort ProductID);

        [DllImport("CMOS_OPTION.dll")]
        public extern static void USB_CloseDevice(System.IntPtr DeviceHandle);

        [DllImport("CMOS_OPTION.dll")]
        public extern static System.IntPtr USB_OpenPipe(System.IntPtr DeviceHandle);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint HPK_GetSensorInformation(IntPtr DeviceHandle, ref UnitIntegrationParameter IntegParam, out UnitSensorInformation SensorInfo);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint HPK_AbortBulkPipe(IntPtr DeviceHandle);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint HPK_ReadCustomerCode(IntPtr DeviceHandle, byte[] Buffer, uint Index, byte[] Code);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint HPK_ReadTypeNumber(IntPtr DeviceHandle, byte[] Buffer);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint HPK_TrigPdTesting(IntPtr DeviceHandle, uint AcquisitionCount, uint Interval, ushort[] Buffer, ref uint DataCount, ref byte StopFlag);
        
        [DllImport("CMOS_OPTION.dll")]
        public extern static uint USB_SuspendDevice(IntPtr DeviceHandle, uint SuspendDelay);

        [DllImport("CMOS_OPTION.dll")]
        public extern static uint USB_ResumeDevice(IntPtr DeviceHandle);

    }
}
