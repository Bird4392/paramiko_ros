/* ============================================================================
 
 Copyright (c) Hamamatsu Photonics K.K. All Rights Reserved.
 
 Project Name : Specu1a
 ----------------------------------------------------------------------
 Module Name  : specu1a.h
 Description  :
 
 ============================================================================ */

#ifndef _SPEC_U1A_H
#define _SPEC_U1A_H

#ifdef  __cplusplus
extern  "C" {
#endif
#include <CoreFoundation/CFPlugIn.h>
#include <IOKit/usb/IOUSBLib.h>
/*----------------------------------------------------------------------------*/
typedef unsigned char   UCHAR;
typedef UInt32          ULONG;
typedef unsigned int    UINT;
typedef unsigned short  USHORT;
typedef int             HANDLE;

#define INVALID_HANDLE_VALUE ((HANDLE)-1)
/*----------------------------------------------------------------------------*/
    typedef struct _tag_UnitIntegrationParameter {
        USHORT Xray_Incident_Threshold;
        USHORT Integration_End_Threshold;
        double Integration_Time;/*msecíPà */
    } UNIT_INTEGRATION_PARAMETER, *PUNIT_INTEGRATION_PARAMETER;

    typedef struct _tag_UnitXrayImage {
        UCHAR Mode;
        UNIT_INTEGRATION_PARAMETER IntegParam;
    } UNIT_XRAY_IMAGE, *PUNIT_XRAY_IMAGE;


    typedef struct _tag_UnitSensorInformation {
        UNIT_INTEGRATION_PARAMETER IntegParam;
        USHORT Lot_Serial_No;
        UCHAR Sensor_Type;
        UCHAR Firmware_Version;
    } UNIT_SENSOR_INFORMATION, *PUNIT_SENSOR_INFORMATION;
    
    /*----------------------------------------------------------------------
     Open/Close
     ----------------------------------------------------------------------*/
    void USB_SetVid(USHORT VID);
    
    HANDLE USB_OpenDevice(
                          USHORT ProductID
                          );
    
    void USB_CloseDevice(
                         HANDLE DeviceHandle
                         );
    
    HANDLE USB_OpenPipe(
                        HANDLE DeviceHandle
                        );
    
    HANDLE USB_OpenTargetDevice(
                                USHORT ProductID,
                                USHORT SerialNo
                                );
    unsigned long USB_SuspendDevice(
                                    HANDLE DeviceHandle
                                    );
    unsigned long USB_ResumeDevice(
                                   HANDLE DeviceHandle
                                   );
    /*----------------------------------------------------------------------
     Read
     ----------------------------------------------------------------------*/
    
    IOReturn HPK_GetXrayCorrectionImage(
                                        HANDLE DeviceHandle,
                                        UCHAR *Buffer,
                                        ULONG *BufferLength,
                                        PUNIT_XRAY_IMAGE pParam
                                        );
    
    IOReturn HPK_GetXrayImage(
                              HANDLE DeviceHandle,
                              UCHAR *Buffer,
                              ULONG *BufferLength,
                              PUNIT_XRAY_IMAGE pParam
                              );
    
    IOReturn HPK_GetTrigPdData(
                               HANDLE DeviceHandle,
                               double AcquisitionTime,
                               USHORT Buffer[],
                               ULONG *BufferLength,
                               PUNIT_INTEGRATION_PARAMETER pIntegParam
                               );
  
    IOReturn HPK_StopTrigPdData(
                                HANDLE DeviceHandle,
                                UCHAR *Buffer,
                                ULONG *BufferLength,
                                PUNIT_INTEGRATION_PARAMETER pIntegParam
                                );
    
    IOReturn HPK_GetSensorInformation(
                                      HANDLE DeviceHandle,
                                      PUNIT_INTEGRATION_PARAMETER pIntegParam,
                                      PUNIT_SENSOR_INFORMATION pSensorInfo
                                      );

    IOReturn HPK_AbortBulkPipe(
                               HANDLE DeviceHandle
                               );
  
    IOReturn HPK_ForceTrigAndGetDummy(
                                      HANDLE DeviceHandle,
                                      UCHAR *Buffer,
                                      ULONG *BufferLength,
                                      PUNIT_XRAY_IMAGE Param
                                      );
    /*----------------------------------------------------------------------
     Other
     ----------------------------------------------------------------------*/
  
    IOReturn USB_FlushPipe(
                           HANDLE DeviceHandle
                           );


#ifdef  __cplusplus
}
#endif

extern void dll_attach();

#endif