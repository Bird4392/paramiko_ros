paramiko_ros v0.1.1

Dr. Banjmain Bird
benjamin.bird@createc.co.uk

This package allows the user to control software packages on none ROS enabled systems. This is particularly handy in instances where certain sensors only have drivers released for Microsoft Windows or Mac OSX, or some flavour of Linux that is not condusive for running ROS. Whilst it is possible to install ROS on these systems, it requires considerable effort to do so. It's also really handy if you have some particularly powerful software that would take ages to replicate the functionality of. The paramiko ros package canrun it, and even pass command line arguments to it, and then grab any resulting data.

paramiko_ros in it's initial form is intended for use with the Hamamatsu X-ray sensor, which runs on Windows. The package executes a script on the target machine using SSH via Paramiko, and then grabs the resultant images via SFTP, again through paramiko, before publishing the subequent images over the Host machines ROS network. 

Works on python 2.7* and python 3* current setup uses python 3, purely because the machine i used for the demo is running ROS Neotic and Ubuntu 20.04, with no python 2.7 support. If you're running ubuntu 18.04 you can use python 2.7 just by changing the first line of the "paramiko_xray.py" file from #!/usr/bin/env python3 to #!/usr/bin/env python

Dependancies:

paramiko ------- sudo apt-get install -y python-paramiko
numpy ---------- sudo apt-get install -y python-numpy
pil------------- pip3 install pillow / pip install pillow

To use this package, configure the launch file as follows:

hostname - the ip address of the target system
username - the user name of the user on the target system
password - the password of the user on the target system
port - the port on which to communicate, just leave as '22' unless you have any specific requiremnts to change it
remotepath - the path of the saved data on the remote system, windows example - C:\Users\user\xray_data.csv linux example /home/ben/xray_data.csv Note that path must be complete with the file name and type!
localpath - the path to store the resulting file on the local system, linux example - /home/ben/catkin_ws/xray_data.csv Note that the path must be complete with the file name and type!
command - the command to run your software on the target machine, it's easier to have your software living in the home directory, so you don't have to mess about specifying paths. Windows example - faux_xray_detector.exe

Once you're all set up, you can use a rosservice call to trigger the package to do its thing.

rosservice call /xray_image "{}"

This will run whatever you've configured in the launch file. You might want to change the code to get a more representative name for the service call. This version was built to interface with the xray detector though, hence the naming conventions. 

Any issues, give me a ring on 07935917928



