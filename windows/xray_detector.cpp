
//x86_64-w64-mingw32-g++ xray_detector.cpp -L. -lCMOS_USB -static-libstdc++ -static-libgcc -o xray_detector.exe
//wine64 ./xray_detector.exe

//Return Values (4 bytes)
//0x00000000 The sensor information was acquired successfully.
//0x00000003 The size of the sensor information is incorrect.
//0x00000005 Invalid device handle.
//0x00000009 Insufficient memory for this function.
//0x00000011 Semaphore timeout.
//0x00000021 Invalid parameter for this function.
//Other values An error code from GetLastError (WinAPI).



/**/

using namespace std;

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <windows.h>
#include "CMOS_USB.h"

template<class T>
std::string tostringX(unsigned len, T v) {
    std::ostringstream os;
    os << std::hex << v;
    auto rv = os.str();
    if(rv.size() < len) rv = std::string(len - rv.size(), '0') + rv;
    return rv;
}

//void SaveArray(unsigned char *buffer, unsigned long *si){
//   static int image[1000][1506];// must be declared static so that is allocated on the heap - https://stackoverflow.com/questions/1847789/segmentation-fault-on-large-array-sizes
//   int index = 14;
//   int pixel;
//  //cout << index << endl;
//   ofstream file ("xray_data.csv");
//   for (int i = 0; i < 1506; i++){
//      for(int j = 0; j < 1000; j++){
//         pixel = buffer[index] | (int(buffer[index+1]) << 8);
//         image[i][j] = pixel;
//         file << image[i][j] << ',';
//         index = index + 2;
//         //cout << pixel << endl;
//      }
//      file << endl;
//   }
//   file.close();
//}

void SaveArray(unsigned char *buffer, unsigned long *si){ // from faux xray
   static int image[1000][1506];//[1000][1506];// must be declared static so that is allocated on the heap - https://stackoverflow.com/questions/1847789/segmentation-fault-on-large-array-sizes
   int index = 0;
   int pixel, i, j;
   ofstream file ("xray_data.csv");
   for (i = 0; i < 1000; i++){//1506; i++){
      for(j = 0; j < 1506; j++){//1000; j++){
         pixel = (int(buffer[index+1]) << 8) | int(buffer[index]);
         image[i][j] = pixel;
         file << image[i][j];
         if (j<1505){
            file << ',';
         }
         index = index + 2;
         //printf("pixel, i, j %d %d %d \n", pixel, i, j);
      }
      //printf("pixel, i, j %d %d %d \n", pixel, i, j);
      //printf(image[i][j])
      file << endl;

   }
   file.close();
   cout << "data saved" << endl;
}

unsigned long GenFauxImage(unsigned char *buffer, int *si){ // from faux xray
   unsigned char MSB, LSB;
   int pixel;
   int value = 1;
   for (int j = 0; j < *si; j = j+2){
      //buffer[j] = rand() % 255;
      if (j < 14){
         LSB = 0;
         MSB = 0;
         buffer[j] = LSB;
         buffer[j+1] = MSB;
         value++;
      }
      else{
         LSB = value;
         MSB = (value >> 8);
         buffer[j] = LSB;
         buffer[j+1] = MSB;
         value++;
      }
      pixel = int(buffer[j]) | (int(buffer[j+1]) << 8); // lsb msb
      //printf("%d \n", pixel);
   }
   return 0;
}


int main(){

    unsigned long size=4435616;//3012016;
    static unsigned char raw_data[4435616];//3012016];
    unsigned long image_result;
    unsigned short ProductID = 0x4400;
    unsigned long sensor_return_status;

    UNIT_SENSOR_INFORMATION SensorInformation;
    UNIT_XRAY_IMAGE XraySettings;
    struct _tag_UnitIntegrationParameter paramXray;

    struct _tag_UnitXrayImage paramXrayImage;
    
    paramXray.Xray_Incident_Threshold = 400; //10
    paramXray.Integration_End_Threshold = 800; //20
	  paramXray.Integration_Time = 400;/*[msec]*/
    paramXrayImage.Mode = 1;

    HANDLE DeviceHandle = USB_OpenDevice(ProductID);
    HANDLE PipeHandle = USB_OpenPipe(DeviceHandle); // not used

    sensor_return_status = HPK_GetSensorInformation(DeviceHandle, &SensorInformation.IntegParam, &SensorInformation);

    u_short serial_number = SensorInformation.Lot_Serial_No;
    std::cout << tostringX(4, serial_number) << '\n';
    sensor_return_status = HPK_GetXrayImage(DeviceHandle, raw_data, &size, &XraySettings);

    
    printf("%lu\n", sensor_return_status);

    SaveArray(raw_data, &size);

    USB_CloseDevice(DeviceHandle);
    return 0;
}
    //printf("%lu\n", sensor_return_status);

    //std::cout << tostringX(4, serial_number) << '\n';

    //printf("%hu\n", SensorInformation.Lot_Serial_No);
    //printf("%i\n", SensorInformation.Lot_Serial_No);
    //printf("%d\n", SensorInformation.Sensor_Type);
    //printf("%d\n", SensorInformation.Firmware_Version);
    //printf("%c\n", SensorInformation.Sensor_Type);
    //printf("%c\n", SensorInformation.Firmware_Version);
