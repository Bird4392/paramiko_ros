#include <iostream>
#include <ctime>
#include <fstream>

using namespace std;
//3012016

unsigned long GenFauxImage(unsigned char *buffer, int *si){
   unsigned char MSB, LSB;
   int pixel;
   int value = 1;
   for (int j = 0; j < *si; j = j+2){
      //buffer[j] = rand() % 255;
      if (j < 14){
         LSB = 0;
         MSB = 0;
         buffer[j] = LSB;
         buffer[j+1] = MSB;
         value++;
      }
      else{
         LSB = value;
         MSB = (value >> 8);
         buffer[j] = LSB;
         buffer[j+1] = MSB;
         value++;
      }
      pixel = int(buffer[j]) | (int(buffer[j+1]) << 8); // lsb msb
      //printf("%d \n", pixel);
   }
   return 0;
}

void SaveArray(unsigned char *buffer, int *si){
   static int image[1000][1506];//[1000][1506];// must be declared static so that is allocated on the heap - https://stackoverflow.com/questions/1847789/segmentation-fault-on-large-array-sizes
   int index = 0;
   int pixel, i, j;
   ofstream file ("xray_data.csv");
   for (i = 0; i < 1000; i++){//1506; i++){
      for(j = 0; j < 1506; j++){//1000; j++){
         pixel = (int(buffer[index+1]) << 8) | int(buffer[index]);
         image[i][j] = pixel;
         file << image[i][j];
         if (j<1505){
            file << ',';
         }
         index = index + 2;
         //printf("pixel, i, j %d %d %d \n", pixel, i, j);
      }
      //printf("pixel, i, j %d %d %d \n", pixel, i, j);
      //printf(image[i][j])
      file << endl;

   }
   file.close();
   cout << "data saved" << endl;
}

int main(){
   int size=3012014;//3012016;
   static unsigned char a[3012016];
   unsigned long image_result;

   image_result = GenFauxImage(a, &size); // will need to print result only sends pointer to size because thats what data shett says
   //for (int j = 0; j < size; j++){
      //cout << int(a[j]) << endl;
   //}
   //cout << size << endl;
   SaveArray(a, &size);
   //cout << size << endl;

   return 0;
}

