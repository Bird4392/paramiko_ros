/* ===================================================================================

  Copyright (c) Hamamatsu Photonics K.K. All Rights Reserved.

    Project Name : CMOS_USB
    ----------------------------------------------------------------------
    Module Name  : CMOS_USB.h
    Description  : Header file to define supported exported functions of CMOS_USB.DLL
	Version      : 2.4.0.0	

====================================================================================== */

#ifndef _CMOS_USB_H
#define _CMOS_USB_H

// Error codes
#define	CMOSUSB_ERROR_RETURN_BUFFER_LENGTH_INCORRECT	0x00000003
#define	CMOSUSB_ERROR_INVALID_HANDLE					0x00000005
#define	CMOSUSB_ERROR_NOT_ENOUGH_MEMORY					0x00000009
#define	CMOSUSB_ERROR_SEM_TIMEOUT						0x00000011
#define	CMOSUSB_ERROR_INVALID_PARAMETER					0x00000021
#define	CMOSUSB_ERROR_SUCCESS_DURING_IMAGE_TRANSFER		0x00000041
#define	CMOSUSB_ERROR_TRIG_PD_DATA_NOT_ACQUIRING		0x00000081
#define	CMOSUSB_ERROR_ACCESS_DENIED						0x00000101

#ifdef  __cplusplus
extern  "C" {
#endif

// Parameters
typedef struct _tag_UnitIntegrationParameter {
	USHORT Xray_Incident_Threshold;
	USHORT Integration_End_Threshold;
	double Integration_Time;/*[msec]*/
} UNIT_INTEGRATION_PARAMETER, *PUNIT_INTEGRATION_PARAMETER;

typedef struct _tag_UnitXrayImage {
	UCHAR Mode;
	UNIT_INTEGRATION_PARAMETER IntegParam;
} UNIT_XRAY_IMAGE, *PUNIT_XRAY_IMAGE;

typedef struct _tag_UnitSensorInformation {
	UNIT_INTEGRATION_PARAMETER IntegParam;
	USHORT Lot_Serial_No;
	UCHAR Sensor_Type;
	UCHAR Firmware_Version;
} UNIT_SENSOR_INFORMATION, *PUNIT_SENSOR_INFORMATION;

/*----------------------------------------------------------------------
    Open/Close
----------------------------------------------------------------------*/
HANDLE WINAPI USB_SetVid(
	USHORT VID,
	LPCSTR DeviceGUID
	);

HANDLE WINAPI USB_OpenDevice(
    USHORT ProductID
    );

void WINAPI USB_CloseDevice(
    HANDLE DeviceHandle
    );

HANDLE WINAPI USB_OpenPipe(
    HANDLE DeviceHandle
    );

HANDLE WINAPI USB_OpenTargetDevice(
	USHORT ProductID,
	USHORT SerialNo
	);

/*----------------------------------------------------------------------
    Suspend / Resume
----------------------------------------------------------------------*/
DWORD WINAPI USB_SuspendDevice(
   HANDLE DeviceHandle,
   UINT Time
   );
DWORD WINAPI USB_ResumeDevice(
   HANDLE DeviceHandle
   );
DWORD WINAPI USB_GetSuspendTime(
	HANDLE DeviceHandle,
	UINT *Time
);
DWORD WINAPI USB_SetDefaultSystemSuspendTime(
	HANDLE DeviceHandle,
	UINT Time
);

/*----------------------------------------------------------------------
    Read
----------------------------------------------------------------------*/
DWORD WINAPI HPK_GetXrayCorrectionImage(
   HANDLE DeviceHandle,
   UCHAR *Buffer,
   ULONG *BufferLength,
   PUNIT_XRAY_IMAGE pParam
   );

DWORD WINAPI HPK_GetXrayImage(
   HANDLE DeviceHandle,
   UCHAR *Buffer,
   ULONG *BufferLength,
   PUNIT_XRAY_IMAGE pParam
   );

DWORD WINAPI HPK_GetTrigPdData(
   HANDLE DeviceHandle,
   double AcquisitionTime,
   USHORT Buffer[],
   ULONG *BufferLength,
   PUNIT_INTEGRATION_PARAMETER pIntegParam
   );

DWORD WINAPI HPK_StopTrigPdData(
   HANDLE DeviceHandle,
   UCHAR *Buffer,
   ULONG *BufferLength, 
   PUNIT_INTEGRATION_PARAMETER pIntegParam
   );

DWORD WINAPI HPK_GetSensorInformation(
   HANDLE DeviceHandle,
   PUNIT_INTEGRATION_PARAMETER pIntegParam,
   PUNIT_SENSOR_INFORMATION pSensorInfo
   );

DWORD WINAPI HPK_AbortBulkPipe(
   HANDLE DeviceHandle
);

DWORD WINAPI HPK_ForceTrigAndGetDummy(
   HANDLE DeviceHandle,
   UCHAR *Buffer,
   ULONG *BufferLength,
   PUNIT_XRAY_IMAGE Param
   );


/*----------------------------------------------------------------------
Write/Read
----------------------------------------------------------------------*/

DWORD WINAPI HPK_ReadCustomerCode(
	HANDLE DeviceHandle,
	UCHAR *Buffer,
	UINT Index,
	UCHAR *Code
	);

DWORD WINAPI HPK_ReadTypeNumber(
	HANDLE DeviceHandle,
	UCHAR *Buffer
	);

DWORD WINAPI HPK_ReadMemory(
	HANDLE DeviceHandle,
	UCHAR *Buffer,
	UINT Address,
	UINT Length
	);
/*----------------------------------------------------------------------
Wait Event
----------------------------------------------------------------------*/

enum {
	CMOSUSB_EVENT_IMAGE_STARTED = 0x0001,
	CMOSUSB_EVENT_IMAGE_STOPPED = 0x0002,
};

DWORD WINAPI HPK_WaitEvent(
	HANDLE DeviceHandle,
	DWORD* pEventStatus,
	DWORD Timeout,
	HANDLE hAbortEvent
	);

#if (defined(_MSC_VER)&&defined(_LINK_CMOSUSB_LIB))
#pragma comment(lib, "cmos_usb.lib")
#endif

#ifdef  __cplusplus
}
#endif

#endif